/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "common_functions.h"

static void dhcpv4c_config_add_options(amxc_var_t* data,
                                       bool req_or_sent) {
    const char* opts = req_or_sent ? "ReqOption" : "SentOption";
    amxc_var_t* var_opts = amxc_var_add_key(amxc_llist_t, data, opts, NULL);
    amxc_var_t* option = amxc_var_add(amxc_htable_t, var_opts, NULL);
    amxc_llist_t options;

    amxc_llist_init(&options);
    amxc_var_add_key(uint8_t, option, "Tag", 66);
    amxc_var_add_key(bool, option, "Enable", true);
    if(req_or_sent == false) {
        // Only when SentOption
        amxc_var_add_key(cstring_t, option, "Value", "736F66746174686F6D65");
    }
    amxc_llist_clean(&options, amxc_string_list_it_free);
}

int start_stop_client(bool start, const char* intf) {
    const char* func = start ? "dhcpv4c-start" : "dhcpv4c-stop";
    int retval = -1;
    amxc_var_t result;
    amxc_var_t data;

    amxc_var_init(&result);
    amxc_var_init(&data);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", "Device.IP.Interface.2.");
    amxc_var_add_key(cstring_t, &data, "IfName", intf);

    retval = amxm_execute_function(NULL, "mod-dhcpv4c", func, &data, &result);
    when_failed(retval, exit);
    retval = GET_INT32(&result, NULL);
exit:

    amxc_var_clean(&result);
    amxc_var_clean(&data);
    return retval;
}

void add_data_to_var(amxc_var_t* data) {
    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, data, "intf_name", INTF_NAME);
    amxc_var_add_key(cstring_t, data, "Interface", "Device.IP.Interface.2.");
    amxc_var_add_key(cstring_t, data, "Enable", "true");
    dhcpv4c_config_add_options(data, "ReqOption");
    dhcpv4c_config_add_options(data, "SentOption");

}
