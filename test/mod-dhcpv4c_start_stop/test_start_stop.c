/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <yajl/yajl_gen.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <unistd.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_parameter.h>
#include <amxd/amxd_transaction.h>
#include <amxut/amxut_bus.h>

#include <amxo/amxo.h>

#include "test_start_stop.h"
#include "dhcp_stat.h"
#include "dhcp_state.h"
#include "dhcp_packet.h"
#include "dhcp_lease.h"
#include "dhcp_dm.h"
#include "dhcp_util.h"
#include "mock_socket.h"
#include "mod_dhcpv4c.h"
#include <amxm/amxm.h>
#include "common_functions.h"

#include <fcntl.h>
#include <errno.h>
#include <amxj/amxj_variant.h>

#define INTF_NAME               "eth0"
#define WRONG_INTF_NAME         "ethx"

int test_setup(UNUSED void** state) {
    amxm_shared_object_t* so = amxm_get_so("self");
    amxm_module_t* mod = NULL;
    assert_non_null(so);
    assert_int_equal(amxm_module_register(&mod, so, MOD_CORE), 0);
    return 0;
}

int test_teardown(UNUSED void** state) {
    return 0;
}

static int dhcpv4c_config_changed(const char* intf) {
    amxc_var_t params;
    amxc_var_t ret;
    int retval = -1;

    amxc_var_init(&params);
    amxc_var_init(&ret);
    add_data_to_var(&params);
    amxc_var_add_key(cstring_t, &params, "IfName", intf);

    retval = amxm_execute_function(NULL, "mod-dhcpv4c", "dhcpv4c-config-update", &params, &ret);
    amxc_var_clean(&params);
    amxc_var_clean(&ret);
    return retval;
}


void test_can_start_module(UNUSED void** state) {

    assert_int_equal(start_stop_client(true, INTF_NAME), 0);

    assert_int_equal(start_stop_client(true, WRONG_INTF_NAME), -1);
}

void test_can_stop_module(UNUSED void** state) {
    assert_int_equal(start_stop_client(false, INTF_NAME), 0);
}

void test_config_changed(UNUSED void** state) {
    assert_int_equal(dhcpv4c_config_changed(INTF_NAME), 0);
    assert_int_equal(dhcpv4c_config_changed(WRONG_INTF_NAME), -1);
}

void test_update_config(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    assert_int_equal(amxm_execute_function(NULL, "mod-dhcpv4c",
                                           "update-config", &data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), 0);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}


