# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.13.7 - 2024-05-30(10:57:11 +0000)

### Other

- [tr181-dhcpv4client] crash in sys_recv_dhcp_packet_raw -> sys_recv_checksum
- [tr181-dhcpv4client] Crashes during test

## Release v0.13.6 - 2024-04-08(14:36:42 +0000)

### Fixes

- [DHCPv4Client] [API] Provide a Renew and Release Implementation

## Release v0.13.5 - 2023-11-19(12:25:32 +0000)

### Fixes

- [TR181-DHCPv4Client] Do not add option 50 twice to packets

## Release v0.13.4 - 2023-08-24(10:16:18 +0000)

### Fixes

- [mod-dhcpv4c] No socket found trace always printed

## Release v0.13.3 - 2023-08-21(14:56:34 +0000)

### Fixes

- [PrplOS][CI][MxL][mod-dhcpv4c] No wan dhcpv4 on CDRouter tests

## Release v0.13.2 - 2023-08-17(14:12:07 +0000)

### Other

- - [TR181-DHCPv4Client] Create unit tests for the mod-dhcpv4c backend

## Release v0.13.1 - 2023-08-03(13:43:42 +0000)

## Release v0.13.0 - 2023-07-06(10:04:20 +0000)

### New

- [TR181-DHCPv4Client] Implement the dhcpv4c-renew amxm call in mod-dhcpv4c

## Release v0.12.0 - 2023-07-06(08:08:48 +0000)

### New

- [TR181-DHCPv4Client] Implement statistics for mod-dhcpv4c

## Release v0.11.1 - 2023-06-28(08:25:13 +0000)

### Fixes

- Fix release after reboot for consecutive reboots

## Release v0.11.0 - 2023-06-27(12:51:06 +0000)

### New

- Implement updating of socket dscp and priority values
-  [TR181-DHCPv4Client] It must be possible to configure broadcast/unicast mode

## Release v0.10.0 - 2023-06-13(11:28:45 +0000)

### New

- Send release after reboot if necessary

## Release v0.9.0 - 2023-06-07(08:37:38 +0000)

### New

- Send client error information to the datamodel

## Release v0.8.0 - 2023-05-26(09:21:29 +0000)

### New

- Add forcerenew support (rfc 3203)

## Release v0.7.0 - 2023-05-25(16:37:23 +0000)

### New

- Implement authentication (rfc 3118)

## Release v0.6.2 - 2023-05-12(14:08:55 +0000)

### Fixes

- Do not accept offers where xid does not match

## Release v0.6.1 - 2023-05-12(10:26:09 +0000)

### Fixes

- Update requested options instead of setting them once

## Release v0.6.0 - 2023-05-11(12:44:38 +0000)

### New

- Set max msg size (option 57) when starting dhcp

## Release v0.5.1 - 2023-05-10(07:57:01 +0000)

### Fixes

- Go to init state after receiving NAK

## Release v0.5.0 - 2023-05-05(11:37:04 +0000)

### New

- Add retransmission configuration

## Release v0.4.3 - 2023-05-04(14:05:27 +0000)

### Fixes

- Reuse requested IP tx option if it was not cleaned explicitly

## Release v0.4.2 - 2023-05-04(10:01:11 +0000)

### Fixes

- Do not set empty tx options
- Store interface path and use it as identifier

## Release v0.4.1 - 2023-04-20(08:57:03 +0000)

### Fixes

- Compare ifindex before reusing existing lease

## Release v0.4.0 - 2023-04-18(13:20:30 +0000)

### New

- Enable renew and rebind timers

## Release v0.3.0 - 2023-04-04(11:00:27 +0000)

### New

- Implement basic DHCP DORA cycle

## Release v0.2.1 - 2023-03-07(10:38:17 +0000)

### Fixes

- Port the existing packet & options lib to amx

## Release v0.2.0 - 2023-03-07(10:03:22 +0000)

### New

- Port the existing packet & options lib to amx

## Release v0.1.0 - 2023-02-23(11:55:00 +0000)

### New

- Initial module code

