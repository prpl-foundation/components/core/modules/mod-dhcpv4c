/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <net/if.h>
#include <net/if_arp.h>

#include <amxc/amxc_macros.h>
#include "debug/sahtrace.h"
#include "debug/sahtrace_macros.h"

#include "dhcp_packet.h"
#include "dhcp_options.h"
#include "dhcp_util.h"
#include "dhcp_txoption.h"
#include "mod_dhcpv4c.h"
#include "dhcp_stat.h"

#define MAX_SECS 65535

// These parameters are suppresed from the requested parameter list for INFORM messages.
static unsigned char dhcp_inform_suppressed_pars[] =
{
    TAG_IP_LEASE_TIME,
    TAG_RENEWAL_TIME,
    TAG_REBIND_TIME,
};

static unsigned char default_mac[ADDR_MAC_LEN] = {0x00, 0x0E, 0x50, 0xAA, 0xBB, 0xCC};

void dhcp_packet_init(dhcp_packet_t* packet) {
    when_null(packet, exit);

    memset(packet, 0, sizeof(dhcp_packet_t));

    packet->other.sin_family = AF_INET;

    packet->tos = -1;
    packet->priority = -1;

exit:
    return;
}

static bool tag_is_suppressed(unsigned char tag) {
    bool ret = false;

    for(unsigned int i = 0; i < sizeof(dhcp_inform_suppressed_pars); i++) {
        if(dhcp_inform_suppressed_pars[i] == tag) {
            ret = true;
            break;
        }
    }

    return ret;
}

static int dhcp_packet_set_macaddress(dhcp_lease_t* lease, dhcp_packet_t* packet) {
    struct ifreq ifreq;
    int s = -1;
    int ret = -1;

    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(packet, exit, ERROR, "Packet is NULL");

    s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    when_true_trace(s < 0, exit, ERROR, "Socket failed[%s]", strerror(errno));

    memset(&ifreq, 0, sizeof(struct ifreq));
    snprintf(ifreq.ifr_name, IFNAMSIZ, "%s", lease->ifname);

    ret = ioctl(s, SIOCGIFHWADDR, &ifreq);
    when_true_trace(ret == -1, exit_sock, ERROR, "Get mac address of [%s] failed.", lease->ifname);

    packet->raw.hlen = ADDR_MAC_LEN;
    packet->raw.htype = ifreq.ifr_hwaddr.sa_family;
    if((ifreq.ifr_hwaddr.sa_family == ARPHRD_ETHER) ||
       (ifreq.ifr_hwaddr.sa_family == ARPHRD_IEEE802)) {
        memcpy(packet->raw.chaddr, ifreq.ifr_hwaddr.sa_data, ADDR_MAC_LEN);
        SAH_TRACEZ_INFO(ME, "Found mac address from[%s]", lease->ifname);
    } else {
        memcpy(packet->raw.chaddr, default_mac, ADDR_MAC_LEN);
    }

    ret = 0;

exit_sock:
    close(s);
exit:
    return ret;
}

static int dhcp_packet_req_options(dhcp_lease_t* lease,
                                   dhcp_msg_type_t type,
                                   dhcp_packet_t* packet,
                                   unsigned char** poption) {
    unsigned char* pprl_len = 0;
    int ret = -1;

    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(packet, exit, ERROR, "Packet is NULL");
    when_null_trace(poption, exit, ERROR, "Poption is NULL");

    // check if msg supports req options
    if((type != DHCPDISCOVER) && (type != DHCPREQUEST) && (type != DHCPINFORM)) {
        SAH_TRACEZ_INFO(ME, "Msg type does not support ReqOptions");
        goto exit;
    }

    // check if we have options
    when_null_trace(amxc_var_get_first(&lease->req_options), exit, INFO, "No ReqOptions provided");

    *(*poption) = TAG_PARM_REQ_LIST;
    (*poption)++;
    pprl_len = (*poption)++;
    *pprl_len = 0;
    packet->packet_size += 2;

    // Iterate over option list and add them to the packet
    amxc_var_for_each(var, &lease->req_options) {
        unsigned char tag = GET_UINT32(var, "Tag");

        if((type == DHCPINFORM) && tag_is_suppressed(tag)) {
            continue;
        }

        *(*poption)++ = tag;
        (*pprl_len)++;
        packet->packet_size++;
    }

    ret = 0;

exit:
    return ret;
}

static int dhcp_packet_tx_options(dhcp_lease_t* lease,
                                  dhcp_msg_type_t msgtype,
                                  dhcp_packet_t* packet,
                                  unsigned char** poption) {
    int ret = -1;

    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(packet, exit, ERROR, "Packet is NULL");
    when_null_trace(poption, exit, ERROR, "Poption is NULL");

    // Iterate over txoptions
    amxc_var_for_each(var, &lease->sent_options) {
        unsigned char tag = GET_UINT32(var, "Tag");

        if(!GET_BOOL(var, "Enable")) {
            SAH_TRACEZ_INFO(ME, "Tx option %u disabled, skip", tag);
            continue;
        }

        if(str_empty(GET_CHAR(var, "Value"))) {
            SAH_TRACEZ_INFO(ME, "Tx option %u has no value set, skip", tag);
            continue;
        }

        switch(msgtype) {
        case DHCPDISCOVER:
        case DHCPINFORM:
        case DHCPREQUEST:
            if((tag != TAG_REQ_IP) && (tag != TAG_IP_LEASE_TIME)) {
                SAH_TRACEZ_INFO(ME, "Add Tx Option[%u]", tag);
                txoption_write_option(packet, poption, var);
            }
            break;

        case DHCPRELEASE:
            if(tag == TAG_DHCP_MSG_AUTH) {
                SAH_TRACEZ_INFO(ME, "Add Tx Option[%u]", tag);
                txoption_write_option(packet, poption, var);
            }
        // FALLTHROUGH
        case DHCPDECLINE:
            if((tag == TAG_CLIENT_ID) || (tag == TAG_USER_CLASS_ID)) {
                SAH_TRACEZ_INFO(ME, "Add Tx Option[%u]", tag);
                txoption_write_option(packet, poption, var);
            }
            break;

        default:
            break;
        }
    }

    ret = 0;

exit:
    return ret;
}

// Select a random number, make sure it is not zero
static unsigned long dhcp_packet_select_xid(long ifindex) {
    unsigned long xid = 0;

    while((xid == 0) || dhcp_lease_get(ifindex, xid) != NULL) {
        xid = rand();
    }

    return xid;
}

static void dhcp_packet_make_addr(dhcp_lease_t* lease,
                                  dhcp_msg_type_t msgtype,
                                  dhcp_packet_t* packet) {
    switch(msgtype) {
    case DHCPDISCOVER:
    case DHCPDECLINE:
        packet->raw.ciaddr = 0;
        break;

    case DHCPINFORM:
    case DHCPRELEASE:
        packet->raw.ciaddr = lease->ip_addr;
        break;

    case DHCPREQUEST:
        if((lease->state == dhcp_client_state_requesting) || (lease->state == dhcp_client_state_rebooting)) {
            SAH_TRACEZ_INFO(ME, "[%s], Use ciaddr == 0 in REQUESTING or REBOOTING state", lease->id);
            packet->raw.ciaddr = 0;
        } else {
            // in bound, renewing or rebinding state
            SAH_TRACEZ_INFO(ME, "[%s], Use ciaddr == ipaddr in BOUND, RENEW or REBIND state", lease->id);
            packet->raw.ciaddr = lease->ip_addr;
        }
        break;

    default:
        break;
    }

    packet->raw.yiaddr = 0;
    packet->raw.siaddr = 0;
    packet->raw.giaddr = 0;
}

static int dhcp_packet_make_req_ip_leasetime(dhcp_lease_t* lease,
                                             dhcp_msg_type_t msgtype,
                                             dhcp_packet_t* packet,
                                             unsigned char** poption) {
    int ret = -1;
    amxc_var_t* txoption = NULL;

    switch(msgtype) {
    case DHCPDISCOVER:
        // include preferred ip address, if any
        txoption = option_list_find_tag(&lease->sent_options, TAG_REQ_IP);
        if(txoption != NULL) {
            SAH_TRACEZ_INFO(ME, "Add IP Address in DHCP Discover");
            txoption_write_option(packet, poption, txoption);
        }

        // include preferred lease time, if any
        txoption = option_list_find_tag(&lease->sent_options, TAG_IP_LEASE_TIME);
        if(txoption != NULL) {
            SAH_TRACEZ_INFO(ME, "Add Lease time in DHCP Discover");
            txoption_write_option(packet, poption, txoption);
        }
        break;

    case DHCPREQUEST:
        // only when requesting the lease after an OFFER or when rebooting, not when trying to extend the lease
        if((lease->state == dhcp_client_state_requesting) || (lease->state == dhcp_client_state_rebooting)) {
            txoption = option_list_find_tag(&lease->sent_options, TAG_REQ_IP);
            if((txoption == NULL) || !GET_BOOL(txoption, "Enable")) {
                SAH_TRACEZ_ERROR(ME, "Cannot find requested IP address option.");
                goto exit;
            }
            SAH_TRACEZ_INFO(ME, "Write TAG_REQ_IP in DHCP REQUEST");
            txoption_write_option(packet, poption, txoption);
        }

        // preferred lease time
        txoption = option_list_find_tag(&lease->sent_options, TAG_IP_LEASE_TIME);
        if(txoption != NULL) {
            SAH_TRACEZ_INFO(ME, "Write TAG_REQ_LEASE_TIME in Request msg");
            txoption_write_option(packet, poption, txoption);
        }
        break;

    case DHCPDECLINE:
        // MUST
        write_optiondword(packet, poption, TAG_REQ_IP, lease->ip_addr);
        break;

    case DHCPINFORM:
    case DHCPRELEASE:
    default:
        break;
    }
    ret = 0;

exit:
    return ret;
}

static void dhcp_packet_make_server_id(dhcp_lease_t* lease,
                                       dhcp_msg_type_t msgtype,
                                       dhcp_packet_t* packet,
                                       unsigned char** poption) {
    switch(msgtype) {
    case DHCPDISCOVER:
    case DHCPINFORM:
        break;

    case DHCPREQUEST:
        // Only when requesting the lease after an OFFER not when trying to extend the lease
        if(lease->state != dhcp_client_state_requesting) {
            break;
        }
    // FALLTHROUGH

    // otherwise, write the server id option
    case DHCPDECLINE:
    case DHCPRELEASE:
        write_optiondword(packet, poption, TAG_SERVER_ID, lease->server_ip);
        break;

    default:
        break;
    }
}

static void dhcp_packet_make_dest_addr(dhcp_lease_t* lease,
                                       dhcp_msg_type_t msgtype,
                                       dhcp_packet_t* packet) {
    dhcp_broadcast_flag_t bflag = DISCOVER_BFLAGS;

    switch(msgtype) {
    case DHCPDISCOVER:
    case DHCPDECLINE:
        // always broadcast
        packet->other.sin_addr.s_addr = htonl(INADDR_BROADCAST);
        packet->other.sin_intf = lease->ifindex;

        bflag = (msgtype == DHCPDISCOVER) ? DISCOVER_BFLAGS : DECLINE_BFLAGS;
        if((lease->flags.flag_broadcast != 0) && ((lease->broadcast_flags & bflag) != 0)) {
            packet->raw.flags = htons(BOOTP_BROADCAST_FLAG);
        } else {
            packet->raw.flags = 0;
        }
        break;
    case DHCPINFORM:
        // server ip can be a broadcast in this state or unicast
        if(lease->server_ip == htonl(INADDR_BROADCAST)) {
            packet->other.sin_addr.s_addr = lease->server_ip;
            packet->other.sin_intf = lease->ifindex;
            break;
        }
    // FALLTHROUGH

    case DHCPREQUEST:
        // sometimes broadcast
        if((lease->state == dhcp_client_state_requesting) ||
           (lease->state == dhcp_client_state_rebinding) ||
           (lease->state == dhcp_client_state_rebooting)) {
            packet->other.sin_addr.s_addr = htonl(INADDR_BROADCAST);
            packet->other.sin_intf = lease->ifindex;

            if(lease->state == dhcp_client_state_requesting) {
                bflag = REQUEST_BFLAGS;
            } else if(lease->state == dhcp_client_state_rebinding) {
                bflag = REBIND_BFLAGS;
            } else {
                bflag = REBOOT_BFLAGS;
            }

            if((lease->flags.flag_broadcast != 0) && ((lease->broadcast_flags & bflag) != 0)) {
                packet->raw.flags = htons(BOOTP_BROADCAST_FLAG);
            } else {
                packet->raw.flags = 0;
            }
            break;
        }
    // or unicast (don't put a break here)
    // FALLTHROUGH

    case DHCPRELEASE:
        // always unicast
        packet->other.sin_addr.s_addr = lease->server_ip;
        packet->other.sin_intf = lease->ifindex;
        packet->raw.flags = 0;
        break;

    default:
        break;
    }
}

static int dhcp_packet_make(dhcp_lease_t* lease, dhcp_msg_type_t msgtype, dhcp_packet_t* packet) {
    unsigned char* poption = NULL;
    int ret = -1;

    packet->packet_size = DHCP_MIN_SIZE;
    packet->raw.opcode = BOOTREQUEST;
    packet->raw.hops = 0;
    // leave this parameter empty it is only used for release on reboot feature
    packet->ifindex = 0;

    // set mac address, family
    dhcp_packet_set_macaddress(lease, packet);

    // Only in the requesting state we must take the xid from the
    // DHCPOFFER otherwise we select a xid ourselves
    // also use the previously used secs field in the REQUESTING state
    if(lease->state != dhcp_client_state_requesting) {
        // if we already have selected one, use that (retransmitting), if the xid is still 0, select a new id
        if(lease->xid == 0) {
            lease->xid = dhcp_packet_select_xid(lease->ifindex);
        }

        if((msgtype != DHCPDECLINE) && (msgtype != DHCPRELEASE)) {
            struct timeval ts;
            uint32_t tmp = 0;

            get_timestamp(&ts);
            tmp = ts.tv_sec - lease->acquiring_time.tv_sec;
            lease->secs = htons((tmp < MAX_SECS) ? tmp : MAX_SECS);
        } else {
            // DHCPDECLINE or DHCPRELEASE
            lease->secs = 0;
        }
    }

    packet->raw.xid = htonl(lease->xid);
    packet->raw.secs = lease->secs;

    dhcp_packet_make_addr(lease, msgtype, packet);

    //  Add options, poption points to raw->options
    write_magic(packet, &poption);

    // message type
    write_optionchar(packet, &poption, TAG_DHCP_MSG_TYPE, msgtype);

    // Req IP and leasetime
    when_failed(dhcp_packet_make_req_ip_leasetime(lease, msgtype, packet, &poption), exit);

    // server identifier
    dhcp_packet_make_server_id(lease, msgtype, packet, &poption);

    // add req options
    dhcp_packet_req_options(lease, msgtype, packet, &poption);

    // write tx options
    dhcp_packet_tx_options(lease, msgtype, packet, &poption);

    // end options
    write_endoption(packet, &poption);

    // broadcast the DHCPREQUEST when in the REQUESTING (first time we acquire the lease) or in the REBINDING state
    packet->other.sin_family = AF_INET;
    packet->other.sin_port = htons(DHSERVER_PORT);

    // Destination address
    dhcp_packet_make_dest_addr(lease, msgtype, packet);

    packet->tos = lease->tos;
    packet->priority = lease->priority;

    ret = 0;

exit:
    return ret;
}

// Do basic checks on the received DHCP packet.
// Return -1 if an error occured, 0 if success.
int dhcp_packet_check_lease(dhcp_packet_t* packet) {
    uint32_t leasetime;
    int autoconfig = 0;
    int ret = -1;

    when_null_trace(packet, exit, ERROR, "Packet is NULL");

    // DHCP header:
    // opcode
    when_false_trace(packet->raw.opcode == BOOTREPLY, exit, ERROR,
                     "Corrupted packet received - invalid opcode.");

    // for FORCERENEW, check that the packet is unicast.
    if(packet->packet_type == DHCPFORCERENEW) {
        if((ntohl(packet->saddr) == UINT32_MAX) ||
           ((ntohl(packet->saddr) & 0xF0000000) == 0xE0000000)) {
            SAH_TRACEZ_ERROR(ME, "ForceRenew must be UNICAST[%lx].", (unsigned long) ntohl(packet->saddr));
            goto exit;
        }
        goto exit_ok;
    }

    // client IP address
    if(((packet->raw.yiaddr == 0) && (packet->packet_type != DHCPOFFER)) ||
       (packet->raw.yiaddr == UINT32_MAX)) {
        // zero yiaddr is only acceptable ico. OFFER - cfr. RFC 2563
        SAH_TRACEZ_ERROR(ME, "Corrupted packet received - invalid yiaddr.");
        goto exit;
    }

    // DHCP options:
    // DHCP server id
    if(packet->dhcp_option[TAG_SERVER_ID].len == 0) {
        SAH_TRACEZ_ERROR(ME, "Corrupted packet received - missing server id.");
        goto exit;
    }

    // for NAK, this is sufficient. Go on in case of OFFER, ACK or FORCERENEW.
    when_true(packet->packet_type == DHCPNAK, exit_ok);

    // DHCP lease time
    if(packet->dhcp_option[TAG_IP_LEASE_TIME].len == 0) {
        SAH_TRACEZ_ERROR(ME, "Corrupted packet received - lease time missing.");
        goto exit;
    }
    READ_OPTION(packet->dhcp_option[TAG_IP_LEASE_TIME], &leasetime, sizeof(leasetime));

    // must be greater than 0
    when_true_trace(leasetime == 0, exit, ERROR, "Corrupted packet received - invalid lease time.");

    // for ACK, this is sufficient. Go on in case of OFFER.
    when_true(packet->packet_type == DHCPACK, exit_ok);

    // zero yiaddr is only valid if auto-configure option is present [RFC-2563]
    if(packet->raw.yiaddr == 0) {
        if(packet->dhcp_option[TAG_AUTO_CONFIGURE].len == 1) {
            READ_OPTION(packet->dhcp_option[TAG_AUTO_CONFIGURE], &autoconfig, sizeof(autoconfig));
        } else {
            SAH_TRACEZ_ERROR(ME, "Corrupted packet received - invalid yiaddr.");
        }

        goto exit;
    }

exit_ok:
    ret = 0;
exit:
    return ret;
}

// Send packet and increase counters
void dhcp_packet_send(dhcp_lease_t* lease, dhcp_msg_type_t msgtype) {
    int ret = 0;
    dhcp_packet_t packet;
    memset(&packet, 0, sizeof(packet));

    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(lease->intf, exit, WARNING, "Lease does not have an interface reference");

    // make packet
    if(dhcp_packet_make(lease, msgtype, &packet) != 0) {
        SAH_TRACEZ_ERROR(ME, "Make packet failed, return");
        dhcp_stat_inc_counter(DHCLIENT_CNT_NO_SENT_FAILURE);
        goto exit;
    }

    // send it
    if(!lease->raw_socket) {
        ret = sys_send_dhcp_packet(lease->intf->dhcp_sock,
                                   &packet,
                                   packet.other.sin_addr.s_addr == INADDR_BROADCAST ? INADDR_ANY : lease->ip_addr);
    } else {
        packet.ifindex = lease->ifindex;
        memcpy(packet.sll_addr, lease->server_lladdr, ADDR_MAC_LEN);
        ret = sys_send_dhcp_packet(lease->intf->dhcp_raw_sock,
                                   &packet,
                                   packet.other.sin_addr.s_addr == INADDR_BROADCAST ? INADDR_ANY : lease->ip_addr);
        memset(packet.sll_addr, 0, CHADDR_LEN);
        packet.ifindex = 0;
    }
    if(ret != 0) {
        // increase counter
        switch(msgtype) {
        case DHCPDISCOVER:
            SAH_TRACEZ_WARNING(ME, "[%s] Broadcast DISCOVER.", lease->id);
            dhcp_stat_inc_counter(DHCLIENT_CNT_NO_SENT_DISCOVER);
            break;

        case DHCPREQUEST:
            SAH_TRACEZ_WARNING(ME, "[%s] Intf[%s] REQUEST %u.%u.%u.%u from %u.%u.%u.%u [%s]", lease->id,
                               lease->ifname, DHCP_IP_QUAD(lease->ip_addr), DHCP_IP_QUAD(lease->server_ip),
                               (packet.other.sin_addr.s_addr == (unsigned int) -1) ? "broadcast" : "unicast");
            dhcp_stat_inc_counter(DHCLIENT_CNT_NO_SENT_REQUEST);
            break;

        case DHCPDECLINE:
            SAH_TRACEZ_WARNING(ME, "Intf[%s] declines %u.%u.%u.%u to server %u.%u.%u.%u.",
                               lease->ifname, DHCP_IP_QUAD(lease->ip_addr), DHCP_IP_QUAD(lease->server_ip));
            dhcp_stat_inc_counter(DHCLIENT_CNT_NO_SENT_DECLINE);
            break;

        case DHCPRELEASE:
            SAH_TRACEZ_WARNING(ME, "Intf[%s] releases %u.%u.%u.%u to server %u.%u.%u.%u.",
                               lease->ifname, DHCP_IP_QUAD(lease->ip_addr), DHCP_IP_QUAD(lease->server_ip));
            dhcp_stat_inc_counter(DHCLIENT_CNT_NO_SENT_RELEASE);
            break;

        case DHCPINFORM:
            SAH_TRACEZ_WARNING(ME, "Intf[%s] informs about %u.%u.%u.%u to %u.%u.%u.%u.",
                               lease->ifname, DHCP_IP_QUAD(lease->ip_addr), DHCP_IP_QUAD(packet.other.sin_addr.s_addr));
            dhcp_stat_inc_counter(DHCLIENT_CNT_NO_SENT_INFORM);
            break;

        default:
            break;
        }
    } else {
        SAH_TRACEZ_ERROR(ME, "[%s], Failed to transmit DHCP msg", lease->id);
        dhcp_stat_inc_counter(DHCLIENT_CNT_NO_SENT_FAILURE);
    }

exit:
    return;
}

