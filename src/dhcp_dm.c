/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include "debug/sahtrace.h"
#include "debug/sahtrace_macros.h"
#include "v4v6option.h"

#include "dhcp_dm.h"
#include "dhcp_txoption.h"
#include "dhcp_util.h"
#include "mod_dhcpv4c.h"

static const char* state_str[dhcp_client_state_number] = {
    "Invalid",
    "Idle",
    "Init_reboot",
    "Rebooting",
    "Init",
    "Selecting",
    "Requesting",
    "Bound",
    "Renewing",
    "Rebinding",
    "Bootp",
    "Informing",
    "Inform_ok"
};

static const char* error_str[] = {
    "None",
    "LocalRelease",
    "RemoteNack",
    "RenewTimeout",
    "RebindTimeout",
    "RequestingTimeout",
    "InformTimeout",
    "XidFailure",
    "ServerIPFailure",
    "IPAddrFailure",
    "StateFailure",
    "MsgFailure",
    "LinkFailure",
    "Authentication Failure"
};

static const char* dhcp_client_state_to_string(dhcp_client_state_t state) {
    const char* str = "Invalid";

    when_true_trace(state >= dhcp_client_state_number, exit, ERROR, "Invalid state");

    str = state_str[state];

exit:
    return str;
}

static const char* dhcp_error_to_string(unsigned long error) {
    const char* str = "None";

    when_true_trace(error > DHCLIENT_ERR_AUTHENTICATION_FAILURE, exit, ERROR, "Invalid error");

    str = error_str[error];

exit:
    return str;
}

// Used to get gateway and dns server info
static void dhcp_option_address_to_var(amxc_var_t* data,
                                       dhcp_lease_t* lease,
                                       const char* param_name,
                                       unsigned char tag) {
    amxc_var_t* addrs = option_list_find_tag(&lease->req_options, tag);
    amxc_var_t info;
    unsigned char* bin = NULL;
    unsigned int len = 0;

    amxc_var_init(&info);

    when_null_trace(addrs, exit, INFO, "[%s] No info found", lease->id);

    // convert hex to bin
    bin = hex_to_bin(GET_CHAR(addrs, "Value"), &len);
    when_null_trace(bin, exit, ERROR, "Unable to parse address info");

    // parse bin to llist
    dhcpoption_v4parse(&info, tag, len, bin);

    // convert llist to csv string
    amxc_var_cast(&info, AMXC_VAR_ID_CSV_STRING);

    // add csv string to var
    amxc_var_set_key(data, param_name, &info, AMXC_VAR_FLAG_COPY);

exit:
    amxc_var_clean(&info);
    free(bin);
    return;
}

void dhcp_dm_update(dhcp_lease_t* lease) {
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_t* params = NULL;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &data, "parameters", NULL);
    amxc_var_add_key(cstring_t, params, "ifname", lease->ifname);
    amxc_var_add_key(cstring_t, params, "status", dhcp_client_state_to_string(lease->state));
    amxc_var_add_key(cstring_t, params, "error", dhcp_error_to_string(lease->client_err));

    // More info must be provided in bound state
    if(lease->state == dhcp_client_state_bound) {
        struct in_addr addr;

        // LeaseTime (lease->lease_time.tv_sec)
        amxc_var_add_key(int32_t, params, "LeaseTimeRemaining", lease->lease_time.tv_sec);
        amxc_var_cast(GET_ARG(params, "LeaseTimeRemaining"), AMXC_VAR_ID_CSTRING);
        // IPAddress (lease)
        addr.s_addr = lease->ip_addr;
        amxc_var_add_key(cstring_t, params, "IPAddress", inet_ntoa(addr));
        // SubnetMask (lease)
        addr.s_addr = lease->netmask;
        amxc_var_add_key(cstring_t, params, "SubnetMask", inet_ntoa(addr));
        // DHCPServer (lease)
        addr.s_addr = lease->server_ip;
        amxc_var_add_key(cstring_t, params, "DHCPServer", inet_ntoa(addr));
        // Req Options (packet/lease)
        amxc_var_add_key(amxc_llist_t, params, "ReqOption",
                         amxc_var_constcast(amxc_llist_t, &lease->req_options));
        // Dnsservers (get from options)
        dhcp_option_address_to_var(params, lease, "DNSServers", TAG_DOMAIN_SERVER);
        // Gateway (get from options)
        dhcp_option_address_to_var(params, lease, "IPRouters", TAG_GATEWAY);
    }

    if(amxm_execute_function(NULL, MOD_CORE, "update-client", &data, &ret) != 0) {
        SAH_TRACEZ_ERROR(ME, "[%s] Failed to update datamodel", lease->id);
    }

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return;
}
