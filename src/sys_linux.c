/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netpacket/packet.h>
#include <netinet/if_ether.h>
#include <net/ethernet.h>
#include <unistd.h>
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>

#include "debug/sahtrace.h"
#include "debug/sahtrace_macros.h"
#include <amxc/amxc_macros.h>

#include "dhcp_packet.h"
#include "dhcp_options.h"
#include "mod_dhcpv4c.h"

#define MAX_IPHDR_LEN 60
#define UDPHDR_LEN 8

// pseudo header for udp checksum
struct pheader {
    uint32_t src;
    uint32_t dst;
    uint8_t placeholder;
    uint8_t protocol;
    uint16_t len;
};

// structure for raw socket
struct ippacket {
    struct iphdr ip;
    struct udphdr udp;
    dhcp_raw_packet_t dhcp;
};

static unsigned short checksum(void* data, int size) {
    unsigned int sum = 0;
    unsigned short* source = (unsigned short*) data;

    while(size > 1) {
        sum += *source++;
        size -= 2;
    }

    if(size > 0) {
        unsigned short tmp = 0;
        *(unsigned char*)&tmp = *(unsigned char*) source;
        sum += tmp;
    }

    while(sum >> 16) {
        sum = (sum & 0xffff) + (sum >> 16);
    }

    SAH_TRACEZ_INFO(ME, "Result checksum[0x%04x]", (unsigned short) ~sum);
    return (unsigned short) ~sum;
}

static void __sys_create_ip_packet(struct ippacket* ippkt,
                                   dhcp_packet_t* packet,
                                   uint32_t saddr,
                                   uint32_t daddr,
                                   unsigned short sport,
                                   unsigned short dport,
                                   uint8_t ttl) {
    memset(ippkt, 0, sizeof(*ippkt));
    memcpy((char*) &ippkt->dhcp, (char*) &packet->raw, packet->packet_size);
    ippkt->ip.protocol = IPPROTO_UDP;
    ippkt->ip.saddr = saddr;
    ippkt->ip.daddr = daddr;
    ippkt->ip.tot_len = htons(sizeof(struct udphdr) + packet->packet_size);
#ifdef __FAVOR_BSD
    ippkt->udp.uh_sport = htons(sport);
    ippkt->udp.uh_dport = htons(dport);
    ippkt->udp.uh_ulen = htons(sizeof(struct udphdr) + packet->packet_size);
    ippkt->udp.uh_sum = checksum(ippkt, sizeof(struct iphdr) + sizeof(struct udphdr) + packet->packet_size);
#else
    ippkt->udp.source = htons(sport);
    ippkt->udp.dest = htons(dport);
    ippkt->udp.len = htons(sizeof(struct udphdr) + packet->packet_size);
    ippkt->udp.check = checksum(ippkt, sizeof(struct iphdr) + sizeof(struct udphdr) + packet->packet_size);
#endif
    ippkt->ip.tot_len = htons(sizeof(struct iphdr) + sizeof(struct udphdr) + packet->packet_size);
    ippkt->ip.ihl = sizeof(struct iphdr) >> 2;
    ippkt->ip.version = 4;
    ippkt->ip.ttl = ttl;
    if(packet->tos >= 0) {
        ippkt->ip.tos = packet->tos;
    }
    ippkt->ip.check = checksum(&ippkt->ip, sizeof(struct iphdr));
}

static int __sys_send_dhcp_packet(dhcp_packet_t* packet,
                                  unsigned short sport,
                                  unsigned short dport,
                                  int intf,
                                  uint32_t saddr,
                                  uint32_t daddr,
                                  uint8_t ttl,
                                  unsigned char chaddr[CHADDR_LEN]) {
    int retval = -1;
    int ret = -1;
    int fd;
    struct sockaddr_ll dest;
    struct ippacket ippkt;

    when_null_trace(packet, exit, ERROR, "Packet pointer is NULL");
    SAH_TRACEZ_INFO(ME, "Sent DHCP Packet");

    fd = socket(PF_PACKET, SOCK_DGRAM, htons(ETH_P_IP));
    when_true_trace(fd < 0, exit, ERROR, "Socket failed[%s]", strerror(errno));

    memset(&dest, 0, sizeof(dest));
    dest.sll_family = AF_PACKET;
    dest.sll_protocol = htons(ETH_P_IP);
    dest.sll_ifindex = intf;
    dest.sll_halen = 6;
    memcpy(dest.sll_addr, chaddr, 6);

    ret = bind(fd, (struct sockaddr*) &dest, sizeof(dest));
    when_true_trace(ret < 0, exit_fd, ERROR, "Bind failed[%s]", strerror(errno));
    SAH_TRACEZ_INFO(ME, "Bind to interface[%d] succeeded", packet->other.sin_intf);

    if(packet->priority >= 0) {
        ret = setsockopt(fd, SOL_SOCKET, SO_PRIORITY, &packet->priority, sizeof(int));
        when_failed_trace(ret, exit_fd, ERROR, "Setsockopt failed[%s]", strerror(errno));
    }
    SAH_TRACEZ_INFO(ME, "Setsockopt priority[%d] succeeded", packet->priority);

    __sys_create_ip_packet(&ippkt, packet, saddr, daddr, sport, dport, ttl);

    retval = sendto(fd,
                    &ippkt,
                    sizeof(struct iphdr) + sizeof(struct udphdr) + packet->packet_size,
                    0,
                    (struct sockaddr*) &dest, sizeof(dest));
    when_true_trace(retval == -1, exit_fd, ERROR, "Sendto failed[%s]", strerror(errno));
    SAH_TRACEZ_INFO(ME, "Sent nr of bytes[%d] successfully", retval);

exit_fd:
    close(fd);
exit:
    return (retval == -1) ? 0 : 1;
}

int sys_send_dhcp_packet(int s, dhcp_packet_t* packet, uint32_t local_ip) {
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Send packet to ip addr[%x], socket[%d]", (unsigned int) htonl(local_ip), s);

    if((packet->other.sin_intf != DHCP_INTF_ANY) &&
       (local_ip == INADDR_ANY) &&
       (packet->other.sin_addr.s_addr == htonl(INADDR_BROADCAST))) {
        // DDW: send dhcp packet to raw socket if local_ip = 0.0.0.0 to avoid picking another IP address automatically (e.g. 192.168.1.1)
        // Other conditions: destination IP address must be broadcast (otherwise MAC resolving necessary)
        // and the outgoing interface has to be known (obviously).
        unsigned char addr[CHADDR_LEN] = {0};

        memset(addr, 0xff, 6);
        retval = __sys_send_dhcp_packet(packet,
                                        DHCLIENT_PORT,
                                        DHSERVER_PORT,
                                        packet->other.sin_intf,
                                        INADDR_ANY,
                                        INADDR_BROADCAST,
                                        64,
                                        addr);
    } else {
        if((packet->tos >= 0) &&
           (setsockopt(s, IPPROTO_IP, IP_TOS, &packet->tos, sizeof(packet->tos)) != 0)) {
            SAH_TRACEZ_ERROR(ME, "setsockopt failed[%s]", strerror(errno));
        }
        if((packet->priority >= 0) &&
           (setsockopt(s, SOL_SOCKET, SO_PRIORITY, &packet->priority, sizeof(int)) != 0)) {
            SAH_TRACEZ_ERROR(ME, "setsockopt failed[%s]", strerror(errno));
        }
        if(packet->ifindex != 0) {
            struct sockaddr_ll sin;
            struct ippacket ippkt;

            __sys_create_ip_packet(&ippkt,
                                   packet,
                                   local_ip,
                                   packet->other.sin_addr.s_addr,
                                   DHCLIENT_PORT,
                                   DHSERVER_PORT,
                                   128);

            sin.sll_ifindex = packet->ifindex;
            sin.sll_halen = ETH_ALEN;
            sin.sll_protocol = htons(ETH_P_IP);
            memcpy(&sin.sll_addr, &packet->sll_addr, 6);

            retval = sendto(s,
                            &ippkt,
                            sizeof(struct iphdr) + sizeof(struct udphdr) + packet->packet_size,
                            0,
                            (struct sockaddr*) &sin,
                            sizeof(sin));
            if(retval == -1) {
                SAH_TRACEZ_ERROR(ME, "sendto failed[%s]", strerror(errno));
                return 0;
            }
        } else {
            // send packet normally
            retval = sendto(s,
                            (char*) &packet->raw, packet->packet_size,
                            0,
                            (struct sockaddr*) &packet->other,
                            sizeof(struct sockaddr_in));
            if(retval == -1) {
                SAH_TRACEZ_ERROR(ME, "sendto failed[%s]", strerror(errno));
                return 0;
            }
        }
        SAH_TRACEZ_INFO(ME, "send packet succeeded[%d]", retval);
    }

    return (retval == -1) ? 0 : 1;
}

// check the UDP checksum of a received packet, return 1(true) if ok, 0 if not.
static int sys_recv_checksum(struct iphdr* iphdr, struct udphdr* udph) {
    struct pheader* psh;
    unsigned short packet_csum = 0;
    unsigned int psize = 0;
    char* pseudogram = NULL;
    unsigned short csum = 0;
    int ret = 1;

#ifdef __FAVOR_BSD
    psize = sizeof(struct pheader) + ntohs(udph->uh_ulen);
    packet_csum = ntohs(udph->uh_sum);
    udph->uh_sum = htons(0);
#else
    psize = sizeof(struct pheader) + ntohs(udph->len);
    packet_csum = ntohs(udph->check);
    udph->check = htons(0);
#endif

    when_true_trace(packet_csum == 0, exit, NOTICE, "Checksum is 0, ignore it");
    when_true_trace(psize < sizeof(struct pheader) + sizeof(struct udphdr), exit, ERROR, "Malformed UDP packet");

    pseudogram = (char*) calloc(1, psize);
    when_null_trace(pseudogram, exit, ERROR, "Failed to allocate pseudogram");
    psh = (struct pheader*) pseudogram;
    psh->src = iphdr->saddr;
    psh->dst = iphdr->daddr;
    psh->placeholder = 0;
    psh->protocol = IPPROTO_UDP;
#ifdef __FAVOR_BSD
    psh->len = udph->uh_ulen;
#else
    psh->len = udph->len;
#endif
    memcpy(pseudogram + sizeof(struct pheader), (char*) udph, ntohs(psh->len));
    SAH_TRACEZ_INFO(ME, "Header len[%zu], (udp + data len)[%d]", sizeof(struct pheader), ntohs(psh->len));

    csum = checksum(pseudogram, psize);
    free(pseudogram);
    when_true_trace(ntohs(csum) == packet_csum, exit, NOTICE, "Checksum match[0x%x]", ntohs(csum));

    SAH_TRACEZ_NOTICE(ME, "Checksum does not match[0x%x - 0x%x], ignore packet.", ntohs(csum), packet_csum);
    ret = 0;

exit:
    return ret;
}

int sys_recv_dhcp_packet_raw(int s, dhcp_packet_t* packet, unsigned char bootp_opcode) {
    struct sockaddr_ll from;
    socklen_t addr_len = sizeof(from);
    int buff_siz = 0;
    char* buff = NULL;
    int n = 0;
    struct iphdr* iphdr = NULL;
    struct udphdr* udph = NULL;
    uint16_t iphdr_len = 0;
    uint32_t packet_size = 0;
    int csum_ok = 0;
    int ret = -1;

    when_null_trace(packet, out_nofree, ERROR, "Packet is NULL");
    buff_siz = sizeof(packet->raw) + MAX_IPHDR_LEN + UDPHDR_LEN; //room for IP-header

    buff = (char*) calloc(1, buff_siz);
    when_null_trace(buff, out_nofree, ERROR, "Calloc failed");

    memset(&from, 0, addr_len);
    n = recvfrom(s, buff, buff_siz, 0, (struct sockaddr*) &from, &addr_len);
    when_true_trace(n < 0, out, ERROR, "Error in recvfrom[%s]", strerror(errno));
    when_true_trace(n == 0, out, WARNING, "Recvfrom perfomed an orderly shutdown");

    iphdr = (struct iphdr*) buff;
    udph = (struct udphdr*) (buff + iphdr->ihl * 4);
    iphdr_len = iphdr->ihl;
    SAH_TRACEZ_INFO(ME, "Received packet size[%d], ip header len[%d]", n, iphdr_len);

    // check UDP checksum, using a pseudo header
    csum_ok = sys_recv_checksum(iphdr, udph);
    when_true_trace(csum_ok == 0, out, WARNING, "Checksum error");

    packet_size = n - (UDPHDR_LEN + (iphdr_len * 4));
    if(packet_size > sizeof(packet->raw)) {
        SAH_TRACEZ_WARNING(ME, "Received packet size[%d] bigger than dhcp_raw_packet_t size[%zu]",
                           packet_size, sizeof(packet->raw));
        packet->packet_size = sizeof(packet->raw);
    } else {
        packet->packet_size = packet_size;
    }

    memcpy(&packet->raw, buff + UDPHDR_LEN + (iphdr_len * 4), packet->packet_size);

    // copy mac address, that's why we do this raw socket magic
    packet->sll_halen = from.sll_halen;
    memcpy(packet->sll_addr, from.sll_addr, from.sll_halen);

    packet->other.sin_intf = from.sll_ifindex;
    packet->saddr = iphdr->saddr;
    SAH_TRACEZ_INFO(ME, "Received packet ifindex[%d], saddr[%lx]",
                    packet->other.sin_intf, (unsigned long) ntohl(packet->saddr));

    when_true_trace(packet->raw.opcode != bootp_opcode, out, ERROR,
                    "Packet opcode(%d != BOOTREQUEST) is wrong", packet->raw.opcode);

    when_true(packet->raw.hlen > CHADDR_LEN, out);

    // scan options
    parse_dhcpoptions(packet);

    // extract DHCP message type
    if(packet->options_valid && (packet->dhcp_option[TAG_DHCP_MSG_TYPE].len == 1)) {
        packet->packet_type = (dhcp_msg_type_t) *(packet->dhcp_option[TAG_DHCP_MSG_TYPE].value_ptr);
    }
    ret = 0;

out:
    free(buff);
out_nofree:
    return ret;
}
