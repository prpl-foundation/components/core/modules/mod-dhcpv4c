/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include "debug/sahtrace.h"
#include "debug/sahtrace_macros.h"
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>

#include "dhcp_interface.h"
#include "dhcp_lease.h"
#include "dhcp_util.h"
#include "dhcp_packet.h"
#include "dhcp_state.h"
#include "mod_dhcpv4c.h"
#include "dhcp_stat.h"

static amxc_llist_t interfaces;

static void dhcp_interface_it_delete(amxc_llist_it_t* it) {
    dhcp_interface_t* intf = amxc_llist_it_get_data(it, dhcp_interface_t, it);
    dhcp_interface_delete(&intf);
}

int dhcp_interface_cleanup(void) {
    amxc_llist_clean(&interfaces, dhcp_interface_it_delete);
    return 0;
}

int dhcp_interface_new(dhcp_interface_t** intf, const char* name, const char* path) {
    int ret = -1;

    when_null_trace(intf, exit, ERROR, "Intf pointer is NULL");
    when_str_empty_trace(name, exit, ERROR, "Empty name is given");
    when_str_empty_trace(path, exit, ERROR, "Empty path is given");

    *intf = (dhcp_interface_t*) calloc(1, sizeof(dhcp_interface_t));
    when_null_trace(*intf, exit, ERROR, "Failed to allocate dhcp interface");

    strncpy((*intf)->name, name, sizeof((*intf)->name) - 1);
    (*intf)->intf_path = strdup(path);
    amxc_llist_it_init(&(*intf)->it);
    amxc_llist_append(&interfaces, &(*intf)->it);
    (*intf)->dhcp_sock = -1;
    (*intf)->dhcp_raw_sock = -1;

    ret = 0;

exit:
    return ret;
}

void dhcp_interface_delete(dhcp_interface_t** intf) {
    when_null(intf, exit);
    when_null(*intf, exit);

    amxc_llist_it_take(&(*intf)->it);
    dhcp_lease_delete(&(*intf)->lease);
    free((*intf)->intf_path);
    free(*intf);
    *intf = NULL;

exit:
    return;
}

dhcp_interface_t* dhcp_interface_find(const char* name) {
    dhcp_interface_t* intf = NULL;

    when_str_empty_trace(name, exit, ERROR, "Given name is empty");

    amxc_llist_iterate(it, &interfaces) {
        intf = amxc_llist_it_get_data(it, dhcp_interface_t, it);
        when_true(strcmp(name, intf->name) == 0, exit);
    }
    intf = NULL;

exit:
    return intf;
}

dhcp_interface_t* dhcp_interface_find_by_path(const char* path) {
    dhcp_interface_t* intf = NULL;

    when_str_empty_trace(path, exit, ERROR, "Given path is empty");

    amxc_llist_iterate(it, &interfaces) {
        intf = amxc_llist_it_get_data(it, dhcp_interface_t, it);
        when_true(strcmp(path, intf->intf_path) == 0, exit);
    }
    intf = NULL;

exit:
    return intf;
}

dhcp_interface_t* dhcp_interface_find_by_id(unsigned long id) {
    dhcp_interface_t* intf = NULL;

    amxc_llist_iterate(it, &interfaces) {
        intf = amxc_llist_it_get_data(it, dhcp_interface_t, it);
        when_true(if_nametoindex(intf->name) == id, exit);
    }
    intf = NULL;

exit:
    return intf;
}

int dhcp_interface_dhcp_raw_socket(dhcp_interface_t* intf) {
    int ret = -1;
    amxc_var_t data;
    amxc_var_t rv;

    amxc_var_init(&data);
    amxc_var_init(&rv);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    when_null_trace(intf, exit, ERROR, "Intf is NULL");

    intf->dhcp_raw_sock = dhcp_util_create_raw_socket(intf->name, DHCLIENT_PORT);

    if(intf->dhcp_raw_sock < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to create raw dhcp socket");
        intf->dhcp_raw_sock = -1;
        goto exit;
    }

    // Add fd to amx eventloop
    amxc_var_add_key(fd_t, &data, "fd", intf->dhcp_raw_sock);
    amxc_var_add_key(cstring_t, &data, "handler", "raw-sock-handler");
    amxc_var_add_key(cstring_t, &data, "intf_name", intf->name);
    if(amxm_execute_function(NULL, MOD_CORE, "event-loop-add-fd", &data, &rv) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add fd to event loop");
        close(intf->dhcp_raw_sock);
        intf->dhcp_raw_sock = -1;
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "[%s], Successfully opened raw listening socket[%d]", intf->name, intf->dhcp_raw_sock);
    ret = 0;

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&rv);
    return ret;
}

void dhcp_interface_dhcp_raw_socket_stop(dhcp_interface_t* intf) {
    amxc_var_t data;
    amxc_var_t rv;

    amxc_var_init(&data);
    amxc_var_init(&rv);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    when_null_trace(intf, exit, ERROR, "Interface is NULL");
    when_true_trace(intf->dhcp_raw_sock == -1, exit, INFO, "Raw dhcp socket is already closed");

    // remove fd from amx eventloop
    amxc_var_add_key(fd_t, &data, "fd", intf->dhcp_raw_sock);
    amxc_var_add_key(cstring_t, &data, "handler", "raw-sock-handler");
    amxc_var_add_key(cstring_t, &data, "intf_name", intf->name);
    if(amxm_execute_function(NULL, MOD_CORE, "event-loop-rm-fd", &data, &rv) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to remove fd from event loop");
    }

    int ret = close(intf->dhcp_raw_sock);
    (void) ret; // use it on sah trace not compiled in
    when_true_trace(ret < 0, exit, ERROR, "Closing raw socket failed: %s", strerror(errno));
    intf->dhcp_raw_sock = -1;

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&rv);
    return;
}

void dhcp_interface_dhcp_raw_read_handler(amxc_var_t* data) {
    dhcp_packet_t packet;
    dhcp_lease_t* lease = NULL;
    dhcp_interface_t* intf = NULL;
    unsigned long xid = 0;

    when_null_trace(data, exit, ERROR, "Data is NULL");
    intf = dhcp_interface_find(GET_CHAR(data, "intf_name"));
    when_null_trace(intf, exit, ERROR, "No interface found");
    lease = intf->lease;
    when_null_trace(lease, exit, ERROR, "No lease found");

    dhcp_packet_init(&packet);

    if(sys_recv_dhcp_packet_raw(intf->dhcp_raw_sock, &packet, BOOTREPLY) != 0) {
        // TODO check for corrupt packet & update stats
        goto exit;
    }

    // Check xid
    xid = ntohl(packet.raw.xid);
    when_false_trace(xid == lease->xid, exit, WARNING,
                     "XID mismatch: received %lu, expected %lu", xid, lease->xid);

    when_false_trace(lease->flags.flag_admin_enabled == 1, exit, INFO,
                     "[%s] Lease is not enabled", lease->id);

    if(packet.packet_type == BOOTP) {
        // Bootp packet received
        dhcp_state_on_bootp(lease, &packet);
    } else {
        // DHCP packet received, store server mac address in the lease data
        when_true_trace(packet.sll_halen > ADDR_MAC_LEN, exit, ERROR,
                        "Received mac address length (%hu) > %d", packet.sll_halen, ADDR_MAC_LEN);
        memcpy(lease->server_lladdr, packet.sll_addr, packet.sll_halen);
        dhcp_state_on_dhcp(lease, &packet);
    }
    if((packet.other.sin_family == AF_INET) && ((packet.packet_size < DHCP_MIN_SIZE) || (packet.raw.opcode != BOOTREQUEST))) {
        /* don't count BOOTREQUEST packets as corrupt packets */
        dhcp_stat_inc_counter(DHCLIENT_CNT_NO_CORRUPT);
    }

exit:
    return;
}

int dhcp_interface_dhcp_socket(dhcp_interface_t* intf) {
    int ret = -1;
    int flag = 1;
    int rcvbuf = 0;
    int err = -1;
    struct sockaddr_in local;
    struct ifreq ifreq;

    memset(&ifreq, 0, sizeof(struct ifreq));
    memset(&local, 0, sizeof(local));
    when_null_trace(intf, exit_no_sock, ERROR, "Intf pointer is NULL");

    intf->dhcp_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    when_true_trace(intf->dhcp_sock < 0, exit_no_sock, ERROR, "[%s] Socket failed[%s]",
                    intf->name, strerror(errno));

    snprintf(ifreq.ifr_name, IFNAMSIZ, "%s", intf->name);

    err = setsockopt(intf->dhcp_sock, SOL_SOCKET, SO_BINDTODEVICE, (char*) &ifreq, sizeof(ifreq));
    when_true_trace(err < 0, exit, ERROR, "[%s] Setsockopt SO_BINDTODEVICE failed[%s]",
                    intf->name, strerror(errno));

    local.sin_family = AF_INET;
    local.sin_addr.s_addr = htonl(INADDR_ANY);
    local.sin_port = htons(DHCLIENT_PORT);

    err = bind(intf->dhcp_sock, (struct sockaddr*) &local, sizeof(local));
    when_true_trace(err < 0, exit, ERROR, "[%s] Bind failed[%s]",
                    intf->name, strerror(errno));

    err = setsockopt(intf->dhcp_sock, SOL_SOCKET, SO_BROADCAST, (char*) &flag, sizeof(flag));
    when_true_trace(err < 0, exit, ERROR, "[%s] Setsockopt SO_BROADCAST failed[%s]",
                    intf->name, strerror(errno));

    err = setsockopt(intf->dhcp_sock, SOL_IP, IP_PKTINFO, (char*) &flag, sizeof(flag));
    when_true_trace(err < 0, exit, ERROR, "[%s] Setsockopt IP_PKTINFO failed[%s]",
                    intf->name, strerror(errno));

    err = setsockopt(intf->dhcp_sock, SOL_SOCKET, SO_RCVBUF, &rcvbuf, sizeof(rcvbuf));
    when_true_trace(err < 0, exit, ERROR, "[%s] Setsockopt SO_RCVBUF failed[%s]",
                    intf->name, strerror(errno));

    err = fcntl(intf->dhcp_sock, F_SETFL, O_NONBLOCK);
    when_true_trace(err < 0, exit, ERROR, "[%s] Cannot set the socket non-blocking[%s]",
                    intf->name, strerror(errno));

    SAH_TRACEZ_INFO(ME, "[%s] Socket successfully created[%d]", intf->name, intf->dhcp_sock);
    ret = 0;

exit:
    if(ret != 0) {
        close(intf->dhcp_sock);
        intf->dhcp_sock = -1;
    }
exit_no_sock:
    return ret;
}

void dhcp_interface_dhcp_socket_stop(dhcp_interface_t* intf) {
    when_null_trace(intf, exit, ERROR, "Interface is NULL");
    when_true_trace(intf->dhcp_sock == -1, exit, INFO, "Dhcp socket is already closed");

    close(intf->dhcp_sock);
    intf->dhcp_sock = -1;

exit:
    return;
}

int dhcp_interface_dhcp_socket_settos(dhcp_interface_t* intf) {
    int ret = -1;
    int s = 0;
    int tos = 0;
    int err = 0;

    when_null_trace(intf, exit, ERROR, "Intf is NULL");

    s = intf->dhcp_sock >= 0 ? intf->dhcp_sock : intf->dhcp_raw_sock;
    when_false_trace(s >= 0, exit, WARNING, "[%s] No socket found", intf->name);

    tos = intf->tos;
    when_true_trace(tos > 255, exit, WARNING, "[%s] Tos value[%d] is out of range",
                    intf->name, tos);

    err = setsockopt(s, IPPROTO_IP, IP_TOS, &tos, sizeof(tos));
    when_true_trace(err < 0, exit, WARNING, "[%s] Set Tos[%d] failed[%s]",
                    intf->name, tos, strerror(errno));

    SAH_TRACEZ_INFO(ME, "[%s] Tos value[%d] set", intf->name, tos);
    ret = 0;

exit:
    return ret;
}
