/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "debug/sahtrace.h"
#include "debug/sahtrace_macros.h"
#include <amxc/amxc_macros.h>

#include "mod_dhcpv4c.h"
#include "dhcp_util.h"
#include "dhcp_txoption.h"
#include "dhcp_options.h"

amxc_var_t* option_list_find_tag(amxc_var_t* option_list, unsigned char tag) {
    amxc_var_t* option = NULL;

    when_null_trace(option_list, exit, ERROR, "Option list is NULL");

    amxc_var_for_each(var, option_list) {
        if(tag == GET_UINT32(var, "Tag")) {
            option = var;
            goto exit;
        }
    }

exit:
    return option;
}

int txoption_write_option(dhcp_packet_t* packet, unsigned char** poption, amxc_var_t* option) {
    unsigned char* buf = NULL;
    uint32_t len = 0;
    unsigned char tag = 0;
    int ret = -1;

    when_null_trace(packet, exit, ERROR, "Packet is NULL");
    when_null_trace(poption, exit, ERROR, "Poption is NULL");
    when_null_trace(option, exit, ERROR, "Option is NULL");

    tag = GET_UINT32(option, "Tag");
    when_false_trace(GET_BOOL(option, "Enable"), exit, INFO, "Option is not enabled");

    buf = hex_to_bin(GET_CHAR(option, "Value"), &len);
    when_null_trace(buf, exit, ERROR, "Could not write option %u to buffer", tag);

    write_optionraw(packet, poption, tag, buf, len);

    ret = 0;

exit:
    free(buf);
    return ret;
}
