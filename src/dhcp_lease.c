/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/ioctl.h>

#include "debug/sahtrace.h"
#include "debug/sahtrace_macros.h"
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include "dhcp_lease.h"
#include "dhcp_util.h"
#include "dhcp_state.h"
#include "dhcp_txoption.h"
#include "mod_dhcpv4c.h"

#define DEFAULT_BROADCAST_FLAGS 0xFFFF
#define RENEW_NROFRETRIES 3

// define a default path to export lease data
#define DEFAULT_EXPORT_FILENAME "/etc/config/tr181-dhcpv4client"

// By default set the broadcast flag so that it does not break
// the scenario on NAT enabled interfaces. Currently NAT is unable to handle
// unicast DHCP messages in response to DHCP discover and request messages
static dhcp_lease_flags_t def_lease_flags = {0, 0, 1, 0, 0, 0, 0};

static void dhcp_retransmit_timer_cb(UNUSED amxp_timer_t* timer, void* priv) {
    dhcp_lease_t* lease = (dhcp_lease_t*) priv;
    when_null_trace(lease, exit, WARNING, "No private data set for retransmit timer");

    dhcp_state_on_timeout(lease);
exit:
    return;
}

static void dhcp_renew_timer_cb(UNUSED amxp_timer_t* timer, void* priv) {
    dhcp_lease_t* lease = (dhcp_lease_t*) priv;
    when_null_trace(lease, exit, WARNING, "No private data set for renew timer");

    dhcp_state_on_timeout_renew(lease);
exit:
    return;
}

static void dhcp_rebind_timer_cb(UNUSED amxp_timer_t* timer, void* priv) {
    dhcp_lease_t* lease = (dhcp_lease_t*) priv;
    when_null_trace(lease, exit, WARNING, "No private data set for rebind timer");

    dhcp_state_on_timeout_rebind(lease);
exit:
    return;
}

// Calculate a random time to wait between t0 and t1
// t0, t1 are in seconds
// time_to_wait is in ms
static void dhcp_lease_random_waittime(struct timeval* tv, unsigned long t0, unsigned long t1) {
    const unsigned long rnd = ((unsigned long) rand()) << 16 | rand(); // in case rand() returns a 16-bit number
    const unsigned long time_to_wait = t0 * 1000 + rnd % ((t1 - t0) * 1000);

    when_null_trace(tv, exit, ERROR, "Timeval ptr is NULL");

    tv->tv_sec = time_to_wait / 1000;
    tv->tv_usec = time_to_wait % 1000;

exit:
    return;
}

static int dhcp_lease_init(dhcp_lease_t* lease, const char* id, dhcp_interface_t* intf) {
    int ret = -1;
    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_str_empty_trace(id, exit, ERROR, "Empty id is given");
    when_null_trace(intf, exit, ERROR, "Intf is NULL");

    memset(lease, 0, sizeof(dhcp_lease_t));

    snprintf(lease->ifname, sizeof(lease->ifname), "%s", intf->name);
    lease->ifindex = if_nametoindex(lease->ifname);
    when_true_trace(lease->ifindex == 0, exit, ERROR, "Interface not found %s", lease->ifname);
    lease->intf = intf;
    lease->first_run = 1;
    lease->state = dhcp_client_state_idle;
    lease->flags = def_lease_flags;
    lease->broadcast_flags = DEFAULT_BROADCAST_FLAGS;
    lease->ip_addr = INADDR_ANY;
    lease->netmask = INADDR_ANY;
    snprintf(lease->id, sizeof(lease->id), "%s", id);
    lease->server_ip = INADDR_BROADCAST;
    amxc_var_init(&lease->req_options);
    amxc_var_set_type(&lease->req_options, AMXC_VAR_ID_LIST);
    amxc_var_init(&lease->sent_options);
    amxc_var_set_type(&lease->sent_options, AMXC_VAR_ID_LIST);
    amxc_var_init(&lease->retransmission_strategy);
    lease->client_err = DHCLIENT_ERR_NONE;
    lease->tos = -1;
    lease->priority = -1;
    lease->renew_counter = -1;
    lease->raw_socket = false;
    lease->xid = 0;

    // timers
    amxp_timer_new(&lease->retransmit_timer, dhcp_retransmit_timer_cb, lease);
    amxp_timer_new(&lease->renew_timer, dhcp_renew_timer_cb, lease);
    amxp_timer_new(&lease->rebind_timer, dhcp_rebind_timer_cb, lease);

    ret = 0;

exit:
    return ret;
}

static uint16_t dhcp_lease_get_mtu(dhcp_lease_t* lease) {
    struct ifreq ifreq;
    uint16_t mtu = 0;
    int s = -1;
    int ret = -1;

    memset(&ifreq, 0, sizeof(struct ifreq));

    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_str_empty_trace(lease->ifname, exit, ERROR, "Lease->ifname is empty");

    s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    when_true_trace(s < 0, exit, ERROR, "Socket failed[%s]", strerror(errno));

    snprintf(ifreq.ifr_name, IFNAMSIZ, "%s", lease->ifname);
    ret = ioctl(s, SIOCGIFMTU, &ifreq);
    when_true_trace(ret == -1, exit, ERROR, "Failed to get mtu for interface %s", lease->ifname);

    mtu = ifreq.ifr_mtu;

exit:
    if(s >= 0) {
        close(s);
    }
    return mtu;
}

int dhcp_lease_new(dhcp_lease_t** lease, const char* id, dhcp_interface_t* intf) {
    int ret = -1;

    when_null_trace(lease, exit, ERROR, "Lease pointer is NULL");
    when_str_empty_trace(id, exit, ERROR, "Empty id is given");

    *lease = (dhcp_lease_t*) calloc(1, sizeof(dhcp_lease_t));
    when_null_trace(*lease, exit, ERROR, "Failed to allocate dhcp lease");

    if(dhcp_lease_init(*lease, id, intf) != 0) {
        free(*lease);
        *lease = NULL;
        goto exit;
    }

    ret = 0;

exit:
    return ret;
}

void dhcp_lease_delete(dhcp_lease_t** lease) {
    when_null(lease, exit);
    when_null(*lease, exit);

    amxc_var_clean(&(*lease)->req_options);
    amxc_var_clean(&(*lease)->sent_options);
    amxc_var_clean(&(*lease)->retransmission_strategy);
    free((*lease)->storage_path);
    free(*lease);
    *lease = NULL;

exit:
    return;
}

dhcp_lease_t* dhcp_lease_get(unsigned long ifindex, unsigned long xid) {
    dhcp_interface_t* intf = dhcp_interface_find_by_id(ifindex);
    dhcp_lease_t* lease = NULL;

    when_null_trace(intf, exit, INFO, "No interface found with id %lu", ifindex);
    if((intf->lease != NULL) && (intf->lease->xid == xid)) {
        SAH_TRACEZ_INFO(ME, "Found lease with xid %lu", xid);
        lease = intf->lease;
    }

exit:
    return lease;
}

void dhcp_lease_set_initial_wait(dhcp_lease_t* lease) {
    struct timeval now;

    when_null_trace(lease, exit, ERROR, "Lease is NULL");

    lease->nr_tries = 0; // means here: number of transmissions of DISCOVER

    // wait random time between 1 and 2 seconds (only first time)
    dhcp_lease_random_waittime(&(lease->timeout), 1, 2);
    get_timestamp(&now);
    timeradd(&now, &(lease->timeout), &(lease->deadline));
    set_timer(lease->retransmit_timer, &(lease->timeout));

exit:
    return;
}

int dhcp_lease_enable(dhcp_lease_t* lease) {
    int ret = -1;
    dhcp_interface_t* intf = NULL;

    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    intf = lease->intf;
    when_null_trace(intf, exit, ERROR, "No interface associated with this lease");

    lease->flags.flag_admin_enabled = 1;

    if(intf->dhcp_sock >= 0) {
        SAH_TRACEZ_INFO(ME, "DHCP Interface %s is already up", intf->name);
    } else {
        dhcp_interface_dhcp_raw_socket(intf);
        dhcp_interface_dhcp_socket(intf);
        dhcp_interface_dhcp_socket_settos(intf);
    }

    ret = 0;

exit:
    return ret;
}

int dhcp_lease_disable(dhcp_lease_t* lease) {
    dhcp_interface_t* intf = NULL;
    int ret = -1;

    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    intf = lease->intf;
    when_null_trace(intf, exit, ERROR, "No interface associated with this lease");

    if(intf->dhcp_sock < 0) {
        SAH_TRACEZ_INFO(ME, "[%s] Interface already down", intf->name);
        ret = 0;
        goto exit;
    }

    dhcp_interface_dhcp_socket_stop(intf);
    dhcp_interface_dhcp_raw_socket_stop(intf);

    lease->flags.flag_admin_enabled = 0;

    ret = 0;

exit:
    return ret;
}

void dhcp_lease_addr_update(dhcp_lease_t* lease, uint32_t ip_addr, uint32_t netmask) {
    struct in_addr addr;
    (void) addr; // use it on sah trace not compiled in

    when_null_trace(lease, exit, ERROR, "Lease is NULL");

    if(lease->ip_addr != ip_addr) {
        lease->ip_addr = ip_addr;
        addr.s_addr = ip_addr;
        SAH_TRACEZ_INFO(ME, "[%s] Set IP Address[%s]", lease->id, inet_ntoa(addr));
    }

    if(lease->netmask != netmask) {
        lease->netmask = netmask;
        addr.s_addr = netmask;
        SAH_TRACEZ_INFO(ME, "[%s] Set subnet mask[%s]", lease->id, inet_ntoa(addr));
    }

exit:
    return;
}

void dhcp_lease_config_req_ip_option(dhcp_lease_t* lease) {
    amxc_var_t* option = NULL;
    amxc_var_t value;
    char* hex_str = NULL;

    amxc_var_init(&value);

    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_true_trace(lease->ip_addr == 0, exit, INFO, "IP address is empty");

    hex_str = bin_to_hex((unsigned char*) &lease->ip_addr, sizeof(lease->ip_addr));
    amxc_var_set(cstring_t, &value, hex_str);

    option = option_list_find_tag(&lease->sent_options, TAG_REQ_IP);
    if(option == NULL) {
        option = amxc_var_add(amxc_htable_t, &lease->sent_options, NULL);
        amxc_var_add_key(uint32_t, option, "Tag", TAG_REQ_IP);
        amxc_var_add_key(bool, option, "Enable", true);
    }

    amxc_var_set_key(option, "Value", &value, AMXC_VAR_FLAG_COPY | AMXC_VAR_FLAG_UPDATE);

exit:
    amxc_var_clean(&value);
    free(hex_str);
    return;
}

void dhcp_lease_set_retransmission_params(dhcp_lease_t* lease, amxc_var_t* params) {
    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(params, exit, ERROR, "Params is NULL");

    amxc_var_convert(&lease->retransmission_strategy, GET_ARG(params, "RetransmissionStrategy"), AMXC_VAR_ID_LIST);
    lease->retransmission_step = 0;

    lease->retransmission_randomize = GET_UINT32(params, "RetransmissionRandomize");
    lease->default_renew_timeout = GET_INT32(params, "RetransmissionRenewTimeout");
    dhcp_lease_init_renew_counter(lease);

exit:
    return;
}

void dhcp_lease_init_renew_counter(dhcp_lease_t* lease) {
    when_null_trace(lease, exit, ERROR, "Lease is NULL");

    lease->nr_tries = 0;
    lease->retransmission_renew_timeout = lease->default_renew_timeout;
    if(lease->retransmission_renew_timeout > 0) {
        lease->renew_counter = RENEW_NROFRETRIES;
        SAH_TRACEZ_NOTICE(ME, "Set renew counter[%d]", lease->renew_counter);
    } else {
        lease->renew_counter = -1;
    }

exit:
    return;
}

void dhcp_lease_set_max_msg_size(dhcp_lease_t* lease) {
    amxc_var_t* option = NULL;
    amxc_var_t value;
    char* hex_str = NULL;
    uint16_t mtu = 0;
    uint16_t ns = 0;

    amxc_var_init(&value);
    when_null_trace(lease, exit, ERROR, "Lease is NULL");

    mtu = dhcp_lease_get_mtu(lease);
    when_true_trace(mtu <= (IPHDR_LEN + UDPHDR_LEN), exit, WARNING,
                    "[%s] Cannot configure max msg size, MTU is %u", lease->id, mtu);

    lease->max_msg_size = mtu - (IPHDR_LEN + UDPHDR_LEN);
    when_true_trace(lease->max_msg_size < DHCP_MIN_MAX_MSG_SIZE, exit, WARNING,
                    "[%s] Max msg size is %u, should be at least %u",
                    lease->id, lease->max_msg_size, DHCP_MIN_MAX_MSG_SIZE);
    ns = htons(lease->max_msg_size); // Ensure correct endianness
    hex_str = bin_to_hex((unsigned char*) &ns, sizeof(ns));
    amxc_var_set(cstring_t, &value, hex_str);

    option = option_list_find_tag(&lease->sent_options, TAG_DHCP_MAX_MSG_SIZE);
    if(option == NULL) {
        option = amxc_var_add(amxc_htable_t, &lease->sent_options, NULL);
        amxc_var_add_key(uint32_t, option, "Tag", TAG_DHCP_MAX_MSG_SIZE);
        amxc_var_add_key(bool, option, "Enable", true);
    }

    amxc_var_set_key(option, "Value", &value, AMXC_VAR_FLAG_COPY | AMXC_VAR_FLAG_UPDATE);

exit:
    amxc_var_clean(&value);
    free(hex_str);
    return;
}

void dhcp_lease_set_auth_params(dhcp_lease_t* lease, amxc_var_t* params) {
    const char* auth_info = NULL;
    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(params, exit, ERROR, "Params is NULL");

    auth_info = GET_CHAR(params, "AuthenticationInformation");
    when_null_trace(auth_info, exit, ERROR, "Auth info is NULL");

    strncpy(lease->authinfo, auth_info, AUTHINFO_LEN);
    lease->check_authentication = GET_BOOL(params, "CheckAuthentication");

exit:
    return;
}

void dhcp_lease_set_storage_path(dhcp_lease_t* lease, const char* path) {
    when_null_trace(lease, exit, ERROR, "Lease is NULL");

    free(lease->storage_path);

    if(str_empty(path)) {
        lease->storage_path = strdup(DEFAULT_EXPORT_FILENAME);
    } else {
        lease->storage_path = strdup(path);
    }

    SAH_TRACEZ_INFO(ME, "[%s] Lease storage path is set to %s", lease->id, lease->storage_path);

exit:
    return;
}

void dhcp_lease_set_socket_opts(dhcp_lease_t* lease, amxc_var_t* params) {
    int dscp = 0;

    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(params, exit, ERROR, "Params is NULL");

    lease->priority = GET_INT32(params, "PriorityMark");

    dscp = GET_INT32(params, "DSCPMark");
    lease->tos = (dscp > 0) ? dscp << 2 : 0;

    if(lease->intf != NULL) {
        lease->intf->tos = lease->tos;
        dhcp_interface_dhcp_socket_settos(lease->intf);
    }

exit:
    return;
}

void dhcp_lease_set_broadcast_flag_params(dhcp_lease_t* lease, amxc_var_t* params) {
    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(params, exit, ERROR, "Params is NULL");

    lease->broadcast_flags = amxc_var_dyncast(uint16_t, GET_ARG(params, "BroadcastFlag"));

exit:
    return;
}
