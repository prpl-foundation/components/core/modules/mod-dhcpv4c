/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <net/if.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <linux/filter.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdint.h>

#include "debug/sahtrace.h"
#include "debug/sahtrace_macros.h"

#include "dhcp_util.h"
#include "mod_dhcpv4c.h"

uint32_t ip_to_classfull_mask(uint32_t ip) {
    uint32_t mask = 0;

    if(IN_CLASSA(ntohl(ip))) {
        mask = htonl(IN_CLASSA_NET);
    } else if(IN_CLASSB(ntohl(ip))) {
        mask = htonl(IN_CLASSB_NET);
    } else if(IN_CLASSC(ntohl(ip))) {
        mask = htonl(IN_CLASSC_NET);
    }

    return mask;
}

int dhcp_util_create_raw_socket(const char* ifname, unsigned int port) {
    int sock = -1;
    int ret = -1;
    int flag = 1;
    struct sockaddr_ll from;

    struct sock_filter filter_instr[] = {
        // check for udp dport 67
        BPF_STMT(BPF_LD | BPF_B | BPF_ABS, 9),
        BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, IPPROTO_UDP, 0, 4),  // UDP?
        BPF_STMT(BPF_LDX | BPF_B | BPF_MSH, 0),                  // moves index register to start of udp
        BPF_STMT(BPF_LD | BPF_H | BPF_IND, 2),                   // load dport in accumulator
        BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, port, 0, 1),         // dport == 67?
        BPF_STMT(BPF_RET | BPF_K, 0x0fffffff),                   // pass
        BPF_STMT(BPF_RET | BPF_K, 0),                            // reject
    };

    struct sock_fprog filter_prog = {
        .len = sizeof(filter_instr) / sizeof(filter_instr[0]),
        .filter = filter_instr,
    };

    when_str_empty_trace(ifname, exit, ERROR, "Ifname is empty");

    sock = socket(AF_PACKET, SOCK_DGRAM, htons(ETH_P_IP));
    when_true_trace(sock < 0, exit, ERROR, "Cannot open socket, [%s]", strerror(errno));
    SAH_TRACEZ_NOTICE(ME, "Open raw socket[%d]", sock);

    ret = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
    when_true_trace(ret < 0, exit, ERROR, "setsockopt(SO_REUSEADDR) failed[%s], close socket[%d]",
                    strerror(errno), sock);

    // For raw sockets we have to use bind to bind to a specific device, see man 7 packet
    memset(&from, 0, sizeof(struct sockaddr_ll));
    from.sll_family = AF_PACKET;
    from.sll_ifindex = if_nametoindex(ifname);
    from.sll_protocol = htons(ETH_P_IP);
    when_true_trace(from.sll_ifindex == 0, exit, ERROR,
                    "Failed to retrieve ifindex[%s] from intf[%s], close socket[%d]",
                    strerror(errno), ifname, sock);

    ret = bind(sock, (struct sockaddr*) &from, sizeof(struct sockaddr_ll));
    when_true_trace(ret < 0, exit, ERROR, "Bind failed[%s], close socket[%d]", strerror(errno), sock);

    ret = setsockopt(sock, SOL_SOCKET, SO_ATTACH_FILTER, &filter_prog, sizeof(filter_prog));
    when_true_trace(ret < 0, exit, ERROR, "Setsockopt(SO_ATTACH_FILTER) failed[%s], close socket[%d]",
                    strerror(errno), sock);

    flag = fcntl(sock, F_GETFL, 0);
    when_true_trace(flag == -1, exit, ERROR, "[%s] Cannot get the socket flags[%s]", ifname, strerror(errno));

    flag |= O_NONBLOCK;
    ret = fcntl(sock, F_SETFL, flag);
    when_true_trace(ret < 0, exit, ERROR, "[%s] Cannot set the socket non-blocking[%s]", ifname, strerror(errno));
    SAH_TRACEZ_NOTICE(ME, "[%s] Set socket non-blocking, flag[0x%x]", ifname, flag);

    ret = sock;

exit:
    if((ret < 0) && (sock >= 0)) {
        close(sock);
    }
    return ret;
}

char* bin_to_hex(unsigned char* bin, int len) {
    int i = 0;
    char* hex = NULL;

    when_null_trace(bin, exit, ERROR, "Bin is NULL");

    hex = (char*) calloc(1, (len * 2) + 1);
    when_null_trace(hex, exit, ERROR, "Failed to allocate hex buffer");

    for(i = 0; i < len; i++) {
        snprintf(&hex[i * 2], 3, "%02X", bin[i]);
    }

exit:
    return hex;
}

unsigned char* hex_to_bin(const char* hex, unsigned int* len) {
    unsigned int i = 0;
    size_t hex_len = 0;
    unsigned char* bin = NULL;

    when_null_trace(hex, exit, ERROR, "Hex is NULL");
    when_null_trace(len, exit, ERROR, "Len is NULL");
    hex_len = strlen(hex);
    when_true_trace((hex_len % 2) != 0, exit, ERROR, "String length is not a multiple of 2");

    bin = (unsigned char*) calloc(1, hex_len / 2);
    when_null_trace(bin, exit, ERROR, "Failed to allocate bin buffer");

    for(i = 0; i < hex_len; i += 2) {
        if(sscanf(hex + i, "%2hhx", &(bin[i / 2])) != 1) {
            free(bin);
            bin = NULL;
            goto exit;
        }
    }
    *len = hex_len / 2;

exit:
    return bin;
}

void get_timestamp(struct timeval* now) {
    struct timespec tv;
    if(now != NULL) {
        clock_gettime(CLOCK_MONOTONIC, &tv);
        now->tv_sec = tv.tv_sec;
        now->tv_usec = tv.tv_nsec / 1000;
    }
}

void set_timer(amxp_timer_t* timer, struct timeval* tv) {
    unsigned long ms = 0;

    when_null_trace(timer, exit, ERROR, "Timer is NULL");
    when_null_trace(tv, exit, ERROR, "Timeval pointer is NULL");

    ms = tv->tv_sec * 1000 + tv->tv_usec / 1000;
    SAH_TRACEZ_NOTICE(ME, "Start timer, expires in [%lus, %luus]", tv->tv_sec, tv->tv_usec);
    amxp_timer_start(timer, ms);

exit:
    return;
}

// Randomize time with +- 1 second, store the result in res
void randomize_time(struct timeval* time, struct timeval* res) {
    struct timeval tv;
    struct timeval tv1;
    uint32_t rnd = ((uint32_t) rand()) % 2048;

    when_null_trace(time, exit, ERROR, "Time is NULL");
    when_null_trace(res, exit, ERROR, "Res is NULL");

    tv.tv_sec = rnd / 1000;
    tv.tv_usec = rnd % 1000;

    if(time->tv_sec == INFINITE_DELAY) {
        res->tv_sec = INFINITE_DELAY;
        res->tv_usec = 0;
        goto exit;
    }

    if(time->tv_sec <= 1) {
        res->tv_sec = tv.tv_sec;
        res->tv_usec = tv.tv_usec;
    }

    tv1.tv_sec = time->tv_sec - 1;
    tv1.tv_usec = time->tv_usec;
    timeradd(&tv1, &tv, res);

exit:
    return;
}

void init_rand(void) {
    int fd = 0;
    int len = 0;
    unsigned int seed = 0;

    fd = open("/dev/urandom", O_RDONLY);
    if(fd >= 0) {
        SAH_TRACEZ_NOTICE(ME, "Using /dev/urandom for random seed");
        len = read(fd, (char*) &seed, sizeof(seed));
        close(fd);
    } else {
        len = 0;
    }

    if(len != sizeof(seed)) {
        // This is a backup when opening /dev/urandom fails
        struct timeval tv;
        gettimeofday(&tv, NULL);
        seed = tv.tv_usec;
        SAH_TRACEZ_NOTICE(ME, "Using usecs for random seed");
    }

    srand(seed);
    return;
}
