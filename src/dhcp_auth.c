/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>

#include "debug/sahtrace.h"
#include "debug/sahtrace_macros.h"
#include <amxc/amxc_macros.h>

#include "dhcp_auth.h"
#include "mod_dhcpv4c.h"

int dhcp_check_authinfo(dhcp_lease_t* lease, dhcp_packet_t* packet, bool mandatory) {
    int res = 0;
    int ret = -1;
    char auth_option[sizeof(struct authentication_info) + AUTHINFO_LEN + 1];
    struct authentication_info* info = (struct authentication_info*) auth_option;
    memset(auth_option, 0, sizeof(auth_option));

    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(packet, exit, ERROR, "Packet is NULL");

    if(lease->authinfo[0] == 0) {
        SAH_TRACEZ_INFO(ME, "No authentication information configured on[%s]", lease->id);
        ret = mandatory ? -1 : 0;
        goto exit;
    }

    if(packet->dhcp_option[TAG_DHCP_MSG_AUTH].len == 0) {
        SAH_TRACEZ_WARNING(ME, "Authentication information configured on [%s] but not received", lease->id);
        ret = mandatory ? -1 : 0;
        goto exit;
    }

    READ_OPTION(packet->dhcp_option[TAG_DHCP_MSG_AUTH], auth_option, sizeof(auth_option));
    auth_option[sizeof(struct authentication_info) + AUTHINFO_LEN] = '\0'; // guarantee null-termination

    // Check Replay Detection
    switch(info->rdm) {
    case 0:
        // Replay detection field must be a monotonically increasing counter
        res = check_replay_detection(lease->replay_detection, info);
        if(res >= 0) {
            SAH_TRACEZ_WARNING(ME, "Authentication: Replay detection error");
        } else {
            unsigned char* rd = (unsigned char*) &lease->replay_detection;
            (void) rd; // use it when sah trace is not compiled in
            memcpy((char*) &lease->replay_detection, (char*) &(info->replay_detection), sizeof(lease->replay_detection));
            SAH_TRACE_INFO("[%02X][%02X][%02X][%02X][%02X][%02X][%02X][%02X]",
                           rd[0], rd[1], rd[2], rd[3], rd[4], rd[5], rd[6], rd[7]);
        }
        break;
    default:
        SAH_TRACEZ_WARNING(ME, "Authentication: RDM method[%d] not supported", info->rdm);
    }

    switch(info->protocol) {
    case 0:
        return check_configuration_token(lease->authinfo, info);
    case 1:
        return check_delayed_authentication(info);
    default:
        SAH_TRACEZ_WARNING(ME, "Authentication: Protocol[%d] not supported", info->protocol);
    }

exit:
    return ret;
}

