/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxm/amxm.h>

#include "mod_dhcpv4c.h"
#include "dhcp_interface.h"
#include "dhcp_lease.h"
#include "dhcp_state.h"
#include "dhcp_util.h"
#include "dhcp_txoption.h"
#include "dhcp_stat.h"

static int dhcpv4c_set_state_start(UNUSED const char* function_name,
                                   amxc_var_t* args,
                                   UNUSED amxc_var_t* ret) {
    dhcp_interface_t* intf = NULL;
    const char* intf_name = GET_CHAR(args, "IfName");
    const char* intf_path = GET_CHAR(args, "Interface");
    int rv = -1;
    int ifindex = 0;
    amxc_var_t* req_ip = NULL;

    when_str_empty_trace(intf_name, exit, ERROR, "Given interface name is empty");
    when_str_empty_trace(intf_path, exit, ERROR, "Given interface path is empty");
    ifindex = if_nametoindex(intf_name);
    when_true_trace(ifindex == 0, exit, ERROR, "Interface not found: %s", intf_name);

    // Find interface by path
    intf = dhcp_interface_find_by_path(intf_path);
    if((intf != NULL) && (strcmp(intf->name, intf_name) != 0)) {
        dhcp_interface_delete(&intf);
    }
    if(intf == NULL) {
        dhcp_interface_new(&intf, intf_name, intf_path);
    }
    when_null_trace(intf, exit, ERROR, "Could not get/create dhcp interface for intf %s", intf_path);

    // If present, find lease and check ifindex
    if((intf->lease != NULL) && (intf->lease->ifindex != ifindex)) {
        dhcp_lease_delete(&intf->lease);
    }
    // If not, create new lease
    if(intf->lease == NULL) {
        dhcp_lease_new(&intf->lease, intf_name, intf);
    }
    when_null_trace(intf->lease, exit, ERROR, "Could not get/create dhcp lease for intf %s", intf_name);

    // set ReqOptions
    amxc_var_copy(&intf->lease->req_options, GET_ARG(args, "ReqOption"));

    // set SentOptions
    // First check if requested IP is present in current options
    req_ip = option_list_find_tag(&intf->lease->sent_options, TAG_REQ_IP);
    if(req_ip != NULL) {
        // If yes, check if it is present in the given options
        if(option_list_find_tag(GET_ARG(args, "SentOption"), TAG_REQ_IP) == NULL) {
            // If not, use the new options but keep the requested IP
            amxc_var_take_it(req_ip);
        } else {
            req_ip = NULL;
        }
    }
    amxc_var_copy(&intf->lease->sent_options, GET_ARG(args, "SentOption"));
    if(req_ip != NULL) {
        amxc_var_set_index(&intf->lease->sent_options, -1, req_ip, AMXC_VAR_FLAG_DEFAULT);
    }

    // Set max msg size
    if(option_list_find_tag(&intf->lease->sent_options, TAG_DHCP_MAX_MSG_SIZE) == NULL) {
        dhcp_lease_set_max_msg_size(intf->lease);
    }

    dhcp_lease_set_retransmission_params(intf->lease, args);
    dhcp_lease_set_auth_params(intf->lease, args);
    dhcp_lease_set_broadcast_flag_params(intf->lease, args);
    dhcp_lease_set_storage_path(intf->lease, GET_CHAR(args, "storage-path"));
    intf->lease->release_reboot = GET_BOOL(args, "ReleaseOnReboot");

    // apply config and start the actual dhcp process
    dhcp_lease_enable(intf->lease);
    dhcp_lease_set_socket_opts(intf->lease, args);
    dhcp_state_start(intf->lease);

    rv = 0;

exit:
    return rv;
}

static int dhcpv4c_set_state_stop(UNUSED const char* function_name,
                                  amxc_var_t* args,
                                  UNUSED amxc_var_t* ret) {
    dhcp_interface_t* intf = NULL;
    const char* intf_path = GET_CHAR(args, "Interface");
    const char* ifname = GET_CHAR(args, "IfName");
    int rv = -1;

    when_str_empty_trace(intf_path, exit, ERROR, "Given interface path is empty");

    // Find interface by path, IfName can be empty at this point
    intf = dhcp_interface_find_by_path(intf_path);
    when_null_trace(intf, exit, ERROR, "Could not get dhcp interface for intf %s", intf_path);
    when_null_trace(intf->lease, exit, ERROR, "Could not get dhcp lease for intf %s", intf_path);

    // If IfName is empty, the network interface is gone
    // Set release_reboot_done to false to ensure that the lease file is not removed
    // when going to IDLE state
    if(str_empty(ifname)) {
        intf->lease->release_reboot_done = false;
    }

    dhcp_state_release(intf->lease);
    dhcp_lease_disable(intf->lease);

    rv = 0;

exit:
    return rv;
}

static int dummy_func(UNUSED const char* function_name,
                      UNUSED amxc_var_t* args,
                      amxc_var_t* ret) {
    amxc_var_set(int32_t, ret, 0);
    return 0;
}

static int dhcpv4c_renew(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {

    dhcp_interface_t* intf = NULL;
    const char* intf_name = amxc_var_constcast(cstring_t, args);
    int rv = -1;

    // Find iface by Ifname
    intf = dhcp_interface_find(intf_name);
    when_null_trace(intf, exit, ERROR, "No dhcp interface found");
    when_null_trace(intf->lease, exit, ERROR, "No dhcp lease found");

    if(intf->lease->state == dhcp_client_state_bound) {
        intf->lease->renew_time.tv_sec = 0;
        intf->lease->renew_time.tv_usec = 500;
        set_timer(intf->lease->renew_timer, &intf->lease->renew_time);
    }
    rv = 0;

exit:
    return rv;
}

static int dhcpv4c_release(UNUSED const char* function_name,
                           amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    int rv = -1;
    dhcp_interface_t* intf = NULL;
    const char* intf_name = NULL;

    when_null_trace(args, exit, ERROR, "No data given for release");

    intf_name = GET_CHAR(args, NULL);
    when_str_empty_trace(intf_name, exit, ERROR, "No interface name given");

    intf = dhcp_interface_find(intf_name);
    when_null_trace(intf, exit, ERROR, "No interface found to release");

    dhcp_state_release(intf->lease);

    dhcp_state_start(intf->lease);
    rv = 0;
exit:
    return rv;
}

static int dhcpv4c_user_script(UNUSED const char* function_name,
                               UNUSED amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_INFO(ME, "Mod-dhcpv4c does not support user scripts");
    return 0;
}

static int dhcpv4c_raw_sock_handler(UNUSED const char* function_name,
                                    amxc_var_t* args,
                                    UNUSED amxc_var_t* ret) {
    dhcp_interface_dhcp_raw_read_handler(args);
    return 0;
}

static int dhcpv4c_config_update(UNUSED const char* function_name,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    dhcp_interface_t* intf = NULL;
    const char* intf_name = GET_CHAR(args, "IfName");
    int rv = -1;

    // Find iface by Ifname
    intf = dhcp_interface_find(intf_name);
    when_null_trace(intf, exit, ERROR, "No dhcp interface found");
    when_null_trace(intf->lease, exit, ERROR, "No dhcp lease found");

    dhcp_lease_set_retransmission_params(intf->lease, args);
    dhcp_lease_set_auth_params(intf->lease, args);
    dhcp_lease_set_socket_opts(intf->lease, args);
    dhcp_lease_set_broadcast_flag_params(intf->lease, args);
    intf->lease->release_reboot = GET_BOOL(args, "ReleaseOnReboot");

    rv = 0;

exit:
    return rv;
}

static int dhcpv4c_get_statistics(UNUSED const char* function_name,
                                  UNUSED amxc_var_t* args,
                                  amxc_var_t* ret) {

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, ret, "CorruptPackets", dhcp_stat_get_counter(DHCLIENT_CNT_NO_CORRUPT));
    amxc_var_add_key(uint32_t, ret, "Discover", dhcp_stat_get_counter(DHCLIENT_CNT_NO_SENT_DISCOVER));
    amxc_var_add_key(uint32_t, ret, "Request", dhcp_stat_get_counter(DHCLIENT_CNT_NO_SENT_REQUEST));
    amxc_var_add_key(uint32_t, ret, "Decline", dhcp_stat_get_counter(DHCLIENT_CNT_NO_SENT_DECLINE));
    amxc_var_add_key(uint32_t, ret, "Release", dhcp_stat_get_counter(DHCLIENT_CNT_NO_SENT_RELEASE));
    amxc_var_add_key(uint32_t, ret, "Inform", dhcp_stat_get_counter(DHCLIENT_CNT_NO_SENT_INFORM));
    amxc_var_add_key(uint32_t, ret, "ForceRenew", dhcp_stat_get_counter(DHCLIENT_CNT_NO_RECV_FORCERENEW));
    amxc_var_add_key(uint32_t, ret, "OtherMessageTypes", dhcp_stat_get_counter(DHCLIENT_CNT_NO_RECV_OTHER));
    amxc_var_add_key(uint32_t, ret, "Offer", dhcp_stat_get_counter(DHCLIENT_CNT_NO_RECV_OFFER));
    amxc_var_add_key(uint32_t, ret, "ACK", dhcp_stat_get_counter(DHCLIENT_CNT_NO_RECV_ACK));
    amxc_var_add_key(uint32_t, ret, "NAK", dhcp_stat_get_counter(DHCLIENT_CNT_NO_RECV_NAK));
    amxc_var_add_key(uint32_t, ret, "FailedPackets", dhcp_stat_get_counter(DHCLIENT_CNT_NO_SENT_FAILURE));

    return 0;
}

static int dhcpv4c_clear_statistics(UNUSED const char* function_name,
                                    UNUSED amxc_var_t* args,
                                    UNUSED amxc_var_t* ret) {
    dhcp_stat_init();
    return 0;
}

static AMXM_CONSTRUCTOR mod_dhcpv4c_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;
    int ret = -1;

    SAH_TRACEZ_INFO(ME, "mod-dhcpv4c start");

    ret = amxm_module_register(&mod, so, MOD_NAME);
    ret |= amxm_module_add_function(mod, "dhcpv4c-start", dhcpv4c_set_state_start);
    ret |= amxm_module_add_function(mod, "dhcpv4c-stop", dhcpv4c_set_state_stop);
    ret |= amxm_module_add_function(mod, "client-change", dummy_func);
    ret |= amxm_module_add_function(mod, "update-config", dummy_func);
    ret |= amxm_module_add_function(mod, "dhcpv4c-renew", dhcpv4c_renew);
    ret |= amxm_module_add_function(mod, "dhcpv4c-release", dhcpv4c_release);
    ret |= amxm_module_add_function(mod, "user-script", dhcpv4c_user_script);
    ret |= amxm_module_add_function(mod, "raw-sock-handler", dhcpv4c_raw_sock_handler);
    ret |= amxm_module_add_function(mod, "dhcpv4c-config-update", dhcpv4c_config_update);
    ret |= amxm_module_add_function(mod, "dhcpv4c-get-statistics", dhcpv4c_get_statistics);
    ret |= amxm_module_add_function(mod, "dhcpv4c-clear-statistics", dhcpv4c_clear_statistics);
    init_rand();

    return ret;
}

static AMXM_DESTRUCTOR mod_dhcpv4c_stop(void) {
    SAH_TRACEZ_INFO(ME, "mod-dhcpv4c stop");

    dhcp_interface_cleanup();
    amxp_timers_enable(false);

    return 0;
}
