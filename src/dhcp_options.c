/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/time.h>
#include <stdint.h>

#include "debug/sahtrace.h"
#include "debug/sahtrace_macros.h"
#include <amxc/amxc_macros.h>

#include "dhcp_util.h"
#include "dhcp_packet.h"
#include "dhcp_options.h"
#include "mod_dhcpv4c.h"

#define WRITE_MAGIC(P) memcpy(P, (char*) MAGIC_COOKIE, 4); P += 4

#define WRITE_ENDOPTION(P) *P++ = TAG_END; P++

#define OPTION_LEN(x) (2 + x)

#define longcpy(dst, src)    { unsigned _i; char* _sp, * _dp; \
        for(_sp = (char*) (src), _dp = (char*) (dst), _i = 0; _i < 4; _i++) { \
            *_dp++ = *_sp++;} }

static void parse_option_buf(dhcp_packet_p packet, unsigned char* start_ptr, unsigned short length);
static bool option_length_is_equal(const unsigned char* value, const unsigned char len, const common_option_t* option);
static bool option_length_is_multi(const unsigned char* value, const unsigned char len, const common_option_t* option);
static bool option_length_is_min(const unsigned char* value, const unsigned char len, const common_option_t* option);
static bool option_length_encap(const unsigned char* value, const unsigned char len, const common_option_t* option);
static bool option_length_rt_encap(const unsigned char* value, const unsigned char len, const common_option_t* option);
static bool option_length_sip_serv(const unsigned char* value, const unsigned char len, const common_option_t* option);

static const unsigned char MAGIC_COOKIE[4] = { 0x63, 0x82, 0x53, 0x63 };

/* DHCP Option names, formats and codes, from RFC2132.

   Format codes:

   I - IP address
   l - 32-bit signed integer
   L - 32-bit unsigned integer
   s - 16-bit signed integer
   S - 16-bit unsigned integer
   b - 8-bit signed integer
   B - 8-bit unsigned integer
   t - ASCII text
   f - flag (true or false)
   A - array of whatever precedes (e.g., IA means array of IP addresses)
   a - array of the preceding character (e.g., IIa means two or more IP
   addresses)
   U - name of an option space (universe)
   F - implicit flag - the presence of the option indicates that the
   flag is true.
   o - the preceding value is optional.
   E - encapsulation, string or colon-seperated hex list (the latter
   two for parsing).   E is followed by a text string containing
   the name of the option space to encapsulate, followed by a '.'.
   If the E is immediately followed by '.', the applicable vendor
   option space is used if one is defined.
   e - If an encapsulation directive is not the first thing in the string,
   the option scanner requires an efficient way to find the encapsulation.
   This is done by placing a 'e' at the beginning of the option.   The
   'e' has no other purpose, and is not required if 'E' is the first
   thing in the option.
   X - either an ASCII string or binary data.   On output, the string is
   scanned to see if it's printable ASCII and, if so, output as a
   quoted string.   If not, it's output as colon-seperated hex.   On
   input, the option can be specified either as a quoted string or as
   a colon-seperated hex list.
   N - enumeration.   N is followed by a text string containing
   the name of the set of enumeration values to parse or emit,
   followed by a '.'.   The width of the data is specified in the
   named enumeration.   Named enumerations are tracked in parse.c.
   d - Domain name (i.e., FOO or FOO.BAR).
 */
static const common_option_t dhcp_options[] =
{
    { "pad", "", NULL, TAG_PAD, 0, 0, 0 },
    { "subnet-mask", "I", option_length_is_equal, TAG_SUBNET_MASK, 4, 0, 0 },
    { "time-offset", "l", option_length_is_equal, TAG_TIME_OFFSET, 4, 0, 0 },
    { "default-routers", "IA", option_length_is_multi, TAG_GATEWAY, 4, 4, 0 },
    { "time-servers", "IA", option_length_is_multi, TAG_TIME_SERVER, 4, 4, 0 },
    { "ien116-name-servers", "IA", option_length_is_multi, TAG_NAME_SERVER, 4, 4, 0 },
    { "domain-name-servers", "IA", option_length_is_multi, TAG_DOMAIN_SERVER, 4, 4, 0 },
    { "log-servers", "IA", option_length_is_multi, TAG_LOG_SERVER, 4, 4, 0 },
    { "cookie-servers", "IA", option_length_is_multi, TAG_COOKIE_SERVER, 4, 4, 0 },
    { "lpr-servers", "IA", option_length_is_multi, TAG_LPR_SERVER, 4, 4, 0 },
    { "impress-servers", "IA", option_length_is_multi, TAG_IMPRESS_SERVER, 4, 4, 0 },
    { "resource-location-servers", "IA", option_length_is_multi, TAG_RLP_SERVER, 4, 4, 0 },
    { "host-name", "X", option_length_is_min, TAG_HOST_NAME, 1, 0, 0 },
    { "boot-file-size", "S", option_length_is_equal, TAG_BOOT_SIZE, 2, 0, 0 },
    { "merit-dump", "t", option_length_is_min, TAG_DUMP_FILE, 1, 0, 0 },
    { "domain-name", "t", option_length_is_min, TAG_DOMAIN_NAME, 1, 0, 0 },
    { "swap-server", "I", option_length_is_equal, TAG_SWAP_SERVER, 4, 0, 0 },
    { "root-path", "t", option_length_is_min, TAG_ROOT_PATH, 1, 0, 0 },
    { "extensions-path", "t", option_length_is_min, TAG_EXTEN_FILE, 1, 0, 0 },
    { "ip-forwarding", "f", option_length_is_equal, TAG_IP_FORWARD, 1, 0, 0 },
    { "non-local-source-routing", "f", option_length_is_equal, TAG_IP_NLSR, 1, 0, 0 },
    { "policy-filter", "IIA", option_length_is_multi, TAG_IP_POLICY_FILTER, 8, 8, 0 },
    { "max-dgram-reassembly", "S", option_length_is_equal, TAG_IP_MAX_DRS, 2, 0, 0 },
    { "default-ip-ttl", "B", option_length_is_equal, TAG_IP_TTL, 1, 0, 0 },
    { "path-mtu-aging-timeout", "L", option_length_is_equal, TAG_IP_MTU_AGE, 4, 0, 0 },
    { "path-mtu-plateau-table", "SA", option_length_is_multi, TAG_IP_MTU_PLAT, 2, 2, 0 },
    { "interface-mtu", "S", option_length_is_equal, TAG_IP_MTU, 2, 0, 0 },
    { "all-subnets-local", "f", option_length_is_equal, TAG_IP_SNARL, 1, 0, 0 },
    { "broadcast-address", "I", option_length_is_equal, TAG_IP_BROADCAST, 4, 0, 0 },
    { "perform-mask-discovery", "f", option_length_is_equal, TAG_IP_SMASKDISC, 1, 0, 0 },
    { "mask-supplier", "f", option_length_is_equal, TAG_IP_SMASKSUPP, 1, 0, 0, },
    { "router-discovery", "f", option_length_is_equal, TAG_IP_ROUTERDISC, 1, 0, 0 },
    { "router-solicitation-address", "I", option_length_is_equal, TAG_IP_ROUTER_SOL_ADDR, 4, 0, 0 },
    { "classfull-static-routes", "IIA", option_length_is_multi, TAG_IP_CLASSFULL_STAT_RT, 8, 8, 0 },
    { "trailer-encapsulation", "f", option_length_is_equal, TAG_IP_TRAILER_ENC, 1, 0, 0 },
    { "arp-cache-timeout", "L", option_length_is_equal, TAG_ARP_TIMEOUT, 4, 0, 0 },
    { "ieee802-3-encapsulation", "f", option_length_is_equal, TAG_ETHER_IEEE, 1, 0, 0 },
    { "default-tcp-ttl", "B", option_length_is_equal, TAG_IP_TCP_TTL, 1, 0, 0 },
    { "tcp-keepalive-interval", "L", option_length_is_equal, TAG_IP_TCP_KA_INT, 4, 0, 0 },
    { "tcp-keepalive-garbage", "f", option_length_is_equal, TAG_IP_TCP_KA_GARBAGE, 1, 0, 0 },
    { "nis-domain", "t", option_length_is_min, TAG_NIS_DOMAIN, 1, 0, 0 },
    { "nis-servers", "IA", option_length_is_multi, TAG_NIS_SERVER, 4, 4, 0 },
    { "ntp-servers", "IA", option_length_is_multi, TAG_NTP_SERVER, 4, 4, 0 },
    { "vendor-specific-info", "E.", option_length_is_min, TAG_VENDOR_SPECIFIC_INFO, 1, 0, 0 },
    { "netbios-name-servers", "IA", option_length_is_multi, TAG_NBNS_SERVER, 4, 4, 0 },
    { "netbios-dd-server", "IA", option_length_is_multi, TAG_NBDD_SERVER, 4, 4, 0 },
    { "netbios-node-type", "B", option_length_is_equal, TAG_NBOTCP_OTPION, 1, 0, 0 },
    { "netbios-scope", "t", option_length_is_min, TAG_NB_SCOPE, 1, 0, 0 },
    { "font-servers", "IA", option_length_is_multi, TAG_XFONT_SERVER, 4, 4, 0 },
    { "x-display-manager", "IA", option_length_is_multi, TAG_XDISPLAY_SERVER, 4, 4, 0 },
    { "dhcp-requested-address", "I", option_length_is_equal, TAG_REQ_IP, 4, 0, 0 },
    { "dhcp-lease-time", "L", option_length_is_equal, TAG_IP_LEASE_TIME, 4, 0, 0 },
    { "dhcp-option-overload", "B", option_length_is_equal, TAG_OPTION_OVERLOAD, 1, 0, 0 },
    { "dhcp-message-type", "B", option_length_is_equal, TAG_DHCP_MSG_TYPE, 1, 0, 0 },
    { "dhcp-server-identifier", "I", option_length_is_equal, TAG_SERVER_ID, 4, 0, 0 },
    { "dhcp-parameter-request-list", "BA", option_length_is_multi, TAG_PARM_REQ_LIST, 1, 1, 0 },
    { "dhcp-message", "t", option_length_is_equal, TAG_DHCP_TEXT_MSG, 1, 0, 0 },
    { "dhcp-max-message-size", "S", option_length_is_equal, TAG_DHCP_MAX_MSG_SIZE, 2, 0, 0 },
    { "dhcp-renewal-time", "L", option_length_is_equal, TAG_RENEWAL_TIME, 4, 0, 0 },
    { "dhcp-rebinding-time", "L", option_length_is_equal, TAG_REBIND_TIME, 4, 0, 0 },
    { "vendor-class-id", "X", option_length_is_min, TAG_VENDOR_CLASS_ID, 1, 0, 0 },
    { "dhcp-client-identifier", "X", option_length_is_min, TAG_CLIENT_ID, 2, 0, 0 },
    { "nwip-domain", "X", option_length_is_min, TAG_NWIP_DOMAIN, 1, 0, 0 },
    { "nwip-suboptions", "Enwip.", option_length_encap, TAG_NWIP_SUBOPTIONS, 2, 0, 1 },
    { "nisplus-domain", "t", option_length_is_min, TAG_NIS_PLUS_DOMAIN, 1, 0, 0 },
    { "nisplus-servers", "IA", option_length_is_multi, TAG_NIS_PLUS_SERVER, 4, 4, 0 },
    { "tftp-server-name", "t", option_length_is_min, TAG_TFTP_SERVER, 1, 0, 0 },
    { "bootfile-name", "t", option_length_is_min, TAG_BOOTFILE_NAME, 1, 0, 0 },
    { "mobile-ip-home-agent", "IA", option_length_is_multi, TAG_MOBILE_IP_HOME_AGENT, 0, 4, 0 },
    { "smtp-server", "IA", option_length_is_multi, TAG_SMTP_SERVER, 4, 4, 0 },
    { "pop-server", "IA", option_length_is_multi, TAG_POP3_SERVER, 4, 4, 0 },
    { "nntp-server", "IA", option_length_is_multi, TAG_NNTP_SERVER, 4, 4, 0 },
    { "www-server", "IA", option_length_is_multi, TAG_WWW_SERVER, 4, 4, 0 },
    { "finger-server", "IA", option_length_is_multi, TAG_FINGER_SERVER, 4, 4, 0 },
    { "irc-server", "IA", option_length_is_multi, TAG_IRC_SERVER, 4, 4, 0 },
    { "streettalk-server", "IA", option_length_is_multi, TAG_ST_SERVER, 4, 4, 0 },
    { "streettalk-directory-assistance-server", "IA", option_length_is_multi, TAG_STDA_SERVER, 4, 4, 0 },
    { "user-class-identifier", "X", option_length_is_min, TAG_USER_CLASS_ID, 1, 0, 0 },
    { "slp-directory-agent", "fIa", option_length_is_multi, TAG_SLP_DIRECTORY_AGENT, 5, 4, 1 },
    { "slp-service-scope", "fto", option_length_is_min, TAG_SLP_SERVICE_SCOPE, 2, 0, 0 },
    { "rapid-commit", "", option_length_is_equal, TAG_RAPID_COMMIT, 0, 0, 0 },
    { "client-fqdn", "Efqdn.", option_length_is_min, TAG_FQDN, 3, 0, 0 },
    { "relay-agent-information", "Eagent.", option_length_encap, TAG_RELAY_AGENT_INFO, 2, 0, 1 },
    { "internet-storage-servers", "X", option_length_is_multi, TAG_INTERNET_STORAGE_SERVERS, 14, 4, 2 },
    { "#84", "X", option_length_is_min, 84, 0, 0, 0 },
    { "nds-servers", "IA", option_length_is_multi, TAG_NDS_SERVER, 4, 4, 0 },
    { "nds-tree-name", "X", option_length_is_min, TAG_NDS_TREE, 1, 0, 0 },
    { "nds-context", "X", option_length_is_min, TAG_NDS_CONTEXT, 1, 0, 0 },
    { "#88", "X", option_length_is_min, 88, 0, 0, 0 },
    { "#89", "X", option_length_is_min, 89, 0, 0, 0 },
    { "dhcp-msg-authentication", "X", option_length_is_min, TAG_DHCP_MSG_AUTH, 11, 0, 0 },
    { "client-last-transaction-time", "L", option_length_is_equal, TAG_CLIENT_LAST_TRANS_TIME, 4, 0, 0 },
    { "associated-ip", "IA", option_length_is_multi, TAG_ASSOCIATED_IP, 4, 4, 0 },                        // Multiple of 4
    { "#93", "X", option_length_is_min, 93, 0, 0, 0 },
    { "#94", "X", option_length_is_min, 94, 0, 0, 0 },
    { "#95", "X", option_length_is_min, 95, 0, 0, 0 },
    { "#96", "X", option_length_is_min, 96, 0, 0, 0 },
    { "#97", "X", option_length_is_min, 97, 0, 0, 0 },
    { "user-auth", "t", option_length_is_min, TAG_USER_AUTH, 1, 0, 0 },
    { "#99", "X", option_length_is_min, 99, 0, 0, 0 },
    { "#100", "X", option_length_is_min, 100, 0, 0, 0 },
    { "#101", "X", option_length_is_min, 101, 0, 0, 0 },
    { "#102", "X", option_length_is_min, 102, 0, 0, 0 },
    { "#103", "X", option_length_is_min, 103, 0, 0, 0 },
    { "#104", "X", option_length_is_min, 104, 0, 0, 0 },
    { "#105", "X", option_length_is_min, 105, 0, 0, 0 },
    { "#106", "X", option_length_is_min, 106, 0, 0, 0 },
    { "#107", "X", option_length_is_min, 107, 0, 0, 0 },
    { "#108", "X", option_length_is_min, 108, 0, 0, 0 },
    { "#109", "X", option_length_is_min, 109, 0, 0, 0 },
    { "#110", "X", option_length_is_min, 110, 0, 0, 0 },
    { "#111", "X", option_length_is_min, 111, 0, 0, 0 },
    { "#112", "X", option_length_is_min, 112, 0, 0, 0 },
    { "#113", "X", option_length_is_min, 113, 0, 0, 0 },
    { "#114", "X", option_length_is_min, 114, 0, 0, 0 },
    { "#115", "X", option_length_is_min, 115, 0, 0, 0 },
    { "auto-config", "f", option_length_is_equal, TAG_AUTO_CONFIGURE, 1, 0, 0 },
    { "name-service-search", "SA", option_length_is_multi, TAG_NAME_SERVICE_SEARCH, 2, 2, 0 },
    { "subnet-selection", "I", option_length_is_equal, TAG_SUBNET_SELECTION, 4, 0, 0 },
    { "domain-search-list", "t", option_length_is_min, TAG_DOMAIN_SEARCH_LIST, 1, 0, 0 },
    { "sip-servers", "X", option_length_sip_serv, TAG_SIP_SERVERS, 3, 4, 1 },
    { "classless-static-routes", "X", option_length_rt_encap, TAG_IP_CLASSLESS_STAT_RT, 5, 0, 0 },
    { "cablelabs-client-configuration", "Eccc", option_length_encap, TAG_CABLELABS_CLIENT_CONFIG, 3, 0, 1 },
    { "geo-config", "X", option_length_is_equal, TAG_GEO_CONFIG, 16, 0, 0 },
    { "vi-vendor-class-id", "Evv", option_length_encap, TAG_VI_VENDOR_CLASS_ID, 6, 0, 4 },
    { "vi-vendor-specific-info", "EvvEA", option_length_encap, TAG_VI_VENDOR_SPECIFIC_INFO, 8, 0, 4 },
    { "#126", "X", option_length_is_min, 126, 0, 0, 0 },
    { "#127", "X", option_length_is_min, 127, 0, 0, 0 },
    { "#128", "X", option_length_is_min, 128, 0, 0, 0 },
    { "#129", "X", option_length_is_min, 129, 0, 0, 0 },
    { "#130", "X", option_length_is_min, 130, 0, 0, 0 },
    { "#131", "X", option_length_is_min, 131, 0, 0, 0 },
    { "#132", "X", option_length_is_min, 132, 0, 0, 0 },
    { "#133", "X", option_length_is_min, 133, 0, 0, 0 },
    { "#134", "X", option_length_is_min, 134, 0, 0, 0 },
    { "#135", "X", option_length_is_min, 135, 0, 0, 0 },
    { "#136", "X", option_length_is_min, 136, 0, 0, 0 },
    { "#137", "X", option_length_is_min, 137, 0, 0, 0 },
    { "#138", "X", option_length_is_min, 138, 0, 0, 0 },
    { "#139", "X", option_length_is_min, 139, 0, 0, 0 },
    { "#140", "X", option_length_is_min, 140, 0, 0, 0 },
    { "#141", "X", option_length_is_min, 141, 0, 0, 0 },
    { "#142", "X", option_length_is_min, 142, 0, 0, 0 },
    { "#143", "X", option_length_is_min, 143, 0, 0, 0 },
    { "#144", "X", option_length_is_min, 144, 0, 0, 0 },
    { "#145", "X", option_length_is_min, 145, 0, 0, 0 },
    { "#146", "X", option_length_is_min, 146, 0, 0, 0 },
    { "#147", "X", option_length_is_min, 147, 0, 0, 0 },
    { "#148", "X", option_length_is_min, 148, 0, 0, 0 },
    { "#149", "X", option_length_is_min, 149, 0, 0, 0 },
    { "#150", "X", option_length_is_min, 150, 0, 0, 0 },
    { "#151", "X", option_length_is_min, 151, 0, 0, 0 },
    { "#152", "X", option_length_is_min, 152, 0, 0, 0 },
    { "#153", "X", option_length_is_min, 153, 0, 0, 0 },
    { "#154", "X", option_length_is_min, 154, 0, 0, 0 },
    { "#155", "X", option_length_is_min, 155, 0, 0, 0 },
    { "#156", "X", option_length_is_min, 156, 0, 0, 0 },
    { "#157", "X", option_length_is_min, 157, 0, 0, 0 },
    { "#158", "X", option_length_is_min, 158, 0, 0, 0 },
    { "#159", "X", option_length_is_min, 159, 0, 0, 0 },
    { "#160", "X", option_length_is_min, 160, 0, 0, 0 },
    { "#161", "X", option_length_is_min, 161, 0, 0, 0 },
    { "#162", "X", option_length_is_min, 162, 0, 0, 0 },
    { "#163", "X", option_length_is_min, 163, 0, 0, 0 },
    { "#164", "X", option_length_is_min, 164, 0, 0, 0 },
    { "#165", "X", option_length_is_min, 165, 0, 0, 0 },
    { "#166", "X", option_length_is_min, 166, 0, 0, 0 },
    { "#167", "X", option_length_is_min, 167, 0, 0, 0 },
    { "#168", "X", option_length_is_min, 168, 0, 0, 0 },
    { "#169", "X", option_length_is_min, 169, 0, 0, 0 },
    { "#170", "X", option_length_is_min, 170, 0, 0, 0 },
    { "#171", "X", option_length_is_min, 171, 0, 0, 0 },
    { "#172", "X", option_length_is_min, 172, 0, 0, 0 },
    { "#173", "X", option_length_is_min, 173, 0, 0, 0 },
    { "#174", "X", option_length_is_min, 174, 0, 0, 0 },
    { "#175", "X", option_length_is_min, 175, 0, 0, 0 },
    { "#176", "X", option_length_is_min, 176, 0, 0, 0 },
    { "#177", "X", option_length_is_min, 177, 0, 0, 0 },
    { "#178", "X", option_length_is_min, 178, 0, 0, 0 },
    { "#179", "X", option_length_is_min, 179, 0, 0, 0 },
    { "#180", "X", option_length_is_min, 180, 0, 0, 0 },
    { "#181", "X", option_length_is_min, 181, 0, 0, 0 },
    { "#182", "X", option_length_is_min, 182, 0, 0, 0 },
    { "#183", "X", option_length_is_min, 183, 0, 0, 0 },
    { "#184", "X", option_length_is_min, 184, 0, 0, 0 },
    { "#185", "X", option_length_is_min, 185, 0, 0, 0 },
    { "#186", "X", option_length_is_min, 186, 0, 0, 0 },
    { "#187", "X", option_length_is_min, 187, 0, 0, 0 },
    { "#188", "X", option_length_is_min, 188, 0, 0, 0 },
    { "#189", "X", option_length_is_min, 189, 0, 0, 0 },
    { "#190", "X", option_length_is_min, 190, 0, 0, 0 },
    { "#191", "X", option_length_is_min, 191, 0, 0, 0 },
    { "#192", "X", option_length_is_min, 192, 0, 0, 0 },
    { "#193", "X", option_length_is_min, 193, 0, 0, 0 },
    { "#194", "X", option_length_is_min, 194, 0, 0, 0 },
    { "#195", "X", option_length_is_min, 195, 0, 0, 0 },
    { "#196", "X", option_length_is_min, 196, 0, 0, 0 },
    { "#197", "X", option_length_is_min, 197, 0, 0, 0 },
    { "#198", "X", option_length_is_min, 198, 0, 0, 0 },
    { "#199", "X", option_length_is_min, 199, 0, 0, 0 },
    { "#200", "X", option_length_is_min, 200, 0, 0, 0 },
    { "#201", "X", option_length_is_min, 201, 0, 0, 0 },
    { "#202", "X", option_length_is_min, 202, 0, 0, 0 },
    { "#203", "X", option_length_is_min, 203, 0, 0, 0 },
    { "#204", "X", option_length_is_min, 204, 0, 0, 0 },
    { "#205", "X", option_length_is_min, 205, 0, 0, 0 },
    { "#206", "X", option_length_is_min, 206, 0, 0, 0 },
    { "#207", "X", option_length_is_min, 207, 0, 0, 0 },
    { "#208", "X", option_length_is_min, 208, 0, 0, 0 },
    { "#209", "X", option_length_is_min, 209, 0, 0, 0 },
    { "#210", "X", option_length_is_min, 210, 0, 0, 0 },
    { "#211", "X", option_length_is_min, 211, 0, 0, 0 },
    { "#212", "X", option_length_is_min, 212, 0, 0, 0 },
    { "#213", "X", option_length_is_min, 213, 0, 0, 0 },
    { "#214", "X", option_length_is_min, 214, 0, 0, 0 },
    { "#215", "X", option_length_is_min, 215, 0, 0, 0 },
    { "#216", "X", option_length_is_min, 216, 0, 0, 0 },
    { "#217", "X", option_length_is_min, 217, 0, 0, 0 },
    { "#218", "X", option_length_is_min, 218, 0, 0, 0 },
    { "#219", "X", option_length_is_min, 219, 0, 0, 0 },
    { "#220", "X", option_length_is_min, 220, 0, 0, 0 },
    { "#221", "X", option_length_is_min, 221, 0, 0, 0 },
    { "#222", "X", option_length_is_min, 222, 0, 0, 0 },
    { "#223", "X", option_length_is_min, 223, 0, 0, 0 },
    { "#224", "X", option_length_is_min, 224, 0, 0, 0 },
    { "#225", "X", option_length_is_min, 225, 0, 0, 0 },
    { "#226", "X", option_length_is_min, 226, 0, 0, 0 },
    { "#227", "X", option_length_is_min, 227, 0, 0, 0 },
    { "#228", "X", option_length_is_min, 228, 0, 0, 0 },
    { "#229", "X", option_length_is_min, 229, 0, 0, 0 },
    { "#230", "X", option_length_is_min, 230, 0, 0, 0 },
    { "#231", "X", option_length_is_min, 231, 0, 0, 0 },
    { "#232", "X", option_length_is_min, 232, 0, 0, 0 },
    { "#233", "X", option_length_is_min, 233, 0, 0, 0 },
    { "#234", "X", option_length_is_min, 234, 0, 0, 0 },
    { "#235", "X", option_length_is_min, 235, 0, 0, 0 },
    { "#236", "X", option_length_is_min, 236, 0, 0, 0 },
    { "#237", "X", option_length_is_min, 237, 0, 0, 0 },
    { "#238", "X", option_length_is_min, 238, 0, 0, 0 },
    { "#239", "X", option_length_is_min, 239, 0, 0, 0 },
    { "#240", "X", option_length_is_min, 240, 0, 0, 0 },
    { "#241", "X", option_length_is_min, 241, 0, 0, 0 },
    { "#242", "X", option_length_is_min, 242, 0, 0, 0 },
    { "#243", "X", option_length_is_min, 243, 0, 0, 0 },
    { "#244", "X", option_length_is_min, 244, 0, 0, 0 },
    { "#245", "X", option_length_is_min, 245, 0, 0, 0 },
    { "#246", "X", option_length_is_min, 246, 0, 0, 0 },
    { "#247", "X", option_length_is_min, 247, 0, 0, 0 },
    { "#248", "X", option_length_is_min, 248, 0, 0, 0 },
    { "msft-classless-static-routes", "X", option_length_rt_encap, TAG_MSFT_IP_CLASSLESS_STAT_RT, 5, 0, 0 },
    { "#250", "X", option_length_is_min, 250, 0, 0, 0 },
    { "#251", "X", option_length_is_min, 251, 0, 0, 0 },
    { "#252", "X", option_length_is_min, 252, 0, 0, 0 },
    { "#253", "X", option_length_is_min, 253, 0, 0, 0 },
    { "#254", "X", option_length_is_min, 254, 0, 0, 0 },
    { "end", "e", NULL, TAG_END, 0, 0, 0 },
};

static void parse_option_buf(dhcp_packet_p packet, unsigned char* start_ptr, unsigned short length) {
    unsigned char* ptr = NULL;
    unsigned char* end_ptr = NULL;
    int i = 0;

    when_null_trace(packet, exit, ERROR, "Packet ptr is NULL");
    when_null_trace(start_ptr, exit, ERROR, "start_ptr ptr is NULL");

    end_ptr = start_ptr + length;
    for(ptr = start_ptr; *ptr != TAG_END && ptr < end_ptr;) {
        const common_option_t* option = NULL;
        int len = 0;
        int code = 0;

        code = ptr[0];
        // skip pad options
        if(code == TAG_PAD) {
            ++ptr;
            continue;
        }

        option = option_get(code);
        len = ptr[1];

        // check validity of length
        if(ptr + len + 2 > end_ptr) {
            packet->options_valid = 0;
            goto exit;
        }

        if((option->check_length == NULL) || option->check_length(ptr + 2, len, option)) {
            packet->dhcp_option[code].len = len;
            packet->dhcp_option[code].value_ptr = ptr + 2;
        }

        ptr += len + 2;
        packet->optionlist[i] = (char) code;
        i++;
    }

    // scanned entire section, no errors found
    packet->options_valid = 1;

exit:
    return;
}

// Check if the option length is equal to the specified length
static bool option_length_is_equal(UNUSED const unsigned char* value,
                                   const unsigned char len,
                                   const common_option_t* option) {
    return len == option->min_len;
}

// Check if the option length is multiple of the specified length
static bool option_length_is_multi(UNUSED const unsigned char* value,
                                   const unsigned char len,
                                   const common_option_t* option) {
    return (len >= option->min_len && (len - option->offset) % option->multi_of == 0);
}

// Check if the option length is minimum the specified length
static bool option_length_is_min(UNUSED const unsigned char* value,
                                 const unsigned char len,
                                 const common_option_t* option) {
    return len >= option->min_len;
}

// Check if the option length is minimum the specified length
static bool option_length_encap(const unsigned char* value,
                                const unsigned char len,
                                const common_option_t* option) {
    unsigned int index = 0;

    while(index + option->offset < len && value != NULL) {
        index += value[option->offset + index] + option->offset + 1;
    }

    return (len >= option->min_len && (value == NULL || index == len));
}

// Check the if route encapsulation length is correct
static bool option_length_rt_encap(const unsigned char* value,
                                   const unsigned char len,
                                   const common_option_t* option) {
    unsigned int index = 0;
    unsigned char netmask;

    while(index < len && value != NULL) {
        netmask = value[index];
        // increment length with the gateway IP address length
        // the netmask length together with the net id length
        index += 5 + netmask / 8 + (netmask % 8 ? 1 : 0);
    }

    return (len >= option->min_len && (value == NULL || index == len));
}

// Check if the option length is multiple of the specified length
static bool option_length_sip_serv(const unsigned char* value,
                                   const unsigned char len,
                                   const common_option_t* option) {
    if((value == NULL) || (value[0] == 0)) {
        return len >= option->min_len ? 1 : 0;
    } else {
        return (len >= (option->min_len + 2) && (len - option->offset) % option->multi_of == 0);
    }
}

// Return option description structure
const common_option_t* option_get(unsigned char option_code) {
    const common_option_t* ptr = dhcp_options;

    while(ptr->code != option_code) {
        ptr++;
    }

    return ptr;
}

// Check if a suboption is allowed for an option code
bool option_suboption_allowed(unsigned char option_code) {
    const common_option_t* option = option_get(option_code);
    const char* enterprise_str = NULL;
    const char* suboption_str = NULL;

    enterprise_str = strstr(option->format, "Evv");
    if((suboption_str = strrchr(option->format, 'E')) == NULL) {
        suboption_str = strchr(option->format, 'X');
    }

    return (suboption_str != NULL) && (suboption_str != enterprise_str);
}

// Check if a suboption is allowed for an option code
bool option_enterprise_allowed(unsigned char option_code) {
    const common_option_t* option = option_get(option_code);
    return (strstr(option->format, "Evv") != NULL) || (strchr(option->format, 'X') != NULL);
}

void parse_dhcpoptions(dhcp_packet_p packet) {
    when_null_trace(packet, exit, ERROR, "Packet ptr is NULL");
    packet->options_valid = 0;

    // look for the magic cookie, otherwise don't parse
    when_true_trace(packet->packet_size < DHCP_MIN_SIZE + sizeof(MAGIC_COOKIE), exit, WARNING,
                    "packet size[%d] is too small", packet->packet_size);

    // Always look out because some processors can only read data on a natural boundary;
    // Normally though the magic cookie should always be on a natural boundary
    when_true_trace(memcmp(packet->raw.options, (char*) MAGIC_COOKIE, 4) != 0,
                    exit, WARNING, "Magic Cookie not found");

    parse_option_buf(packet, (unsigned char*) packet->raw.options + 4, packet->packet_size - DHCP_MIN_SIZE - 4);

    // in case the options were valid until now, check if the filename
    // and/or server name sections have been overloaded for options
    when_true_trace(packet->options_valid == 0, exit, INFO, "Options are not valid");
    switch(packet->dhcp_option[TAG_OPTION_OVERLOAD].len) {
    case 0:
        // no overload option: stop parsing
        break;

    case 1:
        // length 1 is ok: see what sections have been overloaded
        if((packet->dhcp_option[TAG_OPTION_OVERLOAD].value_ptr[0] & 1) != 0) {
            parse_option_buf(packet, (unsigned char*) packet->raw.fname, sizeof(packet->raw.fname));
        }

        if((packet->dhcp_option[TAG_OPTION_OVERLOAD].value_ptr[0] & 2) != 0) {
            parse_option_buf(packet, (unsigned char*) packet->raw.sname, sizeof(packet->raw.sname));
        }
        break;

    default:
        // not ok: size is invalid
        SAH_TRACE_INFO("Size is invalid.");
        packet->options_valid = 0;
    }

exit:
    return;
}

/*
 * Check if enough space is available in the buffer to store the option
 * If not insert the overload option and update the option pointer to
 * the correct buffer if enough space is available
 */
bool check_option_buf_size(dhcp_packet_p packet,
                           unsigned char** poption_ptr,
                           unsigned long option_len,
                           unsigned short* packet_size_incr_ptr) {
    unsigned long free_buf_size = 0;
    bool ret = false;

    when_null_trace(packet, exit, ERROR, "Packet ptr is NULL");
    when_null_trace(packet_size_incr_ptr, exit, ERROR, "packet_size_incr_ptr is NULL");
    when_null_trace(poption_ptr, exit, ERROR, "poption_ptr is NULL");

    *packet_size_incr_ptr = option_len;

    if(packet->dhcp_option[TAG_OPTION_OVERLOAD].value_ptr != NULL) {
        *packet_size_incr_ptr = 0;
        if((packet->dhcp_option[TAG_OPTION_OVERLOAD].value_ptr[0] & 0x02) != 0) {
            free_buf_size = (unsigned char*) (packet->raw.sname + SNAME_LEN) - (unsigned char*) (*poption_ptr) - 1;
        } else if((packet->dhcp_option[TAG_OPTION_OVERLOAD].value_ptr[0] & 0x01) != 0) {
            free_buf_size = (unsigned char*) (packet->raw.fname + FILE_LEN) - (unsigned char*) (*poption_ptr) - 1;
        }
    } else {
        free_buf_size = (unsigned char*) (packet->raw.options + DHCP_OPTION_SIZE) - (unsigned char*) (*poption_ptr) - 4;
    }

    when_false_status(option_len > free_buf_size, exit, ret = true);
    *packet_size_incr_ptr = 0;

    when_true((option_len + 1) > sizeof(packet->raw.fname), exit);

    if(packet->dhcp_option[TAG_OPTION_OVERLOAD].value_ptr == NULL) {
        *(*poption_ptr)++ = TAG_OPTION_OVERLOAD;
        packet->dhcp_option[TAG_OPTION_OVERLOAD].len = *(*poption_ptr)++ = 1;
        packet->dhcp_option[TAG_OPTION_OVERLOAD].value_ptr = *poption_ptr;
        *(*poption_ptr)++ = 1;
        WRITE_ENDOPTION((*poption_ptr));
        *packet_size_incr_ptr = 4;
        *poption_ptr = (unsigned char*) packet->raw.fname;
        memset(packet->raw.fname, TAG_PAD, sizeof(packet->raw.fname));
    } else {
        when_true((option_len + 1) > sizeof(packet->raw.sname), exit);

        *(packet->dhcp_option[TAG_OPTION_OVERLOAD].value_ptr) = 3;
        WRITE_ENDOPTION((*poption_ptr));
        *poption_ptr = (unsigned char*) packet->raw.sname;
        memset(packet->raw.sname, TAG_PAD, sizeof(packet->raw.sname));
    }
    ret = true;

exit:
    return ret;
}

int write_magic(dhcp_packet_p packet, unsigned char** poption_ptr) {
    unsigned short packet_size_incr = 0;
    int ret = -1;

    when_null_trace(packet, exit, ERROR, "Packet ptr is NULL");
    when_null_trace(poption_ptr, exit, ERROR, "poption_ptr is NULL");

    // Reset the overload option so that the check_option_buf_size() function gets not confused
    packet->dhcp_option[TAG_OPTION_OVERLOAD].len = 0;
    packet->dhcp_option[TAG_OPTION_OVERLOAD].value_ptr = NULL;

    *poption_ptr = (unsigned char*) packet->raw.options;

    when_false(check_option_buf_size(packet, poption_ptr, sizeof(MAGIC_COOKIE), &packet_size_incr), exit);

    WRITE_MAGIC(*poption_ptr);
    packet->packet_size += packet_size_incr;
    ret = 0;

exit:
    return ret;
}

int write_endoption(dhcp_packet_p packet, unsigned char** poption_ptr) {
    unsigned short packet_size_incr = 0;
    int ret = -1;

    when_null_trace(packet, exit, ERROR, "Packet ptr is NULL");
    when_null_trace(poption_ptr, exit, ERROR, "poption_ptr is NULL");

    when_false(check_option_buf_size(packet, poption_ptr, 1, &packet_size_incr), exit);

    WRITE_ENDOPTION((*poption_ptr));
    packet->packet_size += packet_size_incr;
    ret = 0;

exit:
    return ret;
}

int write_optionchar(dhcp_packet_p packet,
                     unsigned char** poption_ptr,
                     unsigned char option_type,
                     unsigned char value) {
    unsigned short size = 0;
    int ret = -1;

    when_null_trace(packet, exit, ERROR, "Packet ptr is NULL");
    when_null_trace(poption_ptr, exit, ERROR, "poption_ptr is NULL");

    when_false(check_option_buf_size(packet, poption_ptr, OPTION_LEN(sizeof(unsigned char)), &size), exit);

    *(*poption_ptr)++ = option_type;
    *(*poption_ptr)++ = sizeof(unsigned char);
    *(*poption_ptr)++ = value;

    packet->packet_size += size;
    ret = 0;

exit:
    return ret;
}

int write_optiondword(dhcp_packet_p packet,
                      unsigned char** poption_ptr,
                      unsigned char option_type,
                      uint32_t value) {
    unsigned short packet_size_incr = 0;
    int ret = -1;

    when_null_trace(packet, exit, ERROR, "Packet ptr is NULL");
    when_null_trace(poption_ptr, exit, ERROR, "poption_ptr is NULL");

    when_false(check_option_buf_size(packet, poption_ptr, OPTION_LEN(sizeof(uint32_t)), &packet_size_incr), exit);

    *(*poption_ptr)++ = option_type;
    *(*poption_ptr)++ = sizeof(uint32_t);
    // !!! Some processors cannot write double words on an address not dividable by 4
    memcpy(*poption_ptr, (char*) &value, sizeof(uint32_t));
    *poption_ptr += sizeof(uint32_t);

    packet->packet_size += packet_size_incr;
    ret = 0;

exit:
    return ret;
}

int write_optionraw(dhcp_packet_p packet,
                    unsigned char** poption_ptr,
                    unsigned char option_type,
                    const unsigned char* buf,
                    unsigned long buf_len) {
    unsigned short packet_size_incr = 0;
    int ret = -1;

    when_null_trace(packet, exit, ERROR, "Packet ptr is NULL");
    when_null_trace(poption_ptr, exit, ERROR, "poption_ptr is NULL");
    when_null_trace(buf, exit, ERROR, "Buf is NULL");

    when_false(check_option_buf_size(packet, poption_ptr, OPTION_LEN(buf_len), &packet_size_incr), exit);

    *(*poption_ptr)++ = option_type;
    *(*poption_ptr)++ = buf_len;

    while(buf_len-- != 0) {
        *(*poption_ptr)++ = *buf++;
    }

    packet->packet_size += packet_size_incr;
    ret = 0;

exit:
    return ret;
}

int check_configuration_token(const char* token, struct authentication_info* info) {
    int ret = -1;
    when_str_empty_trace(token, exit, ERROR, "Configuration token is empty");
    when_null_trace(info, exit, ERROR, "Authentication info is NULL");
    when_false_trace(info->protocol == 0, exit, ERROR, "Protocol is not 0");
    // if protocol is 0, the authentication information field holds a simple configuration token

    when_true_trace(info->algorithm != 0, exit, WARNING,
                    "Authentication: Protocol(0) requires Algorithm 0, is [%d]", info->algorithm);

    when_str_empty_trace(info->authentication_information, exit, WARNING,
                         "Authentication: Authentication info is empty");

    when_true_trace(strcmp((char*) info->authentication_information, token) != 0, exit, WARNING,
                    "Authentication: No Configuration token match");

    SAH_TRACEZ_INFO(ME, "Authentication: Configuration token match");

    ret = 0;

exit:
    return ret;
}

int check_delayed_authentication(struct authentication_info* info) {
    int ret = -1;
    when_null_trace(info, exit, ERROR, "Authentication info is NULL");
    when_false_trace(info->protocol == 1, exit, ERROR, "Protocol is not 1");
    // if protocol is 1, the message is using the "delayed authentication" mechanism.

    ret = 0;

exit:
    return ret;
}

// According to rfc 3118, the replay detection must be a monotonically increasing value
// return negative, 0, positive value, if current value is smaller than, equal or bigger than the value in the packet
int check_replay_detection(long long replay_detection, struct authentication_info* info) {
    int ret = 0;

    when_null_trace(info, exit, ERROR, "Authentication info is NULL");

    ret = memcmp((char*) &replay_detection, (char*) &(info->replay_detection), sizeof(long long));
    if(ret < 0) {
        SAH_TRACEZ_NOTICE(ME, "RDM is increasing");
    } else if(ret == 0) {
        SAH_TRACEZ_WARNING(ME, "RDM is the same, is this packet replayed?!?");
    } else {
        SAH_TRACEZ_WARNING(ME, "RDM is not increasing");
    }

exit:
    return ret;
}
