/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <yajl/yajl_gen.h>
#include <amxj/amxj_variant.h>
#include "debug/sahtrace.h"
#include "debug/sahtrace_macros.h"

#include "dhcp_auth.h"
#include "dhcp_packet.h"
#include "dhcp_lease.h"
#include "dhcp_state.h"
#include "dhcp_options.h"
#include "dhcp_util.h"
#include "dhcp_txoption.h"
#include "dhcp_dm.h"
#include "mod_dhcpv4c.h"
#include "dhcp_stat.h"

// minimal time to wait for a reply in seconds
#define REPLY_TIMEOUT 10
#define MIN_RENEW_TIMEOUT 60
#define MIN_REBIND_TIMEOUT 60

// define a maximum leasetime of 21 days, chosen so that
// no overflow will happen when time is expressed in ms.
#define MAX_LEASETIME (21 * 24 * 60 * 60)

// Uncrustify messes up the compare operator in timercmp
// Replace them with defines
// Also, disable uncrustify for these defines as they cause issues
// *INDENT-OFF*
#define CMP_OPERATOR_GT >
#define CMP_OPERATOR_LT <
// *INDENT-ON*

static void dhcp_state_update(dhcp_lease_t* lease, dhcp_client_state_t state);
static void dhcp_state_set_timeout(dhcp_lease_t* lease, struct timeval* deadline, int step0);
static void dhcp_state_goto_init(dhcp_lease_t* lease);
static void dhcp_state_goto_init_reboot(dhcp_lease_t* lease);
static void dhcp_state_goto_rebooting(dhcp_lease_t* lease);
static void dhcp_state_goto_selecting(dhcp_lease_t* lease);
static void dhcp_state_goto_requesting(dhcp_lease_t* lease);
static void dhcp_state_goto_bound(dhcp_lease_t* lease, dhcp_packet_t* packet);
static void dhcp_state_goto_renewing(dhcp_lease_t* lease);
static void dhcp_state_goto_rebinding(dhcp_lease_t* lease);
static void dhcp_state_goto_informing(dhcp_lease_t* lease);
static void dhcp_state_goto_inform_ok(dhcp_lease_t* lease, dhcp_packet_t* packet);
static void dhcp_state_on_offer(dhcp_lease_t* lease, dhcp_packet_t* packet);
static void dhcp_state_on_ack(dhcp_lease_t* lease, dhcp_packet_t* packet);
static void dhcp_state_on_nak(dhcp_lease_t* lease, dhcp_packet_t* packet);
static void dhcp_state_on_force_renew(dhcp_lease_t* lease, dhcp_packet_t* packet);
static void dhcp_state_cleanup_data(dhcp_lease_t* lease, bool del_rqs_ip_option);
static void dhcp_state_set_timer(uint32_t time, struct timeval* res, amxp_timer_t* timer);

static bool time_is_zero(struct timeval* tv) {
    assert(tv);
    return ((tv->tv_sec == 0) && (tv->tv_usec == 0));
}

static void dhcp_lease_req_options_from_packet(dhcp_lease_t* lease, dhcp_packet_t* packet) {
    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(packet, exit, ERROR, "Packet is NULL");

    amxc_var_for_each(var, &lease->req_options) {
        unsigned char tag = GET_UINT32(var, "Tag");
        char* hex_str = NULL;

        if(packet->dhcp_option[tag].value_ptr != NULL) {
            amxc_var_t value;
            amxc_var_init(&value);

            hex_str = bin_to_hex(packet->dhcp_option[tag].value_ptr, packet->dhcp_option[tag].len);
            amxc_var_set(cstring_t, &value, hex_str);
            amxc_var_set_key(var, "Value", &value, AMXC_VAR_FLAG_COPY | AMXC_VAR_FLAG_UPDATE);

            free(hex_str);
            amxc_var_clean(&value);
        }
    }

exit:
    return;
}

static void dhcp_lease_req_options_clean_values(dhcp_lease_t* lease) {
    when_null_trace(lease, exit, ERROR, "Lease is NULL");

    amxc_var_for_each(var, &lease->req_options) {
        amxc_var_t* val = amxc_var_take_key(var, "Value");
        if(val != NULL) {
            amxc_var_delete(&val);
        }
    }

exit:
    return;
}

static int dhcp_var_to_lease_info(dhcp_lease_t* lease, amxc_var_t* data) {
    int ret = -1;
    int rv = -1;
    struct in_addr addr;
    const char* str = NULL;
    unsigned int bin_len = 0;
    unsigned char* bin = NULL;

    memset(&addr, 0, sizeof(addr));
    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(data, exit, ERROR, "Data is NULL");

    str = GET_CHAR(data, "ID");
    when_str_empty_trace(str, exit, ERROR, "No lease ID found");
    strncpy(lease->id, str, sizeof(lease->id) - 1);

    lease->xid = GET_UINT32(data, "XID");
    when_true_trace(lease->xid == 0, exit, ERROR, "No valid XID found");

    str = GET_CHAR(data, "IP Address");
    when_str_empty_trace(str, exit, ERROR, "No lease IP found");
    rv = inet_pton(AF_INET, str, &lease->ip_addr);
    when_true_trace(rv != 1, exit, ERROR, "Failed to parse lease IP");

    str = GET_CHAR(data, "Subnet Mask");
    when_str_empty_trace(str, exit, ERROR, "No subnet mask found");
    rv = inet_pton(AF_INET, str, &lease->netmask);
    when_true_trace(rv != 1, exit, ERROR, "Failed to parse subnet mask");

    str = GET_CHAR(data, "Server IP");
    when_str_empty_trace(str, exit, ERROR, "No server IP found");
    rv = inet_pton(AF_INET, str, &lease->server_ip);
    when_true_trace(rv != 1, exit, ERROR, "Failed to parse server IP");

    str = GET_CHAR(data, "Server MAC Address");
    when_str_empty_trace(str, exit, ERROR, "No server MAC address found");
    bin = hex_to_bin(str, &bin_len);
    when_null_trace(bin, exit, ERROR, "Failed to parse server MAC address");
    memcpy(lease->server_lladdr, bin, sizeof(lease->server_lladdr));

    ret = 0;

exit:
    free(bin);
    return ret;
}

static void dhcp_lease_info_to_var(dhcp_lease_t* lease, amxc_var_t* data) {
    struct in_addr addr;
    char* hex = NULL;

    memset(&addr, 0, sizeof(addr));
    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(data, exit, ERROR, "Data is NULL");

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, data, "ID", lease->id);
    amxc_var_add_key(uint32_t, data, "XID", lease->xid);
    addr.s_addr = lease->ip_addr;
    amxc_var_add_key(cstring_t, data, "IP Address", inet_ntoa(addr));
    addr.s_addr = lease->netmask;
    amxc_var_add_key(cstring_t, data, "Subnet Mask", inet_ntoa(addr));
    addr.s_addr = lease->server_ip;
    amxc_var_add_key(cstring_t, data, "Server IP", inet_ntoa(addr));
    hex = bin_to_hex(lease->server_lladdr, sizeof(lease->server_lladdr));
    amxc_var_add_key(cstring_t, data, "Server MAC Address", hex);

exit:
    free(hex);
}

// Function to export lease data in file
static void dhcp_state_export_in_file(dhcp_lease_t* lease) {
    int file = -1;
    int ret = -1;
    const char* filename = NULL;
    amxc_var_t data;
    amxc_string_t str;
    variant_json_t* writer = NULL;

    amxc_string_init(&str, 0);
    amxc_var_init(&data);
    when_null_trace(lease, exit, ERROR, "Lease is NULL");

    amxc_string_setf(&str, "%s/dhcplease_%s", lease->storage_path, lease->ifname);
    filename = amxc_string_get(&str, 0);

    file = open(filename, O_WRONLY | O_CREAT, 0644);
    when_true_trace(file < 0, exit, ERROR, "Unable to create file %s", filename);

    dhcp_lease_info_to_var(lease, &data);
    amxj_writer_new(&writer, &data);
    ret = amxj_write(writer, file);

    close(file);
    if((ret <= 0) && (remove(filename) != 0)) {
        SAH_TRACE_ERROR("Error removing file %s", filename);
    }

exit:
    amxj_writer_delete(&writer);
    amxc_var_clean(&data);
    amxc_string_clean(&str);
    return;
}

static void dhcp_error_update(dhcp_lease_t* lease, unsigned long error) {
    when_true(lease->client_err == error, exit);

    lease->client_err = error;
    dhcp_dm_update(lease);

exit:
    return;
}

// Update lease state
static void dhcp_state_update(dhcp_lease_t* lease, dhcp_client_state_t state) {
    when_true(lease->state == state, exit);

    lease->state = state;
    dhcp_dm_update(lease);

    // If no release on reboot is requested, skip the next section
    when_false(lease->release_reboot, exit);

    if(lease->state == dhcp_client_state_bound) {
        dhcp_state_export_in_file(lease);
    } else if((lease->state != dhcp_client_state_renewing) &&
              (lease->state != dhcp_client_state_rebinding) &&
              !str_empty(lease->ifname) &&
              lease->release_reboot_done) {
        amxc_string_t str;
        amxc_string_init(&str, 0);

        amxc_string_setf(&str, "%s/dhcplease_%s", lease->storage_path, lease->ifname);
        if(remove(amxc_string_get(&str, 0)) != 0) {
            SAH_TRACEZ_ERROR(ME, "Error removing file %s", amxc_string_get(&str, 0));
        }

        amxc_string_clean(&str);
    }

exit:
    return;
}

// Double the retransmission interval until a maximum has been reached, then keep it constant.
static void dhcp_state_set_timeout(dhcp_lease_t* lease, struct timeval* deadline, int step0) {
    struct timeval dl;
    struct timeval now;
    struct timeval tv;

    if(deadline == NULL) {
        memset(&dl, 0, sizeof(struct timeval));
    } else {
        dl.tv_sec = deadline->tv_sec;
        dl.tv_usec = deadline->tv_usec;
    }

    get_timestamp(&now);

    if((lease->state == dhcp_client_state_renewing) || (lease->state == dhcp_client_state_rebinding)) {
        timersub(&dl, &now, &(lease->timeout));
    } else {
        if(step0 != 0) {
            SAH_TRACEZ_INFO(ME, "Set retransmission step to 0");
            lease->retransmission_step = 0;
        }

        if((lease->retransmission_step >= 0) &&
           (lease->retransmission_step < DHCLIENT_RETRANSMISSION_STRATEGY_LEN)) {
            long nexttimeout = GETI_INT32(&lease->retransmission_strategy, lease->retransmission_step);
            if(nexttimeout == -1) {
                SAH_TRACEZ_INFO(ME, "Keep using previous timeout[%lu:%lu]",
                                lease->timeout.tv_sec, lease->timeout.tv_usec);
                if(time_is_zero(&(lease->timeout))) {
                    SAH_TRACEZ_WARNING(ME, "Timeout is INFINITE_DELAY");
                    lease->timeout.tv_sec = INFINITE_DELAY;
                    lease->timeout.tv_usec = 0;
                }
            }
            if(nexttimeout == 0) {
                SAH_TRACEZ_WARNING(ME, "Timeout is INFINITE_DELAY");
                lease->timeout.tv_sec = INFINITE_DELAY;
                lease->timeout.tv_usec = 0;
            } else if(nexttimeout > 0) {
                // add a random timestamp to the next timeout if configured
                if(lease->retransmission_randomize > 0) {
                    uint32_t rnd = rand() % (lease->retransmission_randomize * 2);
                    SAH_TRACEZ_INFO(ME, "Add random time[%" PRIu32 "] to timeout[%lu]", rnd, nexttimeout);
                    if(lease->retransmission_randomize < (uint32_t) nexttimeout) {
                        nexttimeout = (nexttimeout - lease->retransmission_randomize) + rnd;
                    } else {
                        nexttimeout += rand() % lease->retransmission_randomize;
                    }
                }
                lease->timeout.tv_sec = nexttimeout / 1000;
                lease->timeout.tv_usec = nexttimeout % 1000;
                lease->retransmission_step++;
            }
        }
    }

    SAH_TRACEZ_INFO(ME, "Retransmission_step[%d], Next Timeout[%lus, %luus]",
                    lease->retransmission_step, lease->timeout.tv_sec, lease->timeout.tv_usec);
    set_timer(lease->retransmit_timer, &(lease->timeout));

    timeradd(&dl, &lease->timeout, &tv);
    if(time_is_zero(&(lease->timeout)) && time_is_zero(&dl)) {
        lease->deadline.tv_sec = INFINITE_DELAY;
        lease->deadline.tv_usec = 0;
    } else if(time_is_zero(&dl) || timercmp(&dl, &tv, CMP_OPERATOR_GT)) {
        lease->deadline.tv_sec = tv.tv_sec;
        lease->deadline.tv_usec = tv.tv_usec;
    } else {
        lease->deadline.tv_sec = dl.tv_sec;
        lease->deadline.tv_usec = dl.tv_usec;
    }
    SAH_TRACEZ_INFO(ME, "Set Deadline[%lu, %lu]", lease->deadline.tv_sec, lease->deadline.tv_usec);
}

// Go to the INIT state: this is the start for each lease that does not know of a previous ip address.
// Each lease starts with a random wait time to avoid synchronisations between all the leases in the network.
static void dhcp_state_goto_init(dhcp_lease_t* lease) {
    SAH_TRACEZ_NOTICE(ME, "[%s] Client in init state.", lease->id);
    dhcp_state_update(lease, dhcp_client_state_init);
    // we must wait a random time before continuing: see dhcp_state_on_timeout
    dhcp_lease_set_initial_wait(lease);
}

// Go to the INITREBOOT state: this is the start for each lease that knows of a previous ip address.
// Starts with a random wait time to avoid synchronisations between other leases on the network.
static void dhcp_state_goto_init_reboot(dhcp_lease_t* lease) {
    SAH_TRACEZ_NOTICE(ME, "[%s] Client in init-reboot state.", lease->id);
    dhcp_state_update(lease, dhcp_client_state_init_reboot);
    // we must wait a random time before continuing: see dhcp_state_on_timeout
    dhcp_lease_set_initial_wait(lease);
}

// Go to the REBOOTING state: broadcasting REQUESTs
static void dhcp_state_goto_rebooting(dhcp_lease_t* lease) {
    struct timeval tv;
    memset(&tv, 0, sizeof(struct timeval));

    SAH_TRACEZ_NOTICE(ME, "[%s] Client in rebooting state.", lease->id);
    dhcp_state_update(lease, dhcp_client_state_rebooting);

    // try 3 times (= 2 retries)
    // number of retransmissions of REQUESTs left before re-initializing
    lease->nr_tries = 2;

    // recalculate the timeout and send DHCP Request
    dhcp_state_set_timeout(lease, &tv, 1);
    dhcp_packet_send(lease, DHCPREQUEST);
}

// Go to the SELECTING state: broadcasting DISCOVERS
static void dhcp_state_goto_selecting(dhcp_lease_t* lease) {
    SAH_TRACEZ_NOTICE(ME, "[%s] Client in selecting state.", lease->id);
    dhcp_state_update(lease, dhcp_client_state_selecting);

    SAH_TRACEZ_INFO(ME, "Send DISCOVER, retry[%u]", lease->nr_tries);
    dhcp_packet_send(lease, DHCPDISCOVER);

    // recalculate the timeout
    dhcp_state_set_timeout(lease, NULL, 1);
    // means here: number of transmissions of DISCOVER
    lease->nr_tries++;
}

// Go to the REQUESTING state: sending REQUESTS on an offered lease.
static void dhcp_state_goto_requesting(dhcp_lease_t* lease) {
    SAH_TRACEZ_NOTICE(ME, "[%s] Client in requesting state.", lease->id);
    dhcp_state_update(lease, dhcp_client_state_requesting);

    // try 5 times (= 4 retries)
    // means here: number of retransmissions of REQUESTs left before re-initializing
    lease->nr_tries = 4;

    // recalculate the timeout
    dhcp_state_set_timeout(lease, NULL, 1);

    // make a request packet
    dhcp_packet_send(lease, DHCPREQUEST);
}

// Update timeval in lease object and set the timer,
// time is expressed in seconds
static void dhcp_state_set_timer(uint32_t time, struct timeval* res, amxp_timer_t* timer) {
    struct timeval t;
    struct timeval now;

    when_null_trace(res, exit, ERROR, "Timeval pointer is NULL");
    when_null_trace(timer, exit, ERROR, "Timer is NULL");

    memset(&t, 0, sizeof(struct timeval));
    get_timestamp(&now);

    t.tv_sec = time;

    timeradd(&now, &t, res);
    set_timer(timer, &t);

exit:
    return;
}

// Go to the BOUND state:
// configure the IP interface with the received lease, wait until renewal time.
static void dhcp_state_goto_bound(dhcp_lease_t* lease, dhcp_packet_t* packet) {
    uint32_t leasetime = 0;
    uint32_t renewaltime = 0;
    uint32_t rebindingtime = 0;
    struct timeval now;
    get_timestamp(&now);

    // store the relative time since the IP address has been allocated
    if((lease->state == dhcp_client_state_rebooting) || (lease->state == dhcp_client_state_requesting)) {
        get_timestamp(&lease->uptime);
        SAH_TRACEZ_INFO(ME, "[%s], Set uptime [%lu]", lease->id, lease->uptime.tv_sec);
    }

    // Setup the timeout values
    // Leasetime presence check is performed in function dhcp_packet_check_lease()
    READ_OPTION(packet->dhcp_option[TAG_IP_LEASE_TIME], &leasetime, sizeof(leasetime));

    leasetime = ntohl(leasetime);
    // limit the leasetime
    if(leasetime > MAX_LEASETIME) {
        leasetime = MAX_LEASETIME;
    }
    SAH_TRACEZ_INFO(ME, "[%s], Leasetime is set to[%u]", lease->id, leasetime);

    // calculate leasetime in timeval
    lease->lease_time.tv_sec = leasetime;
    lease->lease_time.tv_usec = 0;

    dhcp_lease_init_renew_counter(lease);

    timeradd(&now, &(lease->lease_time), &(lease->expires));

    // calculate the renewal time
    if(packet->dhcp_option[TAG_RENEWAL_TIME].len == 4) {
        READ_OPTION(packet->dhcp_option[TAG_RENEWAL_TIME], &renewaltime, sizeof(renewaltime));
        renewaltime = ntohl(renewaltime);

        if(renewaltime > leasetime) {
            // default: T1 = 0.5*leasetime
            renewaltime = leasetime / 2;
        }
    } else if(leasetime != INFINITE_DELAY) {
        // default: T1 = 0.5*leasetime
        renewaltime = leasetime / 2;
    } else {
        renewaltime = INFINITE_DELAY;
    }

    // calculate the rebinding time
    if(packet->dhcp_option[TAG_REBIND_TIME].len == 4) {
        READ_OPTION(packet->dhcp_option[TAG_REBIND_TIME], &rebindingtime, sizeof(rebindingtime));
        rebindingtime = ntohl(rebindingtime);

        if(rebindingtime > leasetime) {
            // default: T2 = 0.875*leasetime
            rebindingtime = 0.875 * leasetime;
        }
    } else if(leasetime != INFINITE_DELAY) {
        // default: T2 = 0.875*leasetime
        rebindingtime = 0.875 * leasetime;
    } else {
        rebindingtime = INFINITE_DELAY;
    }

    // consistency check
    if(rebindingtime < renewaltime) {
        rebindingtime = renewaltime;
    }

    if(rebindingtime > leasetime) {
        rebindingtime = leasetime;
    }

    dhcp_state_set_timer(renewaltime, &lease->renew_time, lease->renew_timer);
    SAH_TRACEZ_INFO(ME, "[%s], Set renew time(T1) to [%lu]", lease->id, lease->renew_time.tv_sec);

    dhcp_state_set_timer(rebindingtime, &lease->rebind_time, lease->rebind_timer);
    SAH_TRACEZ_INFO(ME, "[%s], Set rebind time(T2) to [%lu]", lease->id, lease->rebind_time.tv_sec);

    lease->deadline.tv_sec = lease->renew_time.tv_sec;
    lease->deadline.tv_usec = lease->renew_time.tv_usec;
    SAH_TRACEZ_INFO(ME, "[%s], Set deadline time to [%lu]", lease->id, lease->deadline.tv_sec);

    SAH_TRACEZ_NOTICE(ME, "[%s], Lease [%u.%u.%u.%u] to state BOUND", lease->id, DHCP_IP_QUAD(lease->ip_addr));

    // Parse req options from packet into lease structure
    dhcp_lease_req_options_from_packet(lease, packet);
    // Go to BOUND state
    dhcp_state_update(lease, dhcp_client_state_bound);
}

// Go to the RENEWAL state: Send REQUESTS to the DHCP server until it answers.
static void dhcp_state_goto_renewing(dhcp_lease_t* lease) {
    struct timeval nt;
    struct timeval now;
    get_timestamp(&now);

    SAH_TRACEZ_NOTICE(ME, "Client[%s] renews lease %u.%u.%u.%u.",
                      lease->id, DHCP_IP_QUAD(lease->ip_addr));
    dhcp_state_update(lease, dhcp_client_state_renewing);

    if(lease->retransmission_renew_timeout > 0) {
        nt.tv_sec = now.tv_sec + lease->retransmission_renew_timeout;
        nt.tv_usec = now.tv_usec;

        SAH_TRACEZ_NOTICE(ME, "Now[%lu]sec rebind[%lu]sec",
                          now.tv_sec, lease->rebind_time.tv_sec);

        if(timercmp(&nt, &lease->rebind_time, CMP_OPERATOR_LT)) {
            if((lease->renew_counter != -1) && (lease->nr_tries >= lease->renew_counter)) {
                SAH_TRACEZ_NOTICE(ME, "Retransmit REQUEST done[%d of %d]",
                                  lease->nr_tries, lease->renew_counter);
                dhcp_packet_send(lease, DHCPRELEASE);
                dhcp_state_cleanup_data(lease, true);
                dhcp_error_update(lease, DHCLIENT_ERR_RENEWING_TIMEOUT);

                SAH_TRACEZ_NOTICE(ME, "Goto init state");
                dhcp_state_goto_init(lease);
            } else {
                dhcp_packet_send(lease, DHCPREQUEST);
                lease->nr_tries++;

                SAH_TRACEZ_NOTICE(ME, "Next RENEW timeout in[%d]sec, nr[%d], max[%d]",
                                  lease->retransmission_renew_timeout, lease->nr_tries,
                                  lease->renew_counter);
                dhcp_state_set_timer(lease->retransmission_renew_timeout,
                                     &lease->renew_time,
                                     lease->renew_timer);
            }
        } else {
            SAH_TRACEZ_NOTICE(ME, "Await Rebind Timeout [%lu]sec",
                              lease->rebind_time.tv_sec - now.tv_sec);
        }
    } else {
        // LOGARITMIC TIMEOUT, RFC behavior
        SAH_TRACEZ_NOTICE(ME, "Set Rebind Timeout [%lu]sec",
                          lease->rebind_time.tv_sec - now.tv_sec);
        if(timercmp(&now, &lease->rebind_time, CMP_OPERATOR_LT)) {
            SAH_TRACEZ_NOTICE(ME, "Retransmit REQUEST(Renew)");
            dhcp_packet_send(lease, DHCPREQUEST);
            lease->nr_tries++;
            timersub(&lease->rebind_time, &now, &nt);

            if(nt.tv_sec / 2 > MIN_RENEW_TIMEOUT) {
                SAH_TRACEZ_NOTICE(ME, "Set next RENEW Timeout in[%lu]sec, nr[%d], max[%d]",
                                  nt.tv_sec / 2, lease->nr_tries, lease->renew_counter);
                dhcp_state_set_timer((nt.tv_sec / 2), &lease->renew_time, lease->renew_timer);
            } else if(nt.tv_sec > MIN_RENEW_TIMEOUT) {
                nt.tv_sec = MIN_RENEW_TIMEOUT;
                SAH_TRACEZ_NOTICE(ME, "Set next RENEW Timeout in %d sec, nr[%d], max[%d]",
                                  MIN_RENEW_TIMEOUT, lease->nr_tries, lease->renew_counter);
                dhcp_state_set_timer((nt.tv_sec), &lease->renew_time, lease->renew_timer);
            } else {
                SAH_TRACEZ_NOTICE(ME, "Await Rebind Timeout [%lu]sec", nt.tv_sec);
            }
        } else {
            SAH_TRACEZ_WARNING(ME, "Rebind time already expired");
        }
    }
}

// Go to the REBINDING state: broadcast REQUESTS until a server answers.
static void dhcp_state_goto_rebinding(dhcp_lease_t* lease) {
    struct timeval now;
    struct timeval nt;

    SAH_TRACEZ_NOTICE(ME, "Client[%s] rebinds lease %u.%u.%u.%u.",
                      lease->id, DHCP_IP_QUAD(lease->ip_addr));

    get_timestamp(&now);
    dhcp_state_update(lease, dhcp_client_state_rebinding);
    dhcp_packet_send(lease, DHCPREQUEST);

    if(lease->retransmission_renew_timeout > 0) {
        nt.tv_sec = now.tv_sec + lease->retransmission_renew_timeout;
        nt.tv_usec = now.tv_usec;

        SAH_TRACEZ_NOTICE(ME, "DEBUG next timeout[%lu]sec, lease expires[%lu]sec",
                          nt.tv_sec, lease->rebind_time.tv_sec);
        if(timercmp(&nt, &lease->expires, CMP_OPERATOR_LT)) {
            SAH_TRACEZ_WARNING(ME, "Set next REBIND Timeout [%d]sec",
                               lease->retransmission_renew_timeout);
            dhcp_state_set_timer(lease->retransmission_renew_timeout,
                                 &lease->rebind_time,
                                 lease->rebind_timer);
        } else {
            timersub(&lease->expires, &now, &nt);
            SAH_TRACEZ_NOTICE(ME, "Await Lease expiry [%lu]sec", nt.tv_sec);
            dhcp_state_set_timeout(lease, &lease->expires, 0);
        }
    } else {
        if(timercmp(&now, &lease->expires, CMP_OPERATOR_LT)) {
            SAH_TRACEZ_WARNING(ME, "Set next REBIND timeout [%lu]sec",
                               lease->expires.tv_sec - now.tv_sec);

            timersub(&lease->expires, &now, &nt);
            if(nt.tv_sec / 2 > MIN_REBIND_TIMEOUT) {
                dhcp_state_set_timer((nt.tv_sec / 2), &lease->rebind_time, lease->rebind_timer);
            } else if(nt.tv_sec > MIN_REBIND_TIMEOUT) {
                nt.tv_sec = MIN_REBIND_TIMEOUT;
                dhcp_state_set_timer((nt.tv_sec), &lease->rebind_time, lease->rebind_timer);
            } else {
                SAH_TRACEZ_NOTICE(ME, "Await Lease expiry [%lu]sec",
                                  lease->expires.tv_sec - nt.tv_sec);
                dhcp_state_set_timeout(lease, &lease->expires, 0);
            }
        } else {
            SAH_TRACEZ_WARNING(ME, "Lease already expired");
        }
    }
}

// Go to the INFORMING state
static void dhcp_state_goto_informing(UNUSED dhcp_lease_t* lease) {
#if 0
    dhcp_state_update(lease, dhcp_client_state_informing);

    // after 4 tries, start broadcasting
    if(lease->nr_tries == 4) {
        lease->server_ip = INADDR_BROADCAST;
    }

    dhcp_state_set_timeout(lease, NULL, 1);

    // means here: number of transmissions of INFORMs
    lease->nr_tries++;

    // send discover
    dhcp_packet_send(lease, DHCPINFORM);
#endif
}

// Go to the INFORM OK state
static void dhcp_state_goto_inform_ok(UNUSED dhcp_lease_t* lease, UNUSED dhcp_packet_t* packet) {
    // Get the value of the requested options
    // Req options to datamodel
    //dhclient_options_get_param_rqs_values_from_packet(lease, packet);

    dhcp_state_update(lease, dhcp_client_state_inform_ok);

    // don't accept other ACKs
    lease->xid = 0;

    // this lease never expires
    lease->expires.tv_sec = INFINITE_DELAY;
    lease->deadline.tv_sec = INFINITE_DELAY;

    lease->nr_tries = 0;
    SAH_TRACE_NOTICE("Lease[%s] %u.%u.%u.%u goes to inform ok.", lease->id, DHCP_IP_QUAD(lease->ip_addr));
}

// Called when an OFFER has been received
static void dhcp_state_on_offer(dhcp_lease_t* lease, dhcp_packet_t* packet) {
    uint32_t subnet = 0;

    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(packet, exit, ERROR, "Packet is NULL");

    SAH_TRACEZ_WARNING(ME, "[%s] Offer received", lease->id);

    // an OFFER can only be received in the INIT state
    if(lease->state != dhcp_client_state_selecting) {
        dhcp_error_update(lease, DHCLIENT_ERR_STATE_FAILURE);
        SAH_TRACEZ_ERROR(ME, "[%s] Offer received in wrong state[%d].", lease->id, lease->state);
        goto exit;
    }

    if(dhcp_packet_check_lease(packet) != 0) {
        dhcp_error_update(lease, DHCLIENT_ERR_MSG_FAILURE);
        SAH_TRACEZ_ERROR(ME, "[%s] Invalid OFFER received from [%u.%u.%u.%u]", lease->id,
                         DHCP_IP_QUAD(packet->other.sin_addr.s_addr));
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "[%s] Offer basic checks are ok", lease->id);
    if(dhcp_check_authinfo(lease, packet, lease->check_authentication) != 0) {
        dhcp_error_update(lease, DHCLIENT_ERR_AUTHENTICATION_FAILURE);
        SAH_TRACEZ_ERROR(ME, "[%s] Offer received with bad Authentication Information.", lease->id);
        goto exit;
    }

    // offer is acceptable, set ip address
    if(packet->dhcp_option[TAG_SUBNET_MASK].len == 4) {
        READ_OPTION(packet->dhcp_option[TAG_SUBNET_MASK], &subnet, sizeof(subnet));
    } else {
        subnet = ip_to_classfull_mask(packet->raw.yiaddr);
    }
    dhcp_lease_addr_update(lease, packet->raw.yiaddr, subnet);

    // Add the received ip address to the txoption list
    dhcp_lease_config_req_ip_option(lease);

    // Extract the server identifier; must be present
    READ_OPTION(packet->dhcp_option[TAG_SERVER_ID], &lease->server_ip, sizeof(lease->server_ip));

    //we are in the correct state, stop the DISCOVER retransmits:
    SAH_TRACEZ_INFO(ME, "[%s] Valid OFFER, Stop retransmit timer", lease->id);
    amxp_timer_stop(lease->retransmit_timer);

    SAH_TRACEZ_NOTICE(ME, "[%s] DHCP server (%u.%u.%u.%u) offers %u.%u.%u.%u", lease->id,
                      DHCP_IP_QUAD(lease->server_ip), DHCP_IP_QUAD(lease->ip_addr));
    // start requesting the offer
    dhcp_state_goto_requesting(lease);

exit:
    return;
}

// Called when an ACK has been received.
// an ACK may be accepted in any state.
static void dhcp_state_on_ack(dhcp_lease_t* lease, dhcp_packet_t* packet) {
    uint32_t server_id = 0;

    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(packet, exit, ERROR, "Packet is NULL");

    SAH_TRACEZ_WARNING(ME, "[%s] Ack received", lease->id);

    // ACK received, stop retransmission of REQUESTS.
    SAH_TRACEZ_INFO(ME, "[%s] ACK received, Stop retransmit timer", lease->id);
    amxp_timer_stop(lease->retransmit_timer);

    if(dhcp_packet_check_lease(packet) == 0) {
        uint32_t subnet = 0;

        // extract the server identifier; must be present or we don't come here
        READ_OPTION(packet->dhcp_option[TAG_SERVER_ID], &server_id, sizeof(server_id));

        // check the server if not REBINDING or REBOOTING; in which case the server is not yet known
        if((lease->state == dhcp_client_state_rebinding) || (lease->state == dhcp_client_state_rebooting)) {
            lease->server_ip = server_id;
        } else if(lease->server_ip != server_id) {

            SAH_TRACE_WARNING("[%s] Ack from different server %u.%u.%u.%u; %u.%u.%u.%u expected.",
                              lease->id, DHCP_IP_QUAD(server_id), DHCP_IP_QUAD(lease->server_ip));
            lease->server_ip = server_id;
        }

        // it's possible that we get a different lease than the one offered
        if(packet->dhcp_option[TAG_SUBNET_MASK].len == 4) {
            READ_OPTION(packet->dhcp_option[TAG_SUBNET_MASK], &subnet, sizeof(subnet));
        } else {
            subnet = ip_to_classfull_mask(packet->raw.yiaddr);
        }

        dhcp_lease_addr_update(lease, packet->raw.yiaddr, subnet);
        dhcp_lease_config_req_ip_option(lease);

        SAH_TRACEZ_NOTICE(ME, "[%s] %u.%u.%u.%u acks %u.%u.%u.%u.", lease->id,
                          DHCP_IP_QUAD(lease->server_ip), DHCP_IP_QUAD(packet->raw.yiaddr));

        // move to bound state
        dhcp_state_goto_bound(lease, packet);
    } else if(lease->state == dhcp_client_state_informing) {
        if(packet->dhcp_option[TAG_SERVER_ID].len == 4) {
            READ_OPTION(packet->dhcp_option[TAG_SERVER_ID], &server_id, sizeof(server_id));
            lease->server_ip = server_id;
            dhcp_state_goto_inform_ok(lease, packet);
        } else {
            // store the error code invalid server
            dhcp_error_update(lease, DHCLIENT_ERR_SERVER_IP_FAILURE);
            SAH_TRACEZ_WARNING(ME, "[%s] No server id option in ack received from [%u.%u.%u.%u]",
                               lease->id, DHCP_IP_QUAD(packet->other.sin_addr.s_addr));
            goto exit;
        }
    } else {
        // store the error code invalid message
        dhcp_error_update(lease, DHCLIENT_ERR_MSG_FAILURE);
        SAH_TRACEZ_WARNING(ME, "[%s] Invalid ack received from [%u.%u.%u.%u]",
                           lease->id, DHCP_IP_QUAD(packet->other.sin_addr.s_addr));
        goto exit;
    }
    dhcp_error_update(lease, DHCLIENT_ERR_NONE);

exit:
    return;
}

// Called when a NAK has been received.
static void dhcp_state_on_nak(dhcp_lease_t* lease, dhcp_packet_t* packet) {
    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(packet, exit, ERROR, "Packet is NULL");

    SAH_TRACEZ_WARNING(ME, "[%s] %u.%u.%u.%u naks %u.%u.%u.%u.", lease->id,
                       DHCP_IP_QUAD(packet->other.sin_addr.s_addr),
                       DHCP_IP_QUAD(lease->ip_addr));

    // the transaction id of the NAK must match the one from the REQUEST
    if((lease->xid == 0) || (ntohl(packet->raw.xid) != lease->xid)) {
        dhcp_error_update(lease, DHCLIENT_ERR_XID_FAILURE);
        SAH_TRACEZ_ERROR(ME, "[%s] Nak received from %u.%u.%u.%u with invalid id", lease->id,
                         DHCP_IP_QUAD(packet->other.sin_addr.s_addr));
        goto exit;
    }

    switch(lease->state) {
    case dhcp_client_state_rebooting:
        amxp_timer_stop(lease->retransmit_timer);
        // the known IP address gets NAKed by a server, cleanup everything
        dhcp_state_cleanup_data(lease, true);
        dhcp_error_update(lease, DHCLIENT_ERR_REMOTE_NACK);
        // restart in the INIT state
        dhcp_state_goto_init(lease);
        break;

    case dhcp_client_state_requesting:
    case dhcp_client_state_renewing:
    case dhcp_client_state_rebinding:
        if(packet->dhcp_option[TAG_SERVER_ID].len == 4) {
            uint32_t server_id = 0;
            READ_OPTION(packet->dhcp_option[TAG_SERVER_ID], &server_id, sizeof(server_id));

            // check the server
            if(lease->server_ip != server_id) {
                SAH_TRACEZ_WARNING(ME, "Nak from different server %u.%u.%u.%u; %u.%u.%u.%u expected.",
                                   DHCP_IP_QUAD(server_id), DHCP_IP_QUAD(lease->server_ip));
            }
            SAH_TRACEZ_INFO(ME, "Nak from server %u.%u.%u.%u received.", DHCP_IP_QUAD(server_id));

            // cleanup everything
            amxp_timer_stop(lease->retransmit_timer);
            dhcp_state_cleanup_data(lease, true);
            // remember time when address acquisition starts in renewing or rebinding state
            if(lease->state != dhcp_client_state_requesting) {
                get_timestamp(&(lease->acquiring_time));
            }

            dhcp_error_update(lease, DHCLIENT_ERR_REMOTE_NACK);
            // start acquiring a new lease
            dhcp_state_goto_init(lease);
        } else {
            SAH_TRACEZ_ERROR(ME, "[%s] No server id option in nak from %u.%u.%u.%u", lease->id,
                             DHCP_IP_QUAD(packet->other.sin_addr.s_addr));
        }
        break;

    default:
        SAH_TRACEZ_ERROR(ME, "[%s] Receive NAK in wrong state[%d]", lease->id, lease->state);
        break;     // ignore NAKs when received in other states
    }

exit:
    return;
}

// Called when a FORCERENEW has been received.
// The XID can not be used to identify the lease, use ciaddr instead.
// The packet is checked to be UNICAST
// Authentication is being checked.
// Then go to the rebind state.
static void dhcp_state_on_force_renew(dhcp_lease_t* lease, dhcp_packet_t* packet) {
    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(packet, exit, ERROR, "Packet is NULL");

    SAH_TRACEZ_WARNING(ME, "[%s] %u.%u.%u.%u force renews %u.%u.%u.%u.", lease->id,
                       DHCP_IP_QUAD(packet->other.sin_addr.s_addr),
                       DHCP_IP_QUAD(lease->ip_addr));
    SAH_TRACEZ_INFO(ME, "[%s] Force Renew received, stop retransmit timer", lease->id);
    amxp_timer_stop(lease->retransmit_timer);

    if(dhcp_packet_check_lease(packet) != 0) {
        SAH_TRACEZ_WARNING(ME, "[%s] Invalid FORCERENEW received from [%u.%u.%u.%u]",
                           lease->id, DHCP_IP_QUAD(packet->other.sin_addr.s_addr));
        dhcp_error_update(lease, DHCLIENT_ERR_MSG_FAILURE);
        goto exit;
    }

    // Check authentication information
    if(dhcp_check_authinfo(lease, packet, true) != 0) {
        dhcp_error_update(lease, DHCLIENT_ERR_AUTHENTICATION_FAILURE);
        SAH_TRACEZ_ERROR(ME, "[%s] FORCERENEW received with bad Authentication Information.",
                         lease->id);
        goto exit;
    }

    switch(lease->state) {
    case dhcp_client_state_bound:
        amxp_timer_start(lease->rebind_timer, 0);
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "[%s] Received FORCERENEW in wrong state[%d]",
                         lease->id, lease->state);
        break;
    }

exit:
    return;
}

// Cleanup the lease data according to the state
static void dhcp_state_cleanup_data(dhcp_lease_t* lease, bool del_rqs_ip_option) {
    SAH_TRACEZ_INFO(ME, "[%s] Clean up lease data.", lease->id);
    when_null_trace(lease, exit, ERROR, "Lease is NULL");

    switch(lease->state) {
    case dhcp_client_state_rebooting:
    case dhcp_client_state_selecting:
    case dhcp_client_state_requesting:
        break;

    case dhcp_client_state_bootp:
    case dhcp_client_state_bound:
    case dhcp_client_state_renewing:
    case dhcp_client_state_rebinding:
    case dhcp_client_state_inform_ok:
        // Cleanup the req option values
        dhcp_lease_req_options_clean_values(lease);
        break;

    default:
        break;
    }

    lease->xid = 0;
    lease->server_ip = INADDR_BROADCAST;
    lease->deadline.tv_sec = INFINITE_DELAY;
    lease->client_err = DHCLIENT_ERR_NONE;

    dhcp_lease_init_renew_counter(lease);
    dhcp_lease_addr_update(lease, INADDR_ANY, 0);

    memset(&(lease->uptime), 0, sizeof(struct timeval));
    memset(&(lease->expires), 0, sizeof(struct timeval));
    memset(&(lease->timeout), 0, sizeof(struct timeval));
    memset(&(lease->leasetime), 0, sizeof(struct timeval));
    memset(&(lease->renew_time), 0, sizeof(struct timeval));
    memset(&(lease->rebind_time), 0, sizeof(struct timeval));

    // Reset requested IP address
    if(del_rqs_ip_option) {
        amxc_var_t* req_ip = option_list_find_tag(&lease->sent_options, TAG_REQ_IP);
        if(req_ip != NULL) {
            amxc_var_take_it(req_ip);
            amxc_var_delete(&req_ip);
        }
    }

    // stop timers
    SAH_TRACEZ_INFO(ME, "[%s] Clean lease, stop retransmit timers", lease->id);
    amxp_timer_stop(lease->retransmit_timer);
    amxp_timer_stop(lease->rebind_timer);
    amxp_timer_stop(lease->renew_timer);

exit:
    return;
}

// Packet has been received on the CLIENT BOOTP port (68)
// The message is a DHCP message
void dhcp_state_on_dhcp(dhcp_lease_t* lease, dhcp_packet_t* packet) {
    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_null_trace(packet, exit, ERROR, "Packet is NULL");

    switch(packet->packet_type) {
    case DHCPOFFER:
        dhcp_state_on_offer(lease, packet);
        dhcp_stat_inc_counter(DHCLIENT_CNT_NO_RECV_OFFER);
        break;
    case DHCPACK:
        dhcp_state_on_ack(lease, packet);
        dhcp_stat_inc_counter(DHCLIENT_CNT_NO_RECV_ACK);
        break;
    case DHCPNAK:
        dhcp_state_on_nak(lease, packet);
        dhcp_stat_inc_counter(DHCLIENT_CNT_NO_RECV_NAK);

        break;
    case DHCPFORCERENEW:
        dhcp_state_on_force_renew(lease, packet);
        dhcp_stat_inc_counter(DHCLIENT_CNT_NO_RECV_FORCERENEW);
        break;
    default:
        break;     // ignore all the rest
    }

exit:
    return;
}

// Called when a BOOTP packet has been received.
void dhcp_state_on_bootp(UNUSED dhcp_lease_t* lease, UNUSED dhcp_packet_t* packet) {
#if 0
    SAH_TRACEZ_NOTICE(ME, "[%s] Bootp server %u.%u.%u.%u reply", lease->id, DHCP_IP_QUAD(packet->other.sin_addr.s_addr));

    //stop retransmission
    SAH_TRACEZ_INFO(ME, "[%s] Bootp packet received, stop retransmit timer", lease->id);
    amxp_timer_stop(lease->retransmit_timer);

    //dhclient_stat_inc_counter(DHCLIENT_CNT_NO_RECV_BOOTREPLY);

    // the transaction id of the REPLY must match the one from the REQUEST
    if(!lease->xid || (ntohl(packet->raw.xid) != lease->xid)) {
        dhcp_error_update(lease, DHCLIENT_ERR_XID_FAILURE);
        SAH_TRACEZ_ERROR(ME, "[%s] Bootp from %u.%u.%u.%u with invalid id", lease->id, DHCP_IP_QUAD(packet->other.sin_addr.s_addr));
        return;
    }

    // ignore when we are already being served by a real DHCP server
    if((lease->state != dhcp_client_state_init) && (lease->state != dhcp_client_state_selecting)) {
        SAH_TRACEZ_ERROR(ME, "[%s] Discard bootp server, already found dhcp server.", lease->id);
        return;
    }

    // Get the value of the requested options
    dhclient_options_get_param_rqs_values_from_packet(lease, packet);

    // remember the server's ip address
    lease->server_ip = packet->other.sin_addr.s_addr;
    // configure new ip address
    if(!lease->flags.flag_formal) {
        if(dhclient_lease_config_ip(lease, packet->raw.yiaddr, 0L, packet->raw.giaddr) != ERR_NO_ERROR) {
            dhcp_state_cleanup_data(lease, true);
            return;
        }
    }

    // Store the address in the requested IP address option
    dhclient_lease_config_rqs_ip_option(lease);

    // store the relative time when the IP address is assigned
    get_timestamp(&(lease->uptime));

    // move to bootp state
    dhcp_state_update(lease, dhcp_client_state_bootp);
    SAH_TRACEZ_NOTICE(ME, "[%s] Bootp lease %u.%u.%u.%u bound", lease->id, DHCP_IP_QUAD(lease->ip_addr));

    // make sure other responses are no longer accepted
    lease->xid = 0;

    // does not expire; renew and rebinding time not used in this state
    lease->expires.tv_sec = INFINITE_DELAY;
    lease->lease_time.tv_sec = INFINITE_DELAY;
    lease->renew_time.tv_sec = INFINITE_DELAY;
    lease->rebind_time.tv_sec = INFINITE_DELAY;

    dhcp_error_update(lease, DHCLIENT_ERR_NONE);
#endif
}

static void dhcp_state_release_on_reboot(dhcp_lease_t* lease) {
    int ret = 0;
    int file = -1;
    int noroute = 1;
    amxc_string_t str;
    amxc_var_t* data = NULL;
    const char* filename = NULL;
    variant_json_t* reader = NULL;

    amxc_string_init(&str, 0);
    amxj_reader_new(&reader);

    when_null_trace(lease, exit, ERROR, "Lease is NULL");
    when_str_empty_trace(lease->ifname, exit, ERROR, "Lease-> ifname is empty");

    amxc_string_setf(&str, "%s/dhcplease_%s", lease->storage_path, lease->ifname);
    filename = amxc_string_get(&str, 0);
    file = open(filename, O_RDONLY);
    when_true_trace(file < 0, exit, ERROR, "Failed to open file %s for reading", filename);

    ret = amxj_read(reader, file);
    while(ret > 0) {
        ret = amxj_read(reader, file);
    }

    data = amxj_reader_result(reader);
    close(file);

    ret = dhcp_var_to_lease_info(lease, data);
    when_failed_trace(ret, exit, ERROR, "Failed to parse lease information from file");

    ret = setsockopt(lease->intf->dhcp_raw_sock, SOL_SOCKET, SO_DONTROUTE, &noroute, sizeof(noroute));
    when_true_trace(ret < 0, exit, ERROR, "Setsockopt(SO_DONTROUTE) failed: %d", ret);

    lease->raw_socket = true;
    dhcp_packet_send(lease, DHCPRELEASE);

    noroute = 0;
    ret = setsockopt(lease->intf->dhcp_raw_sock, SOL_SOCKET, SO_DONTROUTE, &noroute, sizeof(noroute));
    when_true_trace(ret < 0, exit, ERROR, "Setsockopt(SO_DONTROUTE) failed: %d", ret);

    // Clean some of the lease parameters, otherwise they will be used for the next DORA cycle
    lease->xid = 0;
    lease->ip_addr = 0;
    lease->netmask = 0;
    lease->server_ip = 0;
    lease->raw_socket = false;
    lease->release_reboot_done = true;
    memset(lease->server_lladdr, 0, sizeof(lease->server_lladdr));

exit:
    amxc_var_delete(&data);
    amxc_string_clean(&str);
    amxj_reader_delete(&reader);
    return;
}

// Timeout occured when waiting for packet, retransmit timeout
void dhcp_state_on_timeout(dhcp_lease_t* lease) {
    struct timeval now;
    get_timestamp(&now);

    switch(lease->state) {
    case dhcp_client_state_init_reboot:
        // ready to start REQUESTing previously known lease
        dhcp_state_goto_rebooting(lease);
        break;

    case dhcp_client_state_rebooting:
        // wait for ACK expired
        if(lease->nr_tries-- > 0) {
            // we can try again
            if(lease->nr_tries == 0) {
                lease->timeout.tv_sec = REPLY_TIMEOUT;
                now.tv_sec += REPLY_TIMEOUT;
                randomize_time(&now, &(lease->deadline));
                dhcp_state_set_timeout(lease, &(lease->deadline), 1);
            } else {
                dhcp_state_set_timeout(lease, 0, 0);
            }

            dhcp_packet_send(lease, DHCPREQUEST);
        } else {
            dhcp_state_goto_init(lease);
        }
        break;

    case dhcp_client_state_init:
        // ready to start DISCOVERing
        // First check if a release must be done at reboot
        if(lease->release_reboot && !lease->release_reboot_done) {
            dhcp_state_release_on_reboot(lease);
        }

        dhcp_state_goto_selecting(lease);
        break;

    case dhcp_client_state_selecting:
        // waiting for OFFER expired
        dhcp_state_set_timeout(lease, 0, 0);
        lease->nr_tries++;
        dhcp_packet_send(lease, DHCPDISCOVER);
        break;

    case dhcp_client_state_requesting:
        // waiting for ACK expired
        if(lease->nr_tries-- > 0) {
            SAH_TRACEZ_INFO(ME, "State: dhcp_client_state_requesting, nr_of_tries[%d]", lease->nr_tries);
            // we can try again
            if(lease->nr_tries == 0) {
                lease->timeout.tv_sec = REPLY_TIMEOUT;
                set_timer(lease->retransmit_timer, &(lease->timeout));
            } else {
                dhcp_state_set_timeout(lease, 0, 0);
            }
            dhcp_packet_send(lease, DHCPREQUEST);
        } else {
            SAH_TRACEZ_INFO(ME, "State: dhcp_client_state_requesting, [%d] goto init state", lease->nr_tries);
            // store the error code requesting timeout
            dhcp_error_update(lease, DHCLIENT_ERR_REQUESTING_TIMEOUT);
            dhcp_state_goto_init(lease);
        }
        break;

    case dhcp_client_state_bound:
        // remember time when renewal process is started
        get_timestamp(&(lease->acquiring_time));

        if(timercmp(&(lease->renew_time), &(lease->rebind_time), CMP_OPERATOR_LT)) {
            // go to the RENEWING state
            dhcp_state_goto_renewing(lease);
        } else {
            // go to the REBINDING state
            dhcp_state_goto_rebinding(lease);
        }
        break;

    case dhcp_client_state_renewing:
        if(lease->renew_counter != -1) {
            if(timercmp(&(lease->rebind_time), &now, CMP_OPERATOR_GT) && (lease->nr_tries <= lease->renew_counter)) {
                SAH_TRACEZ_NOTICE(ME, "Retransmit REQUEST, renew counter[%d of %d], calculate delay", lease->nr_tries, lease->renew_counter);
                dhcp_packet_send(lease, DHCPREQUEST);
            } else {
                SAH_TRACEZ_NOTICE(ME, "Retransmit REQUEST done[%d of %d] or rebind time expired", lease->nr_tries, lease->renew_counter);
                dhcp_packet_send(lease, DHCPRELEASE);
                dhcp_state_cleanup_data(lease, true);
                dhcp_error_update(lease, DHCLIENT_ERR_RENEWING_TIMEOUT);
                dhcp_state_goto_init(lease);
            }
        } else {
            if(timercmp(&(lease->rebind_time), &now, CMP_OPERATOR_GT)) {
                // calculate remaining time until rebinding
                dhcp_state_set_timeout(lease, &(lease->rebind_time), 0);
                dhcp_packet_send(lease, DHCPREQUEST);
            } else {
                // go to the REBINDING state
                dhcp_state_goto_rebinding(lease);
                dhcp_error_update(lease, DHCLIENT_ERR_RENEWING_TIMEOUT);
            }
        }
        break;

    case dhcp_client_state_rebinding:
        if(timercmp(&(lease->expires), &now, CMP_OPERATOR_GT)) {
            // calculate remaining time until expiring
            dhcp_state_set_timeout(lease, &(lease->expires), 0);
            dhcp_packet_send(lease, DHCPREQUEST);
        } else {
            // cleanup everything including the req IP option
            dhcp_state_cleanup_data(lease, true);
            dhcp_error_update(lease, DHCLIENT_ERR_REBINDING_TIMEOUT);
            // remember time when address acquisition starts
            get_timestamp(&(lease->acquiring_time));
            // we go back to the initial state of DHCP
            dhcp_state_goto_init(lease);
        }
        break;

    case dhcp_client_state_bootp:
        // this is due to a user command, like renew cleanup everything, keep the req IP option
        dhcp_state_cleanup_data(lease, false);
        // remember time when address acquisition starts
        get_timestamp(&(lease->acquiring_time));
        // we go back to the initial state of DHCP
        dhcp_state_goto_init(lease);
        break;

    case dhcp_client_state_informing:
    // send another INFORM message
    case dhcp_client_state_inform_ok:
        // reaction on renew: restart INFORMing store the error code inform timeout
        dhcp_error_update(lease, DHCLIENT_ERR_INFORM_TIMEOUT);
        dhcp_state_goto_informing(lease);
        break;

    default:
        // ignore this event in all other states
        break;
    }
}

// Timeout occured for renew, only applicable in state BOUND
void dhcp_state_on_timeout_renew(dhcp_lease_t* lease) {
    switch(lease->state) {
    case dhcp_client_state_init_reboot:
    case dhcp_client_state_rebooting:
    case dhcp_client_state_init:
    case dhcp_client_state_selecting:
    case dhcp_client_state_requesting:
    case dhcp_client_state_rebinding:
    case dhcp_client_state_bootp:
    case dhcp_client_state_informing:
    case dhcp_client_state_inform_ok:
        SAH_TRACEZ_NOTICE(ME, "[%s], Ignore RENEW timeout in state[%d]", lease->id, lease->state);
        break;
    case dhcp_client_state_renewing:
    case dhcp_client_state_bound:
        SAH_TRACEZ_NOTICE(ME, "[%s], Start RENEW process", lease->id);
        // remember time when renewal process is started, then go RENEWing state
        get_timestamp(&(lease->acquiring_time));
        dhcp_state_goto_renewing(lease);
        break;
    default:
        // ignore this event in all other states
        break;
    }
}

// Timeout occured for rebind
void dhcp_state_on_timeout_rebind(dhcp_lease_t* lease) {
    struct timeval now;
    get_timestamp(&now);

    switch(lease->state) {
    case dhcp_client_state_init_reboot:
    case dhcp_client_state_rebooting:
    case dhcp_client_state_init:
    case dhcp_client_state_selecting:
    case dhcp_client_state_requesting:
    case dhcp_client_state_bootp:
    case dhcp_client_state_informing:
    case dhcp_client_state_inform_ok:
        SAH_TRACEZ_NOTICE(ME, "[%s], Ignore rebind timeout in state[%d]", lease->id, lease->state);
        break;
    case dhcp_client_state_bound:
        // remember time when renewal process is started, go to REBIND state
        get_timestamp(&(lease->acquiring_time));
        amxp_timer_stop(lease->renew_timer);
        dhcp_state_goto_rebinding(lease);
        break;
    case dhcp_client_state_renewing:
        if(timercmp(&(lease->rebind_time), &now, CMP_OPERATOR_GT)) {
            // calculate remaining time until rebinding
            dhcp_state_set_timeout(lease, &(lease->rebind_time), 0);
            dhcp_packet_send(lease, DHCPREQUEST);
        } else {
            // go to the REBINDING state
            dhcp_state_goto_rebinding(lease);
            dhcp_error_update(lease, DHCLIENT_ERR_RENEWING_TIMEOUT);
        }
        break;
    case dhcp_client_state_rebinding:
        // This case handles rebind timeouts
        dhcp_state_goto_rebinding(lease);
        break;
    default:
        // ignore this event in all other states
        break;
    }
}

// Timeout occured when waiting for packet
void dhcp_state_start(dhcp_lease_t* lease) {
    when_null_trace(lease, exit, ERROR, "Lease is NULL");

    // take timestamp when address acquisition starts
    get_timestamp(&(lease->acquiring_time));

    if(lease->flags.flag_inform == 0) {
        uint32_t ip_addr = 0;
        amxc_var_t* req_ip = option_list_find_tag(&lease->sent_options, TAG_REQ_IP);
        if((lease->first_run == 0) && (req_ip != NULL)) {
            dhcp_lease_addr_update(lease, ip_addr, lease->netmask);
            dhcp_state_goto_init_reboot(lease);
        } else {
            lease->first_run = 0;
            dhcp_state_goto_init(lease);
        }
    } else {
        dhcp_state_goto_informing(lease);
    }

exit:
    return;
}

// Stop the lease (put into IDLE state) and clean everything up
void dhcp_state_stop(dhcp_lease_t* lease) {
    SAH_TRACEZ_NOTICE(ME, "[%s], Client in IDLE state.", lease->id);
    dhcp_state_cleanup_data(lease, true);
    dhcp_state_update(lease, dhcp_client_state_idle);
    lease->first_run = 1;
}

// Send a RELEASE message for the specified lease.
void dhcp_state_release(dhcp_lease_t* lease) {
    // send a RELEASE message if necessary
    switch(lease->state) {
    case dhcp_client_state_requesting:
    case dhcp_client_state_bound:
    case dhcp_client_state_renewing:
    case dhcp_client_state_rebinding:
        dhcp_packet_send(lease, DHCPRELEASE);
        break;

    default:
        break;
    }

    // Put the lease in idle state
    dhcp_state_stop(lease);
    dhcp_error_update(lease, DHCLIENT_ERR_LOCAL_RELEASE);
}
