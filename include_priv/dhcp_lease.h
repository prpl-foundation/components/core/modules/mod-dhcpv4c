/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__DHCP_LEASE_H__)
#define __DHCP_LEASE_H__

#include <net/if.h>
#include <stdbool.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include "dhcp_interface.h"
#include "dhcp_options.h"

#define MAX_TX_OPTIONS 16
#define MAX_PARAM_RQS 32
#define ADDR_MAC_LEN 6

#define DHCLIENT_ID_LEN 40
#define DHCLIENT_RETRANSMISSION_STRATEGY_LEN 40

typedef struct dhcp_lease_flags {
    uint8_t flag_admin_enabled   : 1; // lease is admin enabled
    uint8_t flag_inform          : 1; // only doing INFORM on this lease
    uint8_t flag_broadcast       : 1; // when set, can only receive limited broadcasts when the stack is not configured
    uint8_t flag_inform_specaddr : 1; // doing INFORM on a specific address on the interface: otherwise, take the first ip address of the interface (for saving purposes)
    uint8_t flag_server_rt       : 1; // find and inject a route for the associated server
    uint8_t flag_formal          : 1; // this is a formal dhcp client, meaning that the protocol is applied,
                                      // but no configuration is done (no IP-address assignment etc.
    uint8_t flag_follow_rtlb     : 1; // DHCP messages will follow the route label
} dhcp_lease_flags_t;

// Broadcast flags bit field
typedef enum dhcp_broadcast_flag {
    DISCOVER_BFLAGS = 0x01,
    REQUEST_BFLAGS =  0x02,
    REBIND_BFLAGS =   0x04,
    REBOOT_BFLAGS =   0x08,
    DECLINE_BFLAGS =  0x10,
} dhcp_broadcast_flag_t;

typedef enum dhcp_client_state {
    dhcp_client_state_invalid = 0, // invalid state, must be zero (see dhclient_lease.c)
    dhcp_client_state_idle,        // lease created but idle
    dhcp_client_state_init_reboot, // knowledge of previous lease, request it
    dhcp_client_state_rebooting,   // collect ACK replies from dhcp server that knows about previous IP address
    dhcp_client_state_init,        // not yet owning a lease, discover
    dhcp_client_state_selecting,   // collect replies from dhcp server(s)
    dhcp_client_state_requesting,  // request the selected lease
    dhcp_client_state_bound,       // bound: using selected lease
    dhcp_client_state_renewing,    // trying to extend lease on same server
    dhcp_client_state_rebinding,   // trying to extend lease on any server
    dhcp_client_state_bootp,       // pure BOOTP lease
    dhcp_client_state_informing,   // sending INFORM messages
    dhcp_client_state_inform_ok,   // finished INFORMing
    dhcp_client_state_number       // number of states
} dhcp_client_state_t;

typedef struct dhcp_lease {
    bool enable;
    int first_run;                 // used for initial timeout
    char id[DHCLIENT_ID_LEN];      // unique identification of this lease
    dhcp_client_state_t state;     // current state of the lease
    dhcp_lease_flags_t flags;
    unsigned short broadcast_flags;
    uint32_t ip_addr;
    uint32_t netmask;
    long ifindex;
    char ifname[IFNAMSIZ];
    unsigned long xid;             // last used transaction id
    uint32_t server_ip;            // ip address of DHCP server, network order
    amxc_var_t req_options;        // Option request list
    amxc_var_t sent_options;       // Options sent by the DHCP client
    struct timeval lease_time;     // duration of lease
    struct timeval expires;        // time when the lease expires
    struct timeval renew_time;     // time when the lease has to renew
    struct timeval rebind_time;    // time when the lease has to rebind
    struct timeval acquiring_time; // time at which we first attempt to get a new lease (or to extend a lease)
    struct timeval deadline;       // absolute time for select() to expire: current time + timeout (randomized)
    struct timeval timeout;        // timeout for retransmissions
    int nr_tries;                  // number of retransmits
    struct timeval leasetime;      // relative time the client wants to use the lease
    struct timeval uptime;         // relative time the client has uninterrupted an IP address
    unsigned long client_err;      // client error code
    int renew_counter;             // force renew max retransmission
    int tos;
    int priority;
    amxc_var_t retransmission_strategy;
    uint32_t retransmission_randomize;
    int retransmission_step;
    int retransmission_renew_timeout;
    int default_renew_timeout;
    unsigned short secs;            // saved value of secs value in last sent packet
    amxp_timer_t* retransmit_timer; // timer for retransmits
    amxp_timer_t* renew_timer;      // DHCP renew timer
    amxp_timer_t* rebind_timer;     // DHCP rebind timer
    dhcp_interface_t* intf;
    // authentication information
    bool check_authentication;
    char authinfo[AUTHINFO_LEN + 1];
    long long replay_detection;
    bool raw_socket;                // Indicates if the raw socket should be used for sending packets
    bool release_reboot;            // Indicates if this lease should be released after a reboot
    bool release_reboot_done;       // Indicates if this lease has been released after booting
    char* storage_path;             // Location to store the lease info when needed for release on reboot
    uint16_t max_msg_size;          // send maxMsgSize (option 57) in the DHCP DISCOVER and REQUEST
    unsigned char server_lladdr[ADDR_MAC_LEN];
} dhcp_lease_t;

int dhcp_lease_new(dhcp_lease_t** lease, const char* name, dhcp_interface_t* intf);
void dhcp_lease_delete(dhcp_lease_t** lease);
dhcp_lease_t* dhcp_lease_get(unsigned long ifindex, unsigned long xid);
void dhcp_lease_set_initial_wait(dhcp_lease_t* lease);
int dhcp_lease_enable(dhcp_lease_t* lease);
int dhcp_lease_disable(dhcp_lease_t* lease);
void dhcp_lease_addr_update(dhcp_lease_t* lease, uint32_t ip_addr, uint32_t netmask);
void dhcp_lease_config_req_ip_option(dhcp_lease_t* lease);
void dhcp_lease_set_retransmission_params(dhcp_lease_t* lease, amxc_var_t* params);
void dhcp_lease_init_renew_counter(dhcp_lease_t* lease);
void dhcp_lease_set_max_msg_size(dhcp_lease_t* lease);
void dhcp_lease_set_auth_params(dhcp_lease_t* lease, amxc_var_t* params);
void dhcp_lease_set_storage_path(dhcp_lease_t* lease, const char* path);
void dhcp_lease_set_socket_opts(dhcp_lease_t* lease, amxc_var_t* params);
void dhcp_lease_set_broadcast_flag_params(dhcp_lease_t* lease, amxc_var_t* params);

#endif // __DHCP_LEASE_H__
