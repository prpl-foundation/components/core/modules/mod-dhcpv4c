/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__DHCP_PACKET_H__)
#define __DHCP_PACKET_H__

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdint.h>

#include "dhcp_options.h"
#include "dhcp_lease.h"

// DHCP client/server ports
#define DHSERVER_PORT         67
#define DHCLIENT_PORT         68

#define DHRELAY_PORT          49152
#define DHCP_INTF_ANY         0xFFFF

#define DHCP_OPTION_SIZE      1512
#define DHCP_PACKET_SIZE      sizeof(dhcp_raw_packet_t)
#define DHCP_MIN_SIZE         (DHCP_PACKET_SIZE - DHCP_OPTION_SIZE)
#define DHCP_MIN_MAX_MSG_SIZE 576 // minimum size of the max_msg_size option (57), rfc 2132

#define BOOTP_MIN_LEN         300 // minimum length of a BOOTP packet in bytes

#define CHADDR_LEN            16
#define SNAME_LEN             64
#define FILE_LEN              128

#define BOOTP_BROADCAST_FLAG  0x8000

#define IPHDR_LEN             20
#define UDPHDR_LEN            8

// overlay that extends standard struct sockaddr with extra info
struct sockaddr_in_ext {
    short sin_family;                        // must be AF_INET
    unsigned short sin_port;                 // 16-bit port number
    struct in_addr sin_addr;                 // 32-bit IP address
    unsigned short sin_intf;                 // rx : intf we received on; tx : if to send to
    unsigned short sin_port2;                // rx : dst port; tx : src port to enforce
    struct in_addr sin_addr2;                // rx : dst ip; tx : src ip to enforce
};

// Opcodes (header: "op" field)
typedef enum dhcp_opcode {
    BOOTREQUEST = 1,
    BOOTREPLY
} dhcp_opcode_t;

// Message types [TAG_DHCP_MESS_TYPE]
typedef enum dhcp_msgtype {
    BOOTP = 0,
    DHCPDISCOVER,
    DHCPOFFER,
    DHCPREQUEST,
    DHCPDECLINE,
    DHCPACK,
    DHCPNAK,
    DHCPRELEASE,
    DHCPINFORM,
    DHCPFORCERENEW
} dhcp_msg_type_t;

// Define the structure of a DHCP packet
typedef struct __attribute__((packed)) dhcp_raw_packet {
    unsigned char opcode;             // opcode type
    unsigned char htype;              // hardware address type
    unsigned char hlen;               // hardware len
    unsigned char hops;               // number of hops through gateway
    uint32_t xid;                     // transaction id
    uint16_t secs;                    // seconds since trying to get a lease
    uint16_t flags;
    uint32_t ciaddr;                  // client IP address
    uint32_t yiaddr;                  // your IP address
    uint32_t siaddr;                  // server IP address
    uint32_t giaddr;                  // gateway IP address
    unsigned char chaddr[CHADDR_LEN]; // client's hardware address
    char sname[SNAME_LEN];            // server host name
    char fname[FILE_LEN];             // boot file name
    char options[DHCP_OPTION_SIZE];   // DHCP options
} dhcp_raw_packet_t;

// Define the DHCP packet with parsed information
typedef struct dhcp_packet {
    dhcp_raw_packet_t raw;
    unsigned short packet_size;
    dhcp_msg_type_t packet_type;
    int options_valid;
    struct sockaddr_in_ext other;
    int tos;
    int priority;
    dhcp_option_t dhcp_option[TAG_END];
    unsigned char sll_halen;            // mac addres length as received by raw socket
    unsigned char sll_addr[CHADDR_LEN]; // mac addres as received by raw socket
    uint32_t saddr;                     // source address, specified in IP Header, in network byte order.
    unsigned char optionlist[256];      // ordered list of send options
    long ifindex;
} dhcp_packet_t;

// initializes the packet
void dhcp_packet_init(dhcp_packet_t* packet);

// recv a packet from raw socket
int sys_recv_dhcp_packet_raw(int s, dhcp_packet_t* packet, unsigned char bootp_opcode);

// send packet on the socket
int sys_send_dhcp_packet(int s, dhcp_packet_t* packet, uint32_t local_ip);

int dhcp_packet_check_lease(dhcp_packet_t* packet);
void dhcp_packet_send(dhcp_lease_t* lease, dhcp_msg_type_t msgtype);

#endif // __DHCP_PACKET_H__
