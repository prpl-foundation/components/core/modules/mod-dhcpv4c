/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__DHCP_OPTIONS_H__)
#define __DHCP_OPTIONS_H__

#include <stdint.h>
#include <stdbool.h>

#define READ_OPTION(DHCPOPT, TARGET, MAXLENGTH) memcpy(TARGET, DHCPOPT.value_ptr, DHCPOPT.len > MAXLENGTH ? MAXLENGTH : DHCPOPT.len)

// Tag values used to specify what information is being supplied in
// the vendor (options) data area of the packet.
// RFC 1048
// padding for alignment
#define TAG_PAD                        ((unsigned char) 0)
// Subnet mask
#define TAG_SUBNET_MASK                ((unsigned char) 1)
// Time offset from UTC for this system
#define TAG_TIME_OFFSET                ((unsigned char) 2)
// List of routers on this subnet
#define TAG_GATEWAY                    ((unsigned char) 3)
// List of rfc868 time servers available to client
#define TAG_TIME_SERVER                ((unsigned char) 4)
// List of IEN 116 name servers
#define TAG_NAME_SERVER                ((unsigned char) 5)
// List of DNS name servers
#define TAG_DOMAIN_SERVER              ((unsigned char) 6)
// List of MIT-LCS UDL log servers
#define TAG_LOG_SERVER                 ((unsigned char) 7)
// List of rfc865 cookie servers
#define TAG_COOKIE_SERVER              ((unsigned char) 8)
// List of rfc1179 printer servers (in order to try)
#define TAG_LPR_SERVER                 ((unsigned char) 9)
// List of Imagen Impress servers (in prefered order)
#define TAG_IMPRESS_SERVER             ((unsigned char) 10)
// List of rfc887 Resourse Location servers
#define TAG_RLP_SERVER                 ((unsigned char) 11)
// Hostname of client
#define TAG_HOST_NAME                  ((unsigned char) 12)
// boot file size
#define TAG_BOOT_SIZE                  ((unsigned char) 13)
// RFC 1395
// path to dump to in case of crash
#define TAG_DUMP_FILE                  ((unsigned char) 14)
// domain name for use with the DNS
#define TAG_DOMAIN_NAME                ((unsigned char) 15)
// IP address of the swap server for this machine
#define TAG_SWAP_SERVER                ((unsigned char) 16)
// The path name to the root filesystem for this machine
#define TAG_ROOT_PATH                  ((unsigned char) 17)
// RFC 1497
// filename to tftp with more options in it
#define TAG_EXTEN_FILE                 ((unsigned char) 18)
// RFC 1533
// The following are in rfc1533 and may be used by BOOTP/DHCP
// IP forwarding enable/disable
#define TAG_IP_FORWARD                 ((unsigned char) 19)
// Non-Local source routing enable/disable
#define TAG_IP_NLSR                    ((unsigned char) 20)
// List of pairs of addresses/masks to allow non-local source routing to
#define TAG_IP_POLICY_FILTER           ((unsigned char) 21)
// Maximum size of datagrams client should be prepared to reassemble
#define TAG_IP_MAX_DRS                 ((unsigned char) 22)
// Default IP TTL
#define TAG_IP_TTL                     ((unsigned char) 23)
// Timeout in seconds to age path MTU values found with rfc1191
#define TAG_IP_MTU_AGE                 ((unsigned char) 24)
// Table of MTU sizes to use when doing rfc1191 MTU discovery
#define TAG_IP_MTU_PLAT                ((unsigned char) 25)
// MTU to use on this interface
#define TAG_IP_MTU                     ((unsigned char) 26)
// All subnets are local option
#define TAG_IP_SNARL                   ((unsigned char) 27)
// broadcast address
#define TAG_IP_BROADCAST               ((unsigned char) 28)
// perform subnet mask discovery using ICMP
#define TAG_IP_SMASKDISC               ((unsigned char) 29)
// act as a subnet mask server using ICMP
#define TAG_IP_SMASKSUPP               ((unsigned char) 30)
// perform rfc1256 router discovery
#define TAG_IP_ROUTERDISC              ((unsigned char) 31)
// address to send router solicitation requests
#define TAG_IP_ROUTER_SOL_ADDR         ((unsigned char) 32)
// list of classfull static routes (addr, router) pairs
#define TAG_IP_CLASSFULL_STAT_RT       ((unsigned char) 33)
// use trailers (rfc893) when using ARP
#define TAG_IP_TRAILER_ENC             ((unsigned char) 34)
// timeout in seconds for ARP cache entries
#define TAG_ARP_TIMEOUT                ((unsigned char) 35)
// use either Ethernet version 2 (rfc894) or IEEE 802.3 (rfc1042)
#define TAG_ETHER_IEEE                 ((unsigned char) 36)
// default TCP TTL when sending TCP segments
#define TAG_IP_TCP_TTL                 ((unsigned char) 37)
// time for client to wait before sending a keepalive on a TCP connection
#define TAG_IP_TCP_KA_INT              ((unsigned char) 38)
// don't send keepalive with an octet of garbage for compatability
#define TAG_IP_TCP_KA_GARBAGE          ((unsigned char) 39)
// NIS domainname
#define TAG_NIS_DOMAIN                 ((unsigned char) 40)
// list of NIS servers
#define TAG_NIS_SERVER                 ((unsigned char) 41)
// list of NTP servers
#define TAG_NTP_SERVER                 ((unsigned char) 42)
// and stuff vendors may want to add
#define TAG_VENDOR_SPECIFIC_INFO       ((unsigned char) 43)
// NetBios over TCP/IP name server
#define TAG_NBNS_SERVER                ((unsigned char) 44)
// NetBios over TCP/IP NBDD servers (rfc1001/1002)
#define TAG_NBDD_SERVER                ((unsigned char) 45)
// NetBios over TCP/IP node type option for use with above
#define TAG_NBOTCP_OTPION              ((unsigned char) 46)
// NetBios over TCP/IP scopt option for use with above
#define TAG_NB_SCOPE                   ((unsigned char) 47)
// list of X Window system font servers
#define TAG_XFONT_SERVER               ((unsigned char) 48)
// list of systems running X Display Manager (xdm) available to this client
#define TAG_XDISPLAY_SERVER            ((unsigned char) 49)
// requested IP address
#define TAG_REQ_IP                     ((unsigned char) 50)
// lease time for IP address
#define TAG_IP_LEASE_TIME              ((unsigned char) 51)
// option overloading
#define TAG_OPTION_OVERLOAD            ((unsigned char) 52)
// message type
#define TAG_DHCP_MSG_TYPE              ((unsigned char) 53)
// server identification
#define TAG_SERVER_ID                  ((unsigned char) 54)
// ordered list of requested parameters
#define TAG_PARM_REQ_LIST              ((unsigned char) 55)
// error text message
#define TAG_DHCP_TEXT_MSG              ((unsigned char) 56)
// DHCP maximum packet size willing to accept
#define TAG_DHCP_MAX_MSG_SIZE          ((unsigned char) 57)
// time till client needs to renew
#define TAG_RENEWAL_TIME               ((unsigned char) 58)
// time till client needs to rebind
#define TAG_REBIND_TIME                ((unsigned char) 59)
// Vendor Class identifier
#define TAG_VENDOR_CLASS_ID            ((unsigned char) 60)
// client unique identifier
#define TAG_CLIENT_ID                  ((unsigned char) 61)
// NWIP domain name
#define TAG_NWIP_DOMAIN                ((unsigned char) 62)
// NWIP suboptions
#define TAG_NWIP_SUBOPTIONS            ((unsigned char) 63)
// NIS+ domain name
#define TAG_NIS_PLUS_DOMAIN            ((unsigned char) 64)
// list of NIS+ servers
#define TAG_NIS_PLUS_SERVER            ((unsigned char) 65)
// list of TFTP servers
#define TAG_TFTP_SERVER                ((unsigned char) 66)
// Bootfile name
#define TAG_BOOTFILE_NAME              ((unsigned char) 67)
// list of mobile IP home agents
#define TAG_MOBILE_IP_HOME_AGENT       ((unsigned char) 68)
// list of SMTP servers
#define TAG_SMTP_SERVER                ((unsigned char) 69)
// list of POP3 servers
#define TAG_POP3_SERVER                ((unsigned char) 70)
// list of NNTP servers
#define TAG_NNTP_SERVER                ((unsigned char) 71)
// list of WWW servers
#define TAG_WWW_SERVER                 ((unsigned char) 72)
// list of FINGER servers
#define TAG_FINGER_SERVER              ((unsigned char) 73)
// list of IRC servers
#define TAG_IRC_SERVER                 ((unsigned char) 74)
// list of streetTalk servers
#define TAG_ST_SERVER                  ((unsigned char) 75)
// list of streetTalk Directory Assistence servers
#define TAG_STDA_SERVER                ((unsigned char) 76)
// user Class identifier
#define TAG_USER_CLASS_ID              ((unsigned char) 77)
// list of slp directory agents
#define TAG_SLP_DIRECTORY_AGENT        ((unsigned char) 78)
/* list of slp service*/
#define TAG_SLP_SERVICE_SCOPE          ((unsigned char) 79)
// rapid commit option
#define TAG_RAPID_COMMIT               ((unsigned char) 80)
// FQDN name
#define TAG_FQDN               ((unsigned char) 81)
// Relay Agent Information
#define TAG_RELAY_AGENT_INFO           ((unsigned char) 82)
// list of internet storage name servers
#define TAG_INTERNET_STORAGE_SERVERS   ((unsigned char) 83)
// list of NDS servers
#define TAG_NDS_SERVER                 ((unsigned char) 85)
// NDS tree name
#define TAG_NDS_TREE                   ((unsigned char) 86)
// NDS context
#define TAG_NDS_CONTEXT                ((unsigned char) 87)
// DHCP message authentication option
#define TAG_DHCP_MSG_AUTH              ((unsigned char) 90)
// client last transaction time option
#define TAG_CLIENT_LAST_TRANS_TIME     ((unsigned char) 91)
// associated IP option
#define TAG_ASSOCIATED_IP              ((unsigned char) 92)
// User authentication
#define TAG_USER_AUTH                  ((unsigned char) 98)
// auto-configure option
#define TAG_AUTO_CONFIGURE             ((unsigned char) 116)
// name service search option
#define TAG_NAME_SERVICE_SEARCH        ((unsigned char) 117)
// subnet selection option
#define TAG_SUBNET_SELECTION           ((unsigned char) 118)
// domain search list option
#define TAG_DOMAIN_SEARCH_LIST         ((unsigned char) 119)
// sip servers option
#define TAG_SIP_SERVERS                ((unsigned char) 120)
// list of classless static routes (mask, addr, router) pairs
#define TAG_IP_CLASSLESS_STAT_RT       ((unsigned char) 121)
// cablelabs client config option
#define TAG_CABLELABS_CLIENT_CONFIG    ((unsigned char) 122)
// geo config option
#define TAG_GEO_CONFIG                 ((unsigned char) 123)
// vi-vendor-class-id option
#define TAG_VI_VENDOR_CLASS_ID         ((unsigned char) 124)
// vi-vendor-specific info option
#define TAG_VI_VENDOR_SPECIFIC_INFO    ((unsigned char) 125)
// list of microsoft private classless static routes (mask, addr, router) pairs
#define TAG_MSFT_IP_CLASSLESS_STAT_RT  ((unsigned char) 249)
// End of cookie
#define TAG_END                        ((unsigned char) 255)

// DHCP Relay Agent Information option subtypes:
#define RAI_CIRCUIT_ID                 ((unsigned char) 1)
#define RAI_REMOTE_ID                  ((unsigned char) 2)
#define RAI_AGENT_ID                   ((unsigned char) 3)

#define DHCP_OPTION_MAX_SIZE           255
#define RELAY_AGENT_INFO_SIZE          DHCP_OPTION_MAX_SIZE

#define AUTHINFO_LEN 40

// forward definition of dhcp_packet to avoid circular inclusion of header files
typedef struct dhcp_packet* dhcp_packet_p;

typedef struct common_option {
    const char* name;         // Option name
    const char* format;       // Option format
    bool (* check_length)(const unsigned char* value, const unsigned char len, const struct common_option* option);
    const unsigned char code; // Option number
    const unsigned char min_len;
    const unsigned char multi_of;
    const unsigned char offset;
} common_option_t;

typedef struct dhcp_option {
    unsigned char len;         // length of the value
    unsigned char* value_ptr;  // ptr in the raw packet that points to the value field of the option
    // IMPORTANT REMARK about a value_ptr:
    // the value_ptr is not guaranteed to be on a natural boundary for the data it represents
    // in cases where it represents anything else than a byte, special precautions need to
    // be taken to avoid bus errors. Use the READ and WRITE functions defined in dhcp_options.h
} dhcp_option_t;

struct authentication_info {
    unsigned char protocol;
    unsigned char algorithm;
    unsigned char rdm;
    unsigned char replay_detection[8];
    unsigned char authentication_information[0];
} __attribute__ ((__packed__));

const common_option_t* option_get(unsigned char option_code);
bool option_suboption_allowed(unsigned char option_code);
bool option_enterprise_allowed(unsigned char option_code);
void parse_dhcpoptions(dhcp_packet_p packet);
bool check_option_buf_size(dhcp_packet_p packet,
                           unsigned char** poption_ptr,
                           unsigned long value_len,
                           unsigned short* packet_size_incr);
int write_magic(dhcp_packet_p packet, unsigned char** poption_ptr);
int write_endoption(dhcp_packet_p packet, unsigned char** poption_ptr);
int write_optionchar(dhcp_packet_p packet,
                     unsigned char** poption_ptr,
                     unsigned char option_type,
                     unsigned char value);
int write_optiondword(dhcp_packet_p packet,
                      unsigned char** poption_ptr,
                      unsigned char option_type,
                      uint32_t value);
int write_optionstr(dhcp_packet_p packet,
                    unsigned char** poption_ptr,
                    unsigned char option_type,
                    char* string,
                    int null_termination);
int write_optionraw(dhcp_packet_p packet,
                    unsigned char** poption_str,
                    unsigned char option_type,
                    const unsigned char* buf,
                    unsigned long buf_len);

// authentication option related functions
int check_configuration_token(const char* token, struct authentication_info* info);
int check_delayed_authentication(struct authentication_info* info);
int check_replay_detection(long long replay_detection, struct authentication_info* info);
int set_configuration_token(const char* info, unsigned char* result, unsigned int* resultlen);

#endif // __DHCP_OPTIONS_H__
